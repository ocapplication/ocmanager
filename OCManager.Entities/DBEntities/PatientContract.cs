﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class PatientContract
    {
        [Key]
        [Column(Storage = "contract_id", CanBeNull = false)]
        public int Contract_ID { get; set; }

        [Column(Storage = "contract_tip", CanBeNull = false)]
        public int Contract_Tip { get; set; }

        [Column(Storage = "contract_nomer", CanBeNull = false)]
        public string Contract_Nomer { get; set; }

        [Column(Storage = "contract_amount", CanBeNull = false)]
        public int Contract_Amount { get; set; }

        [Column(Storage = "contract_note", CanBeNull = false)]
        public string Contract_Note { get; set; }

        [Column(Storage = "patient_id", CanBeNull = false)]
        public int Patient_ID { get; set; }

        [Column(Storage = "created", CanBeNull = false)]
        public DateTime Created { get; set; }

        [Column(Storage = "user_id", CanBeNull = false)]
        public int User_ID { get; set; }

        [Column(Storage = "check_number", CanBeNull = false)]
        public string Check_Number { get; set; }

        [Column(Storage = "end_date", CanBeNull = false)]
        public DateTime? End_Date { get; set; }

        [Column(Storage = "contract_zametka", CanBeNull = false)]
        public string Contract_Zametka { get; set; }
    }
}
