﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class Form
    {
        [Key]
        [Column(Storage = "form_id", CanBeNull = false)]
        public Int32 form_ID { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string name { get; set; }

        [Column(Storage = "form_type", CanBeNull = false)]
        public int Form_Type { get; set; }

        [Column(Storage = "description", CanBeNull = false)]
        public string Description { get; set; }

         [Column(Storage = "OMC_Price", CanBeNull = false)]
        public int OMC_Price { get; set; }
        
    }
}
