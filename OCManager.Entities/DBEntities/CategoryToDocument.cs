﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [System.Data.Linq.Mapping.Table(Name = "CategoryToDocument")]
    public class CategoryToDocument
    {
        [Key]
        [Column(Order = 0)]
        [System.Data.Linq.Mapping.Column(Storage = "category_id", CanBeNull = false)]
        public int Category_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [System.Data.Linq.Mapping.Column(Storage = "document_id", CanBeNull = false)]
        public int Document_ID { get; set; }
    }
}
