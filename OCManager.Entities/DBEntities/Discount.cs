﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class Discount
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public Int32 ID { get; set; }

        [Column(Storage = "ts", CanBeNull = false)]
        public DateTime TS { get; set; }

        [Column(Storage = "type", CanBeNull = false)]
        public string Type { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string Name { get; set; }

        [Column(Storage = "value", CanBeNull = false)]
        public float Value { get; set; }

        [Column(Storage = "description", CanBeNull = false)]
        public string Description { get; set; }
    }
}
