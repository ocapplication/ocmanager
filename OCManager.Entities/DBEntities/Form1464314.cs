﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table("Form1464314")]
    public class Form1464314
    {
        [Key]
        [Column("form_1464314_id")]
        public int form_1464314_id { get; set; }
        [Column("form_data_id")]
        public int? form_data_id { get; set; }

        [Column("1_priem")]
        public string priem_1 { get; set; }

        [Column("4_data")]
        public string data_4 { get; set; }
        [Column("6_VIS_OD_")]
        public string VIS_OD_6_ { get; set; }
        [Column("8_VIS_OD_")]
        public string VIS_OD_8_ { get; set; }
        [Column("10_VIS_OS_")]
        public string VIS_OS_10_ { get; set; }
        [Column("11_VIS_OS_")]
        public string VIS_OS_11_ { get; set; }
        [Column("13_Vis_OD")]
        public string Vis_OD_13 { get; set; }
        [Column("14_sph")]
        public string sph_14 { get; set; }
        [Column("15_cyl")]
        public string cyl_15 { get; set; }
        [Column("16_ax")]
        public string ax_16 { get; set; }
        [Column("17_")]
        public string _17 { get; set; }
        [Column("18_Vis_OS")]
        public string Vis_OS_18 { get; set; }
        [Column("19_sph")]
        public string sph_19 { get; set; }
        [Column("20_cyl")]
        public string cyl_20 { get; set; }
        [Column("21_ax")]
        public string ax_21 { get; set; }
        [Column("22_")]
        public string _22 { get; set; }
        [Column("26_ax")]
        public string ax_26 { get; set; }
        [Column("27_diopt")]
        public string diopt_27 { get; set; }
        [Column("28_ax")]
        public string ax_28 { get; set; }
        [Column("29_diopt")]
        public string diopt_29 { get; set; }
        [Column("30_ax")]
        public string ax_30 { get; set; }
        [Column("31_diopt")]
        public string diopt_31 { get; set; }
        [Column("32_ax")]
        public string ax_32 { get; set; }
        [Column("33_diopt")]
        public string diopt_33 { get; set; }
        [Column("35_OD_sph")]
        public string OD_sph_35 { get; set; }
        [Column("36_cyl")]
        public string cyl_36 { get; set; }
        [Column("37_ax")]
        public string ax_37 { get; set; }
        [Column("38_OS_sph")]
        public string OS_sph_38 { get; set; }
        [Column("39_cyl")]
        public string cyl_39 { get; set; }
        [Column("40_ax")]
        public string ax_40 { get; set; }
        [Column("41_midriaz")]
        public string midriaz_41 { get; set; }
        [Column("45_uzkij_zrachok")]
        public string uzkij_zrachok_45 { get; set; }
        [Column("48_SolMidriacil_05")]
        public string SolMidriacil_05_48 { get; set; }
        [Column("51_SolMidriacil_1")]
        public string SolMidriacil_1_51 { get; set; }
        [Column("54_OD")]
        public string OD_54 { get; set; }
        [Column("55____")]
        public string ____55 { get; set; }
        [Column("56_OS")]
        public string OS_56 { get; set; }
        [Column("57____")]
        public string ____57 { get; set; }
        [Column("59_po_girwbergu_bo")]
        public string po_girwbergu_bo_59 { get; set; }
        [Column("60__")]
        public string __60 { get; set; }
        [Column("61_po_girwbergu_vo")]
        public string po_girwbergu_vo_61 { get; set; }
        [Column("62__")]
        public string __62 { get; set; }
        [Column("64_33cm__vo")]
        public string cm__vo_64_33 { get; set; }
        [Column("70_bo")]
        public string bo_70 { get; set; }
        [Column("76_1m__vo")]
        public string m__vo_76_1 { get; set; }
        [Column("82_bo")]
        public string bo_82 { get; set; }
        [Column("88_5m__vo")]
        public string m__vo_88_5 { get; set; }
        [Column("94_bo")]
        public string bo_94 { get; set; }
        [Column("101_bo_ou")]
        public string bo_ou_101 { get; set; }
        [Column("102__")]
        public string __102 { get; set; }
        [Column("103_su")]
        public string su_103 { get; set; }
        [Column("104__")]
        public string __104 { get; set; }
        [Column("105_vo_ou")]
        public string vo_ou_105 { get; set; }
        [Column("106__")]
        public string __106 { get; set; }
        [Column("107_su")]
        public string su_107 { get; set; }
        [Column("108__")]
        public string __108 { get; set; }
        [Column("110_zapas_akkomodacii")]
        public string zapas_akkomodacii_110 { get; set; }
        [Column("111_izrashodovannaya_chast")]
        public string izrashodovannaya_chast_111 { get; set; }
        [Column("112_obem_akkomodacii")]
        public string obem_akkomodacii_122 { get; set; }
        [Column("114_OD")]
        public string OD_114 { get; set; }
        [Column("116_OS")]
        public string OS_116 { get; set; }
        [Column("119_OD_norma")]
        public string OD_norma_119 { get; set; }
        [Column("120_OS_norma")]
        public string OS_norma_120 { get; set; }
        [Column("122_OD_norma")]
        public string OD_norma_122 { get; set; }
        [Column("123_OS_norma")]
        public string OS_norma_123 { get; set; }
        [Column("124_issledovanie_temnovoj_adaptacii_norma")]
        public string issledovanie_temnovoj_adaptacii_norma_124 { get; set; }
        [Column("125_opredelenie_polya_vzora_norma")]
        public string opredelenie_polya_vzora_norma_125 { get; set; }
        [Column("126_opredelenie_cvetooschuscheniya_norma")]
        public string opredelenie_cvetooschuscheniya_norma_126 { get; set; }
        [Column("127_dvizheniya_glaznih_yablok")]
        public string dvizheniya_glaznih_yablok_127 { get; set; }
        [Column("130__")]
        public string __130 { get; set; }
        [Column("131_nistagm")]
        public string nistagm_131 { get; set; }
        [Column("134__")]
        public string __134 { get; set; }
        [Column("138__")]
        public string __138 { get; set; }
        [Column("142__")]
        public string __142 { get; set; }
        [Column("146_konvergenciya")]
        public string konvergenciya_146 { get; set; }
        [Column("149_nalichie_diplopii")]
        public string nalichie_diplopii_149 { get; set; }
        [Column("152_proverka_ravnodejstviya_glaznih_miwc_na_vibor")]
        public string proverka_ravnodejstviya_glaznih_miwc_na_vibor_152 { get; set; }
        [Column("156_polozhenie_vek")]
        public string polozhenie_vek_156 { get; set; }
        [Column("161_smikanie_glaznoj_scheli")]
        public string smikanie_glaznoj_scheli_161 { get; set; }
        [Column("164__")]
        public string __164 { get; set; }
        [Column("165_konjunktiva")]
        public string konjunktiva_165 { get; set; }
        [Column("171__")]
        public string __171 { get; set; }
        [Column("172_oblast_sleznogo_mewka")]
        public string oblast_sleznogo_mewka_172 { get; set; }
        [Column("178_otdelyaemoe")]
        public string otdelyaemoe_178 { get; set; }
        [Column("182_otdelyaemoe_2")]
        public string otdelyaemoe_2_182 { get; set; }
        [Column("186__")]
        public string __186 { get; set; }
        [Column("187_rogovica")]
        public string rogovica_187 { get; set; }
        [Column("191__")]
        public string __191 { get; set; }
        [Column("192_fljuoresceinovaya_instillyacionnaya_proba_norma")]
        public string fljuoresceinovaya_instillyacionnaya_proba_norma_192 { get; set; }
        [Column("199_perednyaya_kamera")]
        public string perednyaya_kamera_199 { get; set; }
        [Column("203_vlaga_perednej_kameri")]
        public string vlaga_perednej_kameri_203 { get; set; }
        [Column("208_kommentarij")]
        public string kommentarij_208 { get; set; }
        [Column("210_raduzhka")]
        public string raduzhka_210 { get; set; }
        [Column("220_kommentarij")]
        public string kommentarij_220 { get; set; }
        [Column("221_zrachok")]
        public string zrachok_221 { get; set; }
        [Column("227_kommentarij")]
        public string kommentarij_227 { get; set; }
        [Column("228_reakciya_zrachka_na_svet_pryamaya")]
        public string reakciya_zrachka_na_svet_pryamaya_228 { get; set; }
        [Column("232_sodruzhestvennaya")]
        public string sodruzhestvennaya_232 { get; set; }
        [Column("236_hrustalik")]
        public string hrustalik_236 { get; set; }
        [Column("242_kommentarij")]
        public string kommentarij_242 { get; set; }
        [Column("243_steklovidnoe_telo")]
        public string steklovidnoe_telo_243 { get; set; }
        [Column("248_kommentarij")]
        public string kommentarij_248 { get; set; }
        [Column("250_dzn")]
        public string dzn_250 { get; set; }
        [Column("257_kommentarij")]
        public string kommentarij_257 { get; set; }
        [Column("258_granici")]
        public string granici_258 { get; set; }
        [Column("263_kommentarij")]
        public string kommentarij_263 { get; set; }
        [Column("264_arterii_i_veni")]
        public string arterii_i_veni_264 { get; set; }
        [Column("268_hod")]
        public string hod_268 { get; set; }
        [Column("272_makulyarnaya_oblast")]
        public string makulyarnaya_oblast_272 { get; set; }
        [Column("276_kommentarij")]
        public string kommentarij_276 { get; set; }
        [Column("277_periferiya_glaznogo_dna")]
        public string periferiya_glaznogo_dna_277 { get; set; }
        [Column("281_kommentarij")]
        public string kommentarij_281 { get; set; }
        [Column("282_lokalizaciya_razriva_setchatki")]
        public string lokalizaciya_razriva_setchatki_282 { get; set; }
        [Column("708_OD_sph")]
        public string OD_sph_708 { get; set; }
        [Column("709_cyl")]
        public string cyl_709 { get; set; }
        [Column("710_ax")]
        public string ax_710 { get; set; }
        [Column("711_OS_sph")]
        public string OS_sph_711 { get; set; }
        [Column("712_cyl")]
        public string cyl_712 { get; set; }
        [Column("713_ax")]
        public string ax_713 { get; set; }
        [Column("714_zapolnil")]
        public string zapolnil_714 { get; set; }
        [Column("715_data")]
        public string data_715 { get; set; }
        [Column("716_OD_sph")]
        public string OD_sph_716 { get; set; }
        [Column("717_cyl")]
        public string cyl_717 { get; set; }
        [Column("718_ax")]
        public string ax_718 { get; set; }
        [Column("719_OS_sph")]
        public string OS_sph_719 { get; set; }
        [Column("720_cyl")]
        public string cyl_720 { get; set; }
        [Column("721_ax")]
        public string ax_721 { get; set; }
        [Column("722_zapolnil")]
        public string zapolnil_722 { get; set; }
        [Column("723_data")]
        public string data_723 { get; set; }
        [Column("27_cikloplegiya")]
        public string cikloplegiya_27 { get; set; }
        [Column("30_cikloplegiya_2")]
        public string cikloplegiya_2_30 { get; set; }
        [Column("301_gonioskopia_upk")]
        public string gonioskopia_upk_301 { get; set; }
        [Column("302_gonioskopia_eye")]
        public string gonioskopia_eye_302 { get; set; }
        [Column("303_gonioskopia_sk")]
        public string gonioskopia_sk_303 { get; set; }
        [Column("304_gonioskopia_eye")]
        public string gonioskopia_eye_304 { get; set; }
        [Column("601_pred_diagnoz")]
        public string pred_diagnoz_601 { get; set; }
        [Column("602_leading")]
        public string leading_602 { get; set; }
        [Column("603_sight")]
        public string sight_603 { get; set; }
        [Column("username")]
        public string Username { get; set; }
        [Column("user_id")]
        public int User_Id { get; set; }
        [Column("data")]
        public DateTime Data { get; set; }
    }
}
