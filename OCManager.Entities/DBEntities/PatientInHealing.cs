﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class PatientInHealing
    {
        [Key]
        [Column(Storage = "id_in_healing", CanBeNull = false)]
        public int ID_In_Healing { get; set; }

        [Column(Storage = "patient_id", CanBeNull = false)]
        public int Patient_Id { get; set; }

        [Column(Storage = "form_data_id", CanBeNull = false)]
        public int Form_Data_Id { get; set; }

        [Column(Storage = "building", CanBeNull = false)]
        public string Building { get; set; }

        [Column(Storage = "form_filling_date", CanBeNull = false)]
        public DateTime Form_Filling_Date { get; set; }

        [Column(Storage = "treatment_date_started", CanBeNull = false)]
        public DateTime? Treatment_Date_Started { get; set; }
        
        [Column(Storage = "dates_of_passed_days", CanBeNull = false)]
        public string Dates_Of_Passed_Days { get; set; }

        [Column(Storage = "date_of_termination_of_treatment", CanBeNull = false)]
        public DateTime? Date_Of_Termination_Of_Treatment { get; set; }

        [Column(Storage = "date_of_end_of_treatment", CanBeNull = false)]
        public DateTime? Date_Of_End_Of_Treatment { get; set; }

        [Column(Storage = "in_healing_note", CanBeNull = false)]
        public string In_Healing_Note { get; set; }

        [Column(Storage = "who_filled_username", CanBeNull = false)]
        public string Who_Filled_Username { get; set; }

        [Column(Storage = "who_filled_nickname", CanBeNull = false)]
        public string Who_Filled_Nickname { get; set; }
    }
}
