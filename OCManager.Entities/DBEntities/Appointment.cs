﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table(Name = "appointment")]
    public class Appointment
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public Int64 ID { get; set; }

        [Column(Storage = "ts", CanBeNull = true)]
        public DateTime? TS { get; set; }

        [Column(Storage = "name", CanBeNull = true)]
        public string Name { get; set; }


        [Column(Storage = "form_id", CanBeNull = true)]
        public int Form_ID { get; set; }


        [Column(Storage = "cost", CanBeNull = true)]
        public double Cost { get; set; }


        [Column(Storage = "type", CanBeNull = true)]
        public string Type { get; set; }

        [Column(Storage = "ordinary_discount", CanBeNull = true), Display(Name = "Ordinary Discount")]
        public string Ordinary_Discount { get; set; }

        [Column(Storage = "second_treatment_discount", CanBeNull = true), Display(Name = "Second Treatment Discount")]
        public string Second_Treatment_Discount { get; set; }

        [Column(Storage = "story_discount", CanBeNull = true), Display(Name = "Story Discount")]
        public string story_discount { get; set; }

        [Column(Storage = "first_consult_bonus", CanBeNull = true), Display(Name = "First Consult Bonus")]
        public string First_Consult_Bonus { get; set; }

        [Column(Storage = "certificate_payment", CanBeNull = true), Display(Name = "Certificate Payment")]
        public string Certificate_Payment { get; set; }

        [Column(Storage = "Doctor_time", CanBeNull = true)]
        public decimal Doctor_Time { get; set; }

        [Column(Storage = "Nurse_Time", CanBeNull = true)]
        public decimal Nurse_Time { get; set; }

        [Column(Storage = "Code_OMC", CanBeNull = true)]
        public int? Code_OMC { get; set; }

        [Column(Storage = "OMC_Price", CanBeNull = true)]
        public decimal? OMC_Price { get; set; }

        [Column(Storage = "Order", CanBeNull = true)]
        public int? Order { get; set; }

    }
}
