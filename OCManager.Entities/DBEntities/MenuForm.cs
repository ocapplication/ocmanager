﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table(Name = "Menu_Form")]
    public class MenuForm
    {
        [Key]
        [Column(Storage = "menu_form_id", CanBeNull = false)]
        public Int32 menu_Form_ID { get; set; }

        [Column(Storage = "menu_id", CanBeNull = false)]
        public Int32 Menu_ID { get; set; }

        [Column(Storage = "form_id", CanBeNull = false)]
        public Int32 form_ID { get; set; }

        [Column(Storage = "title", CanBeNull = false)]
        public string Title { get; set; }

        [Column(Storage = "custom_action", CanBeNull = false)]
        public string Custom_Action { get; set; }
    }
}
