﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class Form2161884
    {
        [Key]
        [Column("form_1410518_id")]
        public int form_1410518_id { get; set; }

        [Column("form_data_id")]
        public int? form_data_id { get; set; }

        [Column("primechanie_k_2051655")]
        public string Primechanie_k_2051655 { get; set; }
        [Column("id_2051655")]
        public int Id_2051655 { get; set; }

        [Column("primechanie_k_2051656")]
        public string Primechanie_k_2051656 { get; set; }           
        [Column("id_2051656")]
        public int Id_2051656 { get; set; }

        [Column("primechanie_k_2051657")]
        public string Primechanie_k_2051657 { get; set; }
        [Column("id_2051657")]
        public int Id_2051657 { get; set; }

        [Column("primechanie_k_2051658")]
        public string Primechanie_k_2051658 { get; set; }
        [Column("id_2051658")]
        public int Id_2051658 { get; set; }

        [Column("primechanie_k_1951659")]
        public string Primechanie_k_1951659 { get; set; }
        [Column("id_1951659")]
        public int Id_1951659 { get; set; }

        [Column("primechanie_k_1951660")]
        public string Primechanie_k_1951660 { get; set; }
        [Column("id_1951660")]
        public int Id_1951660 { get; set; }

        [Column("primechanie_k_1951661")]
        public string Primechanie_k_1951661 { get; set; }
        [Column("id_1951661")]
        public int Id_1951661 { get; set; }

        [Column("primechanie_k_1951662")]
        public string Primechanie_k_1951662 { get; set; }
        [Column("id_1951662")]
        public int Id_1951662 { get; set; }

        [Column("primechanie_k_1951663")]
        public string Primechanie_k_1951663 { get; set; }
        [Column("id_1951663")]
        public int Id_1951663 { get; set; }

        [Column("primechanie_k_1951664")]
        public string Primechanie_k_1951664 { get; set; }
        [Column("id_1951664")]
        public int Id_1951664 { get; set; }

        [Column("primechanie_k_1951671")]
        public string Primechanie_k_1951671 { get; set; }
        [Column("id_1951671")]
        public int Id_1951671 { get; set; }

        [Column("primechanie_k_1951672")]
        public string Primechanie_k_1951672 { get; set; }
        [Column("id_1951672")]
        public int Id_1951672 { get; set; }

        [Column("primechanie_k_1951669")]
        public string Primechanie_k_1951669 { get; set; }
        [Column("id_1951669")]
        public int Id_1951669 { get; set; }

        [Column("primechanie_k_1951670")]
        public string Primechanie_k_1951670 { get; set; }
        [Column("id_1951670")]
        public int Id_1951670 { get; set; }

        [Column("primechanie_k_1951665")]
        public string Primechanie_k_1951665 { get; set; }
        [Column("id_1951665")]
        public int Id_1951665 { get; set; }

        [Column("primechanie_k_1951666")]
        public string Primechanie_k_1951666 { get; set; }
        [Column("id_1951666")]
        public int Id_1951666 { get; set; }

        [Column("primechanie_k_2161880")]
        public string Primechanie_k_2161880 { get; set; }
        [Column("id_2161880")]
        public int Id_2161880 { get; set; }

        [Column("primechanie_k_2161881")]
        public string Primechanie_k_2161881 { get; set; }
        [Column("id_2161881")]
        public int Id_2161881 { get; set; }

        [Column("12_kommentarii_k_kursu_lecheniya")]
        public string kommentarii_k_kursu_lecheniya_12 { get; set; }  // changed name

        [Column("occ_id")]
        public int occ_id { get; set; }

        [Column("occ_id_1")]
        public int occ_id_1 { get; set; }

        [Column("occ_id_2")]
        public int occ_id_2 { get; set; }

        [Column("occ_id_3")]
        public int occ_id_3 { get; set; }

        [Column("occ_id_4")]
        public int occ_id_4 { get; set; }

        [Column("occ_id_5")]
        public int occ_id_5 { get; set; }

        [Column("occ_id_6")]
        public int occ_id_6 { get; set; }

        [Column("occ_id_7")]
        public int occ_id_7 { get; set; }

        [Column("username")]
        public string username { get; set; }

        [Column("user_id")]
        public int user_id { get; set; }

        [Column("data")]
        public DateTime data { get; set; }
    }
}
