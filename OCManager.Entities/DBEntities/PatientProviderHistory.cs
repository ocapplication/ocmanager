﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class PatientProviderHistory
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public int ID { get; set; }

        [Column(Storage = "patient_id", CanBeNull = false)]
        public int Patient_ID { get; set; }

        [Column(Storage = "default_provider", CanBeNull = false)]
        public int Default_Provider { get; set; }

        [Column(Storage = "username", CanBeNull = false)]
        public string Username { get; set; }

        [Column(Storage = "date", CanBeNull = false)]
        public DateTime Date { get; set; }

        [Column(Storage = "type", CanBeNull = false)]
        public string Type { get; set; }

        [Column(Storage = "to_mail", CanBeNull = false)]
        public string To_Mail { get; set; }
    }
}
