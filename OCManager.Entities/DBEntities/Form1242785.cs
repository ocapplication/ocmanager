﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table("form_1242785")]
    public class Form1242785
    {
        [Key]
        [Column("form_1242785_id")]
        public int Form_1242785_ID { get; set; }
        [Column("form_data_id")]
        public int? Form_Data_ID { get; set; }

        [Column("0_tekst_vipiski")]
        public string Tekst_Vipiski_0 { get; set; }

        [Column("username")]
        public string Username { get; set; }

        [Column("user_id")]
        public int User_ID { get; set; }

        [Column("data")]
        public DateTime Data { get; set; }

        [Column("customer")]
        public int Customer { get; set; }

        [Column("tip_vipiski")]
        public int? Tip_Vipiski { get; set; }

        [Column("create_date")]
        public DateTime create_date { get; set; }

        [Column("comment")]
        public string Comment { get; set; }

        [Column("attending_doctor")]
        public int Attending_Doctor { get; set; }

        [Column("head1")]
        public string Head1 { get; set; }

        [Column("head2")]
        public string Head2 { get; set; }

        [Column("signature_603615")]
        public string Signature_603615 { get; set; }

        [Column("signature_613915")]
        public string Signature_613915 { get; set; }

        [Column("signature_status")]
        public int? Signature_Status { get; set; }

        [Column("NextAssignee")]
        public string NextAssignee { get; set; }
    }
}
