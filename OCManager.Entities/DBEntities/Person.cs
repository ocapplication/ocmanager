﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table(Name="Person")]
    public class Person
    {
        [Key]
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        [Column(Storage = "person_id", CanBeNull = false)]
        public Int32 Person_ID { get; set; }

        [Column(Storage = "salutation", CanBeNull = true)]
        public string Salutation { get; set; }

        [Column(Storage = "last_name", CanBeNull = true)]
        public string Last_Name { get; set; }


        [Column(Storage = "first_name", CanBeNull = true)]
        public string First_Name { get; set; }


        [Column(Storage = "middle_name", CanBeNull = true)]
        public string Middle_Name { get; set; }


        [Column(Storage = "gender", CanBeNull = true)]
        public int Gender { get; set; }

        [Column(Storage = "initials", CanBeNull = true)]
        public string Initials { get; set; }

        [Column(Storage = "date_of_birth", CanBeNull = true)]
        public Nullable<DateTime> Date_Of_Birth { get; set; }

        [Column(Storage = "summary", CanBeNull = true)]
        public string Summary { get; set; }

        [Column(Storage = "title", CanBeNull = true)]
        public string Title { get; set; }

        [Column(Storage = "notes", CanBeNull = true)]
        public string Notes { get; set; }

        [Column(Storage = "email", CanBeNull = true)]
        public string  Email { get; set; }

        [Column(Storage = "secondary_email", CanBeNull = true)]
        public string Secondary_Email { get; set; }

        [Column(Storage = "has_photo", CanBeNull = true)]
        public string Has_Photo { get; set; }

        [Column(Storage = "identifier", CanBeNull = true)]
        public string Identifier { get; set; }

        [Column(Storage = "identifier_type", CanBeNull = true)]
        public string Identifier_Type { get; set; }

        [Column(Storage = "marital_status", CanBeNull = true)]
        public string Marital_Status { get; set; }
        [Column(Storage = "ts", CanBeNull = false)]
        public DateTime TS { get; set; }
    }
}
