﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
      [Table(Name = "Form_Data")]
    public class Form_Data
    {
        [Key]
        [Column(Storage = "form_data_id", CanBeNull = false)]
        public Int32 form_Data_ID { get; set; }

        [Column(Storage = "form_id", CanBeNull = false)]
        public Int32 form_ID { get; set; }


        [Column(Storage = "external_id", CanBeNull = true)]
        public int External_ID { get; set; }

        [Column(Storage = "username", CanBeNull = false)]
        public string username { get; set; }

        [Column(Storage = "last_edit", CanBeNull = false)]
        public DateTime last_Edit { get; set; }

        [Column(Storage = "user_id", CanBeNull = true)]
        public int User_ID { get; set; }

        [Column(Storage = "omc", CanBeNull = true)]
        public int Omc { get; set; }
    }
}
