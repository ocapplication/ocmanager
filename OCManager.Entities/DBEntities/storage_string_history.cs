﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCManager.Entities.DBEntities
{
    public class storage_string_history
    {
        [Key]
        [System.Data.Linq.Mapping.Column(Storage = "ID", CanBeNull = false)]
        public Int32 ID { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "foreign_key", CanBeNull = false)]
        public Int32 foreign_Key { get; set; }
        
        [System.Data.Linq.Mapping.Column(Storage = "NameKeyOld", CanBeNull = false)]
        public string NameKeyOld { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "NameKeyNew", CanBeNull = false)]
        public string NameKeyNew { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "ValueKeyOld", CanBeNull = false)]
        public string ValueKeyOld { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "ValueKeyNew", CanBeNull = false)]
        public string ValueKeyNew { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "DateOfChange", CanBeNull = false)]
        public DateTime DateOfChange { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "AppointmentID", CanBeNull = false)]
        public Int32 AppointmentID { get; set; }
    }
}
