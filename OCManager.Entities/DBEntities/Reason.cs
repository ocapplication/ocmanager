﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
     [Table(Name = "Reasons")]
    public class Reason
    {
        [Key]
        [Column(Storage = "reason_id", CanBeNull = false)]
        public Int32 Reason_ID { get; set; }

        [Column(Storage = "reason_text", CanBeNull = false)]
        public string Reason_Text { get; set; }
    }
}
