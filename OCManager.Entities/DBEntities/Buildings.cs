﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table(Name = "Buildings")]
    public class Buildings
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public int ID { get; set; }

        [Column(Storage = "description", CanBeNull = false)]
        public string Description { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string Name { get; set; }


        [Column(Storage = "practice_id", CanBeNull = false)]
        public int Practice_ID { get; set; }


        [Column(Storage = "identifier", CanBeNull = false)]
        public string Identifier { get; set; }


        [Column(Storage = "facility_code_id", CanBeNull = false)]
        public int Facility_Code_ID { get; set; }

        [Column(Storage = "order", CanBeNull = false )]
        public int Order { get; set; }
    }
}
