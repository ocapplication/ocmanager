﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using System.ComponentModel.DataAnnotations;

namespace OCManager.Entities.DBEntities
{
    [Table(Name = "category")]
    public class Category
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public Int32 ID { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string Name { get; set; }

        [Column(Storage = "value", CanBeNull = false)]
        public string Value { get; set; }

        [Column(Storage = "parent", CanBeNull = false)]
        public Int32 Parent { get; set; }

        [Column(Storage = "lft", CanBeNull = false)]
        public Int32 LFT { get; set; }

        [Column(Storage = "rght", CanBeNull = false)]
        public Int32 Rght { get; set; }
    }
}
