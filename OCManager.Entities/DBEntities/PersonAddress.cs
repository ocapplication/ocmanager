﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [System.Data.Linq.Mapping.Table(Name = "person_address")]
    public class PersonAddress
    {
        [Key]
        [Column(Order = 0)]
        [System.Data.Linq.Mapping.Column(Storage = "person_id", CanBeNull = false)]
        public int Person_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [System.Data.Linq.Mapping.Column(Storage = "address_id", CanBeNull = false)]
        public int Address_Id { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "address_type", CanBeNull = false)]
        public int Address_Type { get; set; }

    }
}
