﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [System.Data.Linq.Mapping.Table(Name = "address")]
    public class Address
    {
        [Key]
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        [Column(Storage = "address_id", CanBeNull = false)]
        public Int32 Address_Id { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string Name { get; set; }

        [Column(Storage = "type", CanBeNull = true)]
        public string Type { get; set; }

        [Column(Storage = "line1", CanBeNull = false)]
        public string Line1 { get; set; }

        [Column(Storage = "line2", CanBeNull = false)]
        public string Line2 { get; set; }

        [Column(Storage = "city", CanBeNull = false)]
        public string City { get; set; }

        [Column(Storage = "region", CanBeNull = false)]
        public int Region { get; set; }

        [Column(Storage = "country", CanBeNull = false)]
        public int County { get; set; }

        [Column(Storage = "state", CanBeNull = false)]
        public int State { get; set; }

        [Column(Storage = "postal_code", CanBeNull = false)]
        public string Postal_Code { get; set; }

        [Column(Storage = "notes", CanBeNull = false)]
        public string Notes { get; set; }
    }
}
