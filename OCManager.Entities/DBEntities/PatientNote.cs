﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class PatientNote
    {
        [Key]
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        [Column(Storage = "patient_note_id", CanBeNull = false)]
        public int Patient_Note_Id { get; set; }

        [Column(Storage = "patient_id", CanBeNull = false)]
        public int Patient_ID { get; set; }

        [Column(Storage = "user_id", CanBeNull = false)]
        public int User_ID { get; set; }

        [Column(Storage = "priority", CanBeNull = false)]
        public int Priority { get; set; }

        [Column(Storage = "note_date", CanBeNull = false)]
        public DateTime Note_Date { get; set; }

        [Column(Storage = "note", CanBeNull = false)]
        public string Note { get; set; }

        [Column(Storage = "deprecated", CanBeNull = false)]
        public Int16 Deprecated { get; set; }
    }
}
