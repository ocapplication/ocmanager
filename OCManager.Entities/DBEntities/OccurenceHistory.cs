﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace OCManager.Entities.DBEntities
{
    public  partial class OccurenceHistory
    {
        [Key]
        [Column(Storage = "oc_h_id", CanBeNull = false)]
        public Int32 Oc_H_ID { get; set; }

        [Column(Storage = "oc_id", CanBeNull = false)]
        public Int32 Oc_ID { get; set; }

        [Column(Storage = "start", CanBeNull = false)]
        public DateTime Start { get; set; }


        [Column(Storage = "end", CanBeNull = false)]
        public DateTime End { get; set; }


        [Column(Storage = "notes", CanBeNull = false)]
        public string Notes { get; set; }


        [Column(Storage = "location_id", CanBeNull = false)]
        public int Location_ID { get; set; }

        [Column(Storage = "user_id", CanBeNull = true)]
        public int User_ID { get; set; }

        [Column(Storage = "last_change_id", CanBeNull = true)]
        public int Last_Change_ID { get; set; }

        [Column(Storage = "external_id", CanBeNull = true)]
        public Nullable<Int32> External_ID { get; set; }

        [Column(Storage = "reason_code", CanBeNull = false)]
        public int Reason_Code { get; set; }

        [Column(Storage = "timestamp", CanBeNull = false)]
        public DateTime TimeStamp { get; set; }

        [Column(Storage = "walkin", CanBeNull = false)]
        public Int16 WalkIn { get; set; }
        
        [Column(Storage = "isDeleted", CanBeNull= false)]
        public bool isDeleted { get; set; }
    }
}
