﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class DiscountNote
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public Int32 ID { get; set; }

        [Column(Storage = "foreign_id", CanBeNull = false)]
        public int Foreign_ID { get; set; }

        [Column(Storage = "note_type", CanBeNull = false)]
        public int Note_Type { get; set; }

        [Column(Storage = "note", CanBeNull = false)]
        public string Note { get; set; }

        [Column(Storage = "discount", CanBeNull = false)]
        public int Discount { get; set; }

        [Column(Storage = "owner", CanBeNull = false)]
        public int Owner { get; set; }

        [Column(Storage = "date", CanBeNull = false)]
        public DateTime Date { get; set; }

        [Column(Storage = "revision", CanBeNull = false)]
        public DateTime Revision { get; set; }


    }
}
