﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [System.Data.Linq.Mapping.Table(Name = "storage_string")]
    public class storage_string
    {
        [Key]
        [Column(Order = 0)]
        [System.Data.Linq.Mapping.Column(Storage = "foreign_key", CanBeNull = false)]
        public Int32 foreign_Key { get; set; }

        [Key]
        [Column(Order = 1)]
        [System.Data.Linq.Mapping.Column(Storage = "value_key", CanBeNull = false)]
        public string value_Key { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "value", CanBeNull = false)]
        public string value { get; set; }
        [System.Data.Linq.Mapping.Column(Storage = "appointment_id", CanBeNull = false)]
        public Int32 Appointment_ID { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "order_number", CanBeNull = false)]
        public Int32? order_number { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "procedure_name", CanBeNull = false)]
        public string procedure_name { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "procedure_price", CanBeNull = false)]
        public decimal? procedure_price { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "discount", CanBeNull = false)]
        public string discount { get; set; }

        [System.Data.Linq.Mapping.Column(Storage = "procedure_total", CanBeNull = false)]
        public decimal? procedure_total { get; set; }
    }
}
