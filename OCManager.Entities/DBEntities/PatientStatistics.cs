﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table(Name = "patient_statistics")]
    public class PatientStatistics
    {
        [Key]
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        [Column(Storage = "person_id", CanBeNull = false)]
        public Int32 Person_ID { get; set; }

        [Column(Storage = "ethnicity", CanBeNull = false)]
        public string Ethnicity { get; set; }

        [Column(Storage = "race", CanBeNull = true)]
        public string Race { get; set; }


        [Column(Storage = "income", CanBeNull = true)]
        public string Income { get; set; }


        [Column(Storage = "income_ext", CanBeNull = true)]
        public string Income_Ext { get; set; }


        [Column(Storage = "language", CanBeNull = true)]
        public string Language { get; set; }

        [Column(Storage = "migrant_status", CanBeNull = true)]
        public string Migrant_Status { get; set; }

        [Column(Storage = "registration_location", CanBeNull = true)]
        public int Registration_Location { get; set; }

        [Column(Storage = "sign_in_date", CanBeNull = true)]
        public DateTime sign_in_date { get; set; }

        [Column(Storage = "monthly_income", CanBeNull = true)]
        public string Monthly_Income { get; set; }

        [Column(Storage = "family_size", CanBeNull = true)]
        public string Family_Size { get; set; }

        [Column(Storage = "recom_clinics", CanBeNull = true)]
        public string Recom_Clinics { get; set; }

        [Column(Storage = "recom_doctor", CanBeNull = true)]
        public string Recom_Doctor { get; set; }
    }
}
