﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class Room
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public Int32 ID { get; set; }

        [Column(Storage = "description", CanBeNull = false)]
        public string Description { get; set; }

        [Column(Storage = "number_seats", CanBeNull = false)]
        public Int32 Number_Seats { get; set; }

        [Column(Storage = "building_id", CanBeNull = false)]
        public Int32 Building_ID { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string name { get; set; }

        [Column(Storage = "order", CanBeNull = false)]
        public Int32 Order { get; set; }
    }
}
