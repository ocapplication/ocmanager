﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    [Table(Name = "Documents")]
    public class Documents
    {
        [Key]
        [Column(Storage = "id", CanBeNull = false)]
        public Int32 ID { get; set; }

        [Column(Storage = "name", CanBeNull = false)]
        public string Name { get; set; }

        [Column(Storage = "type", CanBeNull = true)]
        public string Type { get; set; }


        [Column(Storage = "size", CanBeNull = true)]
        public int Size { get; set; }


        [Column(Storage = "date", CanBeNull = true)]
        public DateTime Date { get; set; }


        [Column(Storage = "url", CanBeNull = true)]
        public string url { get; set; }

        [Column(Storage = "mimetype", CanBeNull = true)]
        public string MimeType { get; set; }

        [Column(Storage = "pages", CanBeNull = true)]
        public Nullable<int> Pages { get; set; }

        [Column(Storage = "owner", CanBeNull = true)]
        public Nullable<int> Owner { get; set; }

        [Column(Storage = "revision", CanBeNull = true)]
        public DateTime Revision { get; set; }

        [Column(Storage = "foreign_id", CanBeNull = true)]
        public int Foreign_ID { get; set; }

        [Column(Storage = "group_id", CanBeNull = true)]
        public int Group_ID { get; set; }
    }
}
