﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class PatientInsurance
    {
        [Key]
        [Column(Storage = "ins_id", CanBeNull = false)]
        public int ins_id { get; set; }

        [Column(Storage = "ins_tip", CanBeNull = false)]
        public string Ins_Tip { get; set; }

        [Column(Storage = "ins_nomer", CanBeNull = false)]
        public string Ins_Nomer { get; set; }

        [Column(Storage = "ins_name", CanBeNull = false)]
        public string Ins_Name { get; set; }

        [Column(Storage = "patient_id", CanBeNull = false)]
        public int Patient_Id { get; set; }

        [Column(Storage = "created", CanBeNull = false)]
        public DateTime Created { get; set; }

        [Column(Storage = "birth_cert", CanBeNull = false)]
        public string Birth_Cert { get; set; }

        [Column(Storage = "napravlenie", CanBeNull = false)]
        public string Napravlenie { get; set; }

        [Column(Storage = "naprav_kuda", CanBeNull = false)]
        public string Naprav_Kuda { get; set; }

        [Column(Storage = "polik_name", CanBeNull = false)]
        public string Polik_Name { get; set; }

        [Column(Storage = "polik_city", CanBeNull = false)]
        public string Polik_City { get; set; }

        [Column(Storage = "naprav_date", CanBeNull = false)]
        public DateTime? Naprav_Date { get; set; }

        [Column(Storage = "agent_name", CanBeNull = false)]
        public string Agent_Name { get; set; }

        [Column(Storage = "agent_date", CanBeNull = false)]
        public DateTime? Agent_Date { get; set; }

        [Column(Storage = "region", CanBeNull = false)]
        public string Region { get; set; }

        [Column(Storage = "kurator", CanBeNull = false)]
        public string Kurator { get; set; }

        [Column(Storage = "user_id", CanBeNull = false)]
        public int User_Id { get; set; }
    }
}
