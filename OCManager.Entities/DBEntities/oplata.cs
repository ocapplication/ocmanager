﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.DBEntities
{
    public class oplata
    {
        [Key]
        [Column(Storage = "oplata_id", CanBeNull = false)]
        public Int64 oplata_ID { get; set; }

        [Column(Storage = "patient_id", CanBeNull = false)]
        public Int64 patient_ID { get; set; }

        [Column(Storage = "naznach_id", CanBeNull = false)]
        public Int64 naznach_ID { get; set; }

        [Column(Storage = "sum_payed", CanBeNull = false)]
        public Double sum_payed { get; set; }


        [Column(Storage = "created", CanBeNull = false)]
        public DateTime created { get; set; }

        [Column(Storage = "comment", CanBeNull = false)]
        public string comment { get; set; }

        [Column(Storage = "kassir_id", CanBeNull = false)]
        public Int64 kassir_ID { get; set; }

        [Column(Storage = "konsult", CanBeNull = false)]
        public Int16 Konsult { get; set; }

        [Column(Storage = "vozvrat", CanBeNull = false)]
        public Int16 Vozvrat { get; set; }


        [Column(Storage = "tip_oplati", CanBeNull = false)]
        public Int16 Tip_Oplati { get; set; }

        [Column(Storage = "certif_id", CanBeNull = false)]
        public Int64 Certif_ID { get; set; }


        [Column(Storage = "sberbank", CanBeNull = false)]
        public Int16 Sberbank { get; set; }

        [Column(Storage = "contract_id", CanBeNull = false)]
        public Int32 Contract_ID { get; set; }



    }
}
