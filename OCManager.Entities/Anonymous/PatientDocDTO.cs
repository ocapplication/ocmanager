﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class PatientDocDTO
    {
        public Int32 ID { get; set; }
        public string DocName { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public DateTime Date { get; set; }
        public string url { get; set; }
        public string MimeType { get; set; }     
        public Nullable<int> Pages { get; set; }
        public Nullable<int> Owner { get; set; }
        public DateTime Revision { get; set; }
        public int PatientID { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
