﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class PatientNumberDTO
    {
        public int NumberID { get; set; }
        public string NumberType { get; set; }
        public string PatientNote { get; set; }
        public int PatientNumber { get; set; }
    }
}
