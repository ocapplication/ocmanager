﻿using System;
using System.Collections.Generic;
using OCManager.Entities.DBEntities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;

namespace OCManager.Entities.Anonymous
{
    public class PatientCardDTO
    {
        public int PatientID { get; set; }
        public string PatientFName { get; set; }
        public string PatientLName { get; set; }
        public int Age { get; set; }
        public String DOB { get; set; }
        public string Sex { get; set; }
        public string Photo { get; set; }
        public string RegistrationLocation { get; set; }
        public int RegistrationLocationID { get; set; }
        public string Source { get; set; }
        public string RegistrationDate { get; set; }
        public int PatientCardNumber { get; set; }
        public PatientInsurance PatientIns { get; set; }
        public IPagedList<OccurenceHistoryDTO> patienthistory { get; set; }
        public List<OCManager.Entities.Anonymous.FormDataDTO> FormDataList { get; set; }
        public List<Number> PatientNumbers { get; set; }
        public List<Address> PatientAddress { get; set; }
        public List<OCManager.Entities.Anonymous.MenuFormDTO> MenuForms { get; set; }
        public List<PatientNote> PatientNoteList { get; set; }
        public List<PatientContract> PatientContractList { get; set; }
    }
}
