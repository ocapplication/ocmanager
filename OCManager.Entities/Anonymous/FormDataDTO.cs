﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class FormDataDTO
    {
        public int FormID { get; set; }
        public string FormName { get; set; }
        public DateTime LastEdit { get; set; }
        public string UserName { get; set; }
        public int FormDataID { get; set; }

    }
}
