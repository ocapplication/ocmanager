﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class Doctors
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string isDisabled { get; set; }
        public string Color { get; set; }
    }
}
