﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class MenuFormDTO
    {
        public Int32 MenuFormID { get; set; }
        public Int32 MenuID { get; set; }
        public string MenuName { get; set; }
        public Int32 FormID { get; set; }
        public string FormName { get; set; }
    }
}
