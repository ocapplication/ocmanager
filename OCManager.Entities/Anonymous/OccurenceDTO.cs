﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class OccurenceDTO
    {
        public int OccurenceID { get; set; }
        public string Person_Last_Name { get; set; }
        public string Person_First_Name { get; set; }
        public DateTime Person_DOB { get; set; }

        public DateTime Occurence_History_Start { get; set; }
        public DateTime Occurence_History_End { get; set; }

        public int Patient_Rec_Number { get; set; }
        public string UserNickName { get; set; }

        public DateTime TimeStamp { get; set; }
        public string Notes { get; set; }

        public string Number { get; set; }

        public string ReasonCode { get; set; }
        public string PatientNckName { get; set; }

        public string UserNickName1 { get; set; }
        public string ODate { get; set; }

        public string Color { get; set; }
        public int PersonID { get; set; }
    }
}
