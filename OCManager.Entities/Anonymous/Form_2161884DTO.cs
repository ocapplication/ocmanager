﻿using System;
using System.Collections.Generic;
using OCManager.Entities.DBEntities;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class Form_2161884DTO
    {
        public int form_1410518_id { get; set; }
        
        public int? form_data_id { get; set; }
        
        public string Primechanie_k_2051655 { get; set; }        
        public int Id_2051655 { get; set; }
        public string Name_2051655 { get; set; }
        public int Cost_2051655 { get; set; }
        public List<Appointment> Procedure_2051655 { get; set; }

        public string Primechanie_k_2051656 { get; set; }
        public int Id_2051656 { get; set; }
        public string Name_2051656 { get; set; }
        public int Cost_2051656 { get; set; }
        public List<Appointment> Procedure_2051656 { get; set; }

        public string Primechanie_k_2051657 { get; set; }
        public int Id_2051657 { get; set; }
        public string Name_2051657 { get; set; }
        public int Cost_2051657 { get; set; }
        public List<Appointment> Procedure_2051657 { get; set; }

        public string Primechanie_k_2051658 { get; set; }
        public int Id_2051658 { get; set; }
        public string Name_2051658 { get; set; }
        public int Cost_2051658 { get; set; }
        public List<Appointment> Procedure_2051658 { get; set; }

        public string Primechanie_k_1951659 { get; set; }
        public int Id_1951659 { get; set; }
        public string Name_1951659 { get; set; }
        public int Cost_1951659 { get; set; }
        public List<Appointment> Procedure_1951659 { get; set; }

        public string Primechanie_k_1951660 { get; set; }
        public int Id_1951660 { get; set; }
        public string Name_1951660 { get; set; }
        public int Cost_1951660 { get; set; }
        public List<Appointment> Procedure_1951660 { get; set; }

        public string Primechanie_k_1951661 { get; set; }
        public int Id_1951661 { get; set; }
        public string Name_1951661 { get; set; }
        public int Cost_1951661 { get; set; }
        public List<Appointment> Procedure_1951661 { get; set; }

        public string Primechanie_k_1951662 { get; set; }
        public int Id_1951662 { get; set; }
        public string Name_1951662 { get; set; }
        public int Cost_1951662 { get; set; }
        public List<Appointment> Procedure_1951662 { get; set; }

        public string Primechanie_k_1951663 { get; set; }
        public int Id_1951663 { get; set; }
        public string Name_1951663 { get; set; }
        public int Cost_1951663 { get; set; }
        public List<Appointment> Procedure_1951663 { get; set; }

        public string Primechanie_k_1951664 { get; set; }
        public int Id_1951664 { get; set; }
        public string Name_1951664 { get; set; }
        public int Cost_1951664 { get; set; }
        public List<Appointment> Procedure_1951664 { get; set; }

        public string Primechanie_k_1951671 { get; set; }
        public int Id_1951671 { get; set; }
        public string Name_1951671 { get; set; }
        public int Cost_1951671 { get; set; }
        public List<Appointment> Procedure_1951671 { get; set; }

        public string Primechanie_k_1951672 { get; set; }
        public int Id_1951672 { get; set; }
        public string Name_1951672 { get; set; }
        public int Cost_1951672 { get; set; }
        public List<Appointment> Procedure_1951672 { get; set; }

        public string Primechanie_k_1951669 { get; set; }
        public int Id_1951669 { get; set; }
        public string Name_1951669 { get; set; }
        public int Cost_1951669 { get; set; }
        public List<Appointment> Procedure_1951669 { get; set; }

        public string Primechanie_k_1951670 { get; set; }
        public int Id_1951670 { get; set; }
        public string Name_1951670 { get; set; }
        public int Cost_1951670 { get; set; }
        public List<Appointment> Procedure_1951670 { get; set; }


        public string Primechanie_k_1951665 { get; set; }
        public int Id_1951665 { get; set; }
        public string Name_1951665 { get; set; }
        public int Cost_1951665 { get; set; }
        public List<Appointment> Procedure_1951665 { get; set; }

        public string Primechanie_k_1951666 { get; set; }
        public int Id_1951666 { get; set; }
        public string Name_1951666 { get; set; }
        public int Cost_1951666 { get; set; }
        public List<Appointment> Procedure_1951666 { get; set; }

        public string Primechanie_k_2161880 { get; set; }
        public int Id_2161880 { get; set; }
        public string Name_2161880 { get; set; }
        public int Cost_2161880 { get; set; }
        public List<Appointment> Procedure_2161880 { get; set; }

        public string Primechanie_k_2161881 { get; set; }
        public int Id_2161881 { get; set; }
        public string Name_2161881 { get; set; }
        public int Cost_2161881 { get; set; }
        public List<Appointment> Procedure_2161881 { get; set; }

        public string kommentarii_k_kursu_lecheniya_12 { get; set; }  // changed name

        public int occ_id { get; set; }

        public int occ_id_1 { get; set; }

        public int occ_id_2 { get; set; }

        public int occ_id_3 { get; set; }

        public int occ_id_4 { get; set; }

        public int occ_id_5 { get; set; }

        public int occ_id_6 { get; set; }

        public int occ_id_7 { get; set; }

        public string username { get; set; }

        public int user_id { get; set; }

        public DateTime data { get; set; }
    }


}
