﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class OccurenceHistoryDTO
    {
        public int OccurenceID { get; set; }
        public int PatientID { get; set; }
        public DateTime Start { get; set; }
        public string Notes { get; set; }
        public DateTime End { get; set; }
        public DateTime TimeStamp { get; set; }

        public string Reason { get; set; }
        public string DoctorName { get; set; }
        public string LocationName { get; set; }
        public string PatientName { get; set; }
        public string LastChangedBy { get; set; }
        public bool? hasAttended { get; set; }
    }
}
