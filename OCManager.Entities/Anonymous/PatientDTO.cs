﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class PatientDTO
    {
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public Int64 PhoneNumber { get; set; }
        public int BuildingLocation { get; set; }
        public string RegistrationDate { get; set; }
        public string Source { get; set; }
        public int RecordNumber { get; set; }
        public string Number { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
