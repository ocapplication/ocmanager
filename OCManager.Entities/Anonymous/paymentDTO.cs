﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class paymentDTO
    {
        public DateTime lastEdit { get; set; }
        public string username { get; set; }
        public DateTime created { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public Double sum_paid { get; set; }

        public Double balance { get; set; }
    }
}
