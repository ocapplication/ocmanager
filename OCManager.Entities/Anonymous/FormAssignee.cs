﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Entities.Anonymous
{
    public class FormAssignee
    {
        public int AttendingDoc { get; set; }
        public string Head1 { get; set; }
        public string Head2 { get; set; }
        public string Deputy { get; set; }
        public string Main { get; set; }
        public DateTime Created { get; set; }
        public int SignatureStatus { get; set; }
    }
}
