﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace OCManager.Data
{
    public class OCManagerContext : DbContext
    {
        public OCManagerContext()
            : base("OCManagerConnection")
        {
        }

        //public virtual DbSet<EventTask> EventTasks { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Occurence> Occurences { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Person> Persons { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Patient> Patients { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Number> Numbers { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Address> Address { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PersonNumber> PersonNumbers { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PersonAddress> PersonAddress { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.User> Users { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.OccurenceHistory> OccurenceHistories { get; set; }

        public virtual DbSet<OCManager.Entities.DBEntities.Reason> Reasons { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Room> Rooms { get; set; }

        public virtual DbSet<OCManager.Entities.DBEntities.Form_Data> Form_Datas { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Form> Forms { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.oplata> oplatas { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.storage_string> storage_strings { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Documents> Documents { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.CategoryToDocument> CategoryToDocument { get; set; }

        public virtual DbSet<OCManager.Entities.DBEntities.Buildings> Buildings { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PatientStatistics> PatientStatistics { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.DiscountNote> Notes { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PatientNote> PatientNote { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Category> Category { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.MenuForm> MenuForm { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Form1464314> Form1464314 { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Appointment> Appointments { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Discount> Discount { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Form2161884> Form2161884 { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.Form1242785> Form1242785 { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PatientProviderHistory> PatientProviderHistory { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PatientContract> PatientContract { get; set; }
        public virtual DbSet<OCManager.Entities.Anonymous.PatientDocDTO> PatientDocDTOes { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.storage_string_history> StorageStringHistory { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PatientInHealing> PatientInHealing { get; set; }
        public virtual DbSet<OCManager.Entities.DBEntities.PatientInsurance> PatientInsurance { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Entity<EventTask>().ToTable("tblEvent");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Occurence>().ToTable("occurences");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Person>().ToTable("person");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Patient>().ToTable("patient");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PersonNumber>().ToTable("person_number");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PersonAddress>().ToTable("person_address");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Number>().ToTable("number");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Address>().ToTable("address");
            modelBuilder.Entity<OCManager.Entities.DBEntities.User>().ToTable("user");
            modelBuilder.Entity<OCManager.Entities.DBEntities.OccurenceHistory>().ToTable("occurences_history");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Reason>().ToTable("reasons");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Room>().ToTable("rooms");

            modelBuilder.Entity<OCManager.Entities.DBEntities.oplata>().ToTable("oplata");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Form_Data>().ToTable("form_data");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Form>().ToTable("form");
            modelBuilder.Entity<OCManager.Entities.DBEntities.storage_string>().ToTable("storage_string");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Documents>().ToTable("document");
            modelBuilder.Entity<OCManager.Entities.DBEntities.CategoryToDocument>().ToTable("category_to_document");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Buildings>().ToTable("buildings");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PatientStatistics>().ToTable("patient_statistics");
            modelBuilder.Entity<OCManager.Entities.DBEntities.DiscountNote>().ToTable("note");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PatientNote>().ToTable("patient_note");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Category>().ToTable("category");
            modelBuilder.Entity<OCManager.Entities.DBEntities.MenuForm>().ToTable("menu_form");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Form1464314>().ToTable("form_1464314");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Appointment>().ToTable("appointment");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Discount>().ToTable("discount");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PatientContract>().ToTable("patient_contract");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Form2161884>().ToTable("form_2161884");
            modelBuilder.Entity<OCManager.Entities.DBEntities.Form1242785>().ToTable("form_1242785");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PatientProviderHistory>().ToTable("patient_provider_history");
            modelBuilder.Entity<OCManager.Entities.DBEntities.storage_string_history>().ToTable("storage_string_history");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PatientInHealing>().ToTable("patient_in_healing");
            modelBuilder.Entity<OCManager.Entities.DBEntities.PatientInsurance>().ToTable("patient_insurance");
        }
    }
}
