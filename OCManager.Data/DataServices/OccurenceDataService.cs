﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Interface.DataInterface;
using OCManager.Entities.DBEntities;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Mail;
using System.Data.Entity;
using OCManager.Entities.Anonymous;
using System.Globalization;

namespace OCManager.Data.DataServices
{
    public class OccurenceDataService : EntityFrameworkService, IOccurenceDataService
    {
        OCManagerContext dbContext = null;
        //public OccurenceDataService()
        //{
        //    dbContext = new OCManagerContext();
        //}
        public List<OccurenceDTO> GetOccurencesList(int locationID, string selectedDate)
        {
            var i = DateTime.Now.Date;
            //string newFormat = DateTime.ParseExact(selectedDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("MM'/'dd'/'yyyy");
            DateTime dd = Convert.ToDateTime(selectedDate).Date;
            var res = dbConnection.Occurences
                .Join(dbConnection.OccurenceHistories,
                o => o.ID,
                oh => oh.Oc_ID,
                (o, oh) => new { occ = o, OccHis = oh })
               .GroupJoin(dbConnection.Persons,
                          o_oh => o_oh.OccHis.External_ID,
                          p => p.Person_ID,
                          (o_oh, p) => new { occurence_OcHist = o_oh, person = p.FirstOrDefault() })
               .GroupJoin(dbConnection.Patients,
                          o_oh_p => o_oh_p.person.Person_ID,
                          pat => pat.Person_ID,
                          (o_oh_p, pat) => new { occurence_OccHis_Per = o_oh_p, patient = pat.FirstOrDefault() })
               .GroupJoin(dbConnection.PersonNumbers,
                          o_oh_p_pat => o_oh_p_pat.occurence_OccHis_Per.person.Person_ID,
                          pn => pn.Person_ID,
                          (o_oh_p_pat, pn) => new { occurence_OccHis_Per_pat = o_oh_p_pat, personNumber = pn.FirstOrDefault() })
                .GroupJoin(dbConnection.Numbers,
                           o_oh_p_pat_pn => o_oh_p_pat_pn.personNumber.Number_Id,
                           n => n.Number_Id,
                           (o_oh_p_pat_pn, n) => new { occurence_OccHisPer_pat_pn = o_oh_p_pat_pn, number = n.FirstOrDefault() })
                .GroupJoin(dbConnection.Users,
                            o_oh_p_pat_pn_n => o_oh_p_pat_pn_n.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.Last_Change_ID,
                            u => u.User_ID,
                            (o_oh_p_pat_pn_n, u) => new { occurence_OccHisPer_pat_pn_num = o_oh_p_pat_pn_n, user = u.FirstOrDefault() })
                .GroupJoin(dbConnection.Users,
                           oc_p_pat_pn_p_u => oc_p_pat_pn_p_u.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.occ.User_ID,
                           u1 => u1.User_ID,
                           (o_oh_p_pat_pn_n_u, u1) => new { occurence_OccHisper_pat_pn_num_user = o_oh_p_pat_pn_n_u, user1 = u1.FirstOrDefault() })
              .GroupJoin(dbConnection.Users,
                           o_oh_p_pat_pn_p_uu => o_oh_p_pat_pn_p_uu.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.patient.Default_Provider,
                           u2 => u2.User_ID,
                           (o_oh_p_pat_pn_p_uu, u2) => new { occurence_OccHisper_pat_pn_num_user = o_oh_p_pat_pn_p_uu, user2 = u2.FirstOrDefault() })
                .GroupJoin(dbConnection.Reasons,
                           o_oh_p_pat_pn_p_u_r => o_oh_p_pat_pn_p_u_r.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.Reason_Code,
                           r => r.Reason_ID,
                           (o_oh_p_pat_pn_p_u_r, r) => new { occurence_OccHisper_pat_pn_num_user_rsn = o_oh_p_pat_pn_p_u_r, rsn = r.FirstOrDefault() })

                //.Where(o => o.occurenceper_pat_pn_num_user.occurencePer_pat_pn_num.occurencePer_pat_pn.occurencePer_pat.occurencePer.occurence.Oc_ID == 1867847).ToList();
                .Where(o => o.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.occ.Location_ID == locationID
                            && DbFunctions.TruncateTime(
                            o.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.Start) == dd
                            && o.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.occ.isDeleted == false
                            ).ToList();
            List<OccurenceDTO> occurenceList = new List<OccurenceDTO>();
            foreach (var item in res)
            {
                OccurenceDTO objOccurence = new OccurenceDTO();
                objOccurence.OccurenceID = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.occ.ID;
                objOccurence.Person_First_Name = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.person.First_Name;
                objOccurence.Person_Last_Name = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.person.Last_Name;
                objOccurence.UserNickName = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.user1.Nickname;
                objOccurence.Occurence_History_Start = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.Start;
                objOccurence.Occurence_History_End = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.End;
                objOccurence.Person_DOB = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.person.Date_Of_Birth.Value;
                objOccurence.Patient_Rec_Number = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.patient.Record_Number;
                objOccurence.TimeStamp = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.TimeStamp;
                objOccurence.Number = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.number.number;
                objOccurence.Notes = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.Notes;
                objOccurence.ReasonCode = item.rsn.Reason_Text;
                objOccurence.PatientNckName = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.user1.Nickname;
                objOccurence.UserNickName1 = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.user.Nickname;
                objOccurence.ODate = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.TimeStamp.ToString("dd/MM/yyyy");
                objOccurence.Color = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.user1.Color;
                objOccurence.PersonID = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.person.Person_ID;

                var result = occurenceList.Where(c => c.OccurenceID == objOccurence.OccurenceID).FirstOrDefault();
                if (result != null)
                {
                    DateTime oldDT = result.TimeStamp;
                    DateTime newDT = item.occurence_OccHisper_pat_pn_num_user_rsn.occurence_OccHisper_pat_pn_num_user.occurence_OccHisper_pat_pn_num_user.occurence_OccHisPer_pat_pn_num.occurence_OccHisPer_pat_pn.occurence_OccHis_Per_pat.occurence_OccHis_Per.occurence_OcHist.OccHis.TimeStamp;

                    if (newDT > oldDT)
                    {
                        occurenceList.Remove(result);
                        occurenceList.Add(objOccurence);
                    }
                }
                else
                {
                    occurenceList.Add(objOccurence);
                }
            }

            return occurenceList;
        }

        public List<Reason> GetReasonFromDB()
        {
            var hh = dbConnection.Reasons.ToList();

            return hh;
        }
        public int SavePatientDB(PatientDTO obj)
        {
            Person person = new Person();
            using (var trans = dbConnection.Database.BeginTransaction())
            {

                person.First_Name = obj.FirstName;
                person.Last_Name = obj.LastName;
                person.Middle_Name = obj.MiddleName;
                person.Date_Of_Birth = DateTime.ParseExact(obj.DOB, "MM/dd/yyyy", null);
                person.TS = DateTime.Now;
                int pID = dbConnection.Persons.Max(a => a.Person_ID);
                person.Person_ID = pID + 1;
                dbConnection.Persons.Add(person);
                dbConnection.SaveChanges();

                Patient pat = new Patient();
                pat.Person_ID = person.Person_ID;
                int recNo = dbConnection.Patients.Max(a => a.Record_Number);
                pat.Is_Default_Provider_Primary = 0;
                pat.Default_Provider = 0;
                pat.Employer_Name = "";
                pat.Payer_Id = 0;
                pat.Default_Manager = "";
                pat.Old_Numbs = 0;
                pat.Sequence = 0;
                pat.Discount2015 = 0;
                pat.Record_Number = recNo + 1;
                pat.Ts = DateTime.Now;

                dbConnection.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[patient] ON");

                dbConnection.Patients.Add(pat);
                dbConnection.SaveChanges();

                dbConnection.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[patient] OFF");

                PatientStatistics ps = new PatientStatistics();
                ps.Person_ID = person.Person_ID;
                ps.Registration_Location = obj.BuildingLocation;
                ps.sign_in_date = DateTime.Now;
                dbConnection.PatientStatistics.Add(ps);

                Number n = new Number();
                n.number = Convert.ToString(obj.PhoneNumber);
                int numID = dbConnection.Numbers.Max(a => a.Number_Id);
                n.Number_Id = numID + 1;
                n.Active = 1;
                n.Number_Type = 1;
                n.Notes = "";
                dbConnection.Numbers.Add(n);
                dbConnection.SaveChanges();

                PersonNumber pn = new PersonNumber();
                pn.Number_Id = n.Number_Id;
                pn.Person_ID = person.Person_ID;
                dbConnection.PersonNumbers.Add(pn);

                dbConnection.SaveChanges();

                trans.Commit();
            }

            return person.Person_ID;
        }
        public List<PatientDTO> SearchPatientFromDB(string sc1, string sc2, string sc3)
        {
            int recNo = 0;
            if (!string.IsNullOrEmpty(sc1))
            {
                if (sc1.All(char.IsDigit))
                {
                    recNo = Convert.ToInt32(sc1);
                }
            }
            DateTime dob = new DateTime();
            if (!string.IsNullOrEmpty(sc3))
            {
                dob = Convert.ToDateTime(sc3).Date;
            }
            
            var list = dbConnection.Persons
                .Join(dbConnection.Patients,
                p => p.Person_ID,
                pat => pat.Person_ID,
                (p, pat) => new { person = p, patient = pat }).
                Join(dbConnection.PersonNumbers,
                p_pat => p_pat.person.Person_ID,
                 pn => pn.Person_ID,
                (p_pat, pn) => new { person_Pat = p_pat, personNum = pn }).
                Join(dbConnection.Numbers,
                pn_p_pat => pn_p_pat.personNum.Number_Id,
                n => n.Number_Id,
                (pn_p_pat, n) => new { patNum_per_pat = pn_p_pat, num = n })
                .Select(s => new PatientDTO
                {
                    PersonID = s.patNum_per_pat.person_Pat.person.Person_ID,
                    FirstName = s.patNum_per_pat.person_Pat.person.First_Name,
                    LastName = s.patNum_per_pat.person_Pat.person.Last_Name,
                    DateOfBirth = s.patNum_per_pat.person_Pat.person.Date_Of_Birth,
                    RecordNumber = s.patNum_per_pat.person_Pat.patient.Record_Number,
                    Number = s.num.number
                }).ToList();

            List<PatientDTO> lstPat = new List<PatientDTO>();
            if (!string.IsNullOrEmpty(sc1))
            {
                lstPat = list.Where(a => a.FirstName.Contains(sc1)
                       || a.LastName.Contains(sc1)
                       || a.RecordNumber == recNo ).ToList();
            }
            if (!string.IsNullOrEmpty(sc2))
            {
                lstPat.AddRange(list.Where(a => a.Number.Contains(sc2)));
            }
            if (!string.IsNullOrEmpty(sc3))
            {
                lstPat.AddRange(list.Where(a => a.DateOfBirth == dob));
            }
            List<PatientDTO> finalList = lstPat.Select(s => new PatientDTO
            {
                FirstName = s.FirstName,
                LastName = s.LastName,
                RecordNumber = s.RecordNumber,
                DateOfBirth = s.DateOfBirth,
                PersonID = s.PersonID
            }).ToList();

            var res = finalList.GroupBy(g=>g.PersonID).Select(s=>s.First()).ToList();
            return res;
        }
        public int SavePatientNoteToDB(PatientNote pn)
        {
            pn.Patient_Note_Id = dbConnection.PatientNote.Max(m => m.Patient_Note_Id)+1;
            dbConnection.PatientNote.Add(pn);
            dbConnection.SaveChanges();
            return pn.Patient_Note_Id;

        }
        public void DeletePatientNoteByID(int pnID)
        {
            PatientNote pn = dbConnection.PatientNote.FirstOrDefault(a => a.Patient_Note_Id == pnID);
            dbConnection.PatientNote.Remove(pn);
            dbConnection.SaveChanges();
        }
        public int SendEmailToDoc_SavePatProviderHistory(int patID, int concernedPerID, string username)
        {
            PatientProviderHistory pph = new PatientProviderHistory();
            pph.Patient_ID = patID;
            pph.Username = username;
            pph.Type = "request";
            pph.Default_Provider = 0;
            pph.Date = DateTime.Now;
            pph.To_Mail = dbConnection.Persons.FirstOrDefault(a => a.Person_ID == concernedPerID).Email;
            var res = dbConnection.PatientProviderHistory.Add(pph);
            dbConnection.SaveChanges();
            string response = SendEmailToDoc(pph.To_Mail, "Hi You have been invited as a Patient Supervisor");
            return res.ID;
        }

        public List<PatientProviderHistory>  GetSupervisorDocListFromDB(int patID)
        {
            return dbConnection.PatientProviderHistory.Where(a => a.Patient_ID == patID && a.Type == "assignment").ToList();
        }

        public List<Doctors> GetDoctorList()
        {
            List<Doctors> docs = new List<Doctors>();
            Doctors obj = new Doctors();
            obj.ID = 0;
            obj.Name = "--Select--";
            docs.Add(obj);
            try
            {
                var list = dbConnection.OccurenceHistories.Join(dbConnection.Users, oh => oh.User_ID, u => u.User_ID,
                                                (oh, u) => new Doctors { ID = u.User_ID, Name = u.username, isDisabled = u.Disabled }).Where(x => x.isDisabled == "no")
                                                .GroupBy(a => a.ID).Select(a => a.FirstOrDefault()).OrderBy(s=>s.Name).ToList();
                docs.AddRange(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return docs;
        }

        public List<PatientSearch> GetPatientList()
        {
            List<PatientSearch> patients = dbConnection.OccurenceHistories.Join(dbConnection.Patients, oh => oh.External_ID,
                                        p => p.Person_ID, (oh, p) => new { occurence_history = oh, patient = p })
                            .Join(dbConnection.Persons, oh_pat => oh_pat.patient.Person_ID,
                            per => per.Person_ID,
                          (oh_pat, per) => new { occurenceHis_pat = oh_pat, person = per }) //.Where(a=>a.occurenceHis_pat.patient.Record_Number == rec).ToList()
                          .Select(s => new PatientSearch
                          {
                              Name = s.occurenceHis_pat.patient.Record_Number + " " + s.person.First_Name
                                  + " " + s.person.Last_Name,
                              ID = s.person.Person_ID
                          }).Distinct().OrderBy(o => o.Name).ToList();
            return patients;

            //           select distinct per.first_name + per.last_name + '_' + cast (p.record_number as varchar(100)) as patientName,  
            //   per.person_id as personID from occurences_history oh inner join patient p on 
            //oh.external_id = p.person_id
            //inner join person per on per.person_id = p.person_id

            //order by patientName

        }

        public void SaveAppointment(Occurence oc)
        {
            if (oc.ID > 0)
            {
                OccurenceHistory och = new OccurenceHistory();
                och.Oc_ID = oc.ID;
                och.Start = oc.Start;
                och.End = oc.End;
                och.Notes = oc.Notes;
                och.Location_ID = oc.Location_ID;
                och.User_ID = oc.User_ID;
                och.Last_Change_ID = oc.Last_Change_ID;
                och.External_ID = oc.External_ID;
                och.Reason_Code = oc.Reason_Code;
                och.TimeStamp = oc.TimeStamp;
                och.WalkIn = 0;
                och.isDeleted = false;

                dbConnection.OccurenceHistories.Add(och);

                var occ = dbConnection.Occurences.FirstOrDefault(a => a.ID == oc.ID);
                occ.Location_ID = och.Location_ID;

                dbConnection.SaveChanges();
            }
            else
            {
                using (var trans = dbConnection.Database.BeginTransaction())
                {
                    Occurence newOccurence = new Occurence();
                    newOccurence = dbConnection.Occurences.Add(oc);
                    dbConnection.SaveChanges();

                    OccurenceHistory och = new OccurenceHistory();
                    och.Oc_ID = newOccurence.ID;
                    och.Start = oc.Start;
                    och.End = oc.End;
                    och.Notes = oc.Notes;
                    och.Location_ID = oc.Location_ID;
                    och.User_ID = oc.User_ID;
                    och.Last_Change_ID = oc.Last_Change_ID;
                    och.External_ID = oc.External_ID;
                    och.Reason_Code = oc.Reason_Code;
                    och.TimeStamp = oc.TimeStamp;
                    och.WalkIn = 0;
                    och.isDeleted = false;

                    dbConnection.OccurenceHistories.Add(och);
                    dbConnection.SaveChanges();

                    trans.Commit();
                }
            }

        }

        public OccurenceHistory GetAppointmentById(int appointmentID)
        {
            var appointment = dbConnection.OccurenceHistories.Where(och => och.Oc_ID == appointmentID).OrderByDescending(o => o.TimeStamp).FirstOrDefault();
            //var name = dbConnection.OccurenceHistories.Join(dbConnection.Patients, oh =>oh.External_ID,
            //            p =>p.Person_ID, (oh,p) => new {occurence_history= oh, patient = p})
            //            .Join(dbConnection.Persons )
            return appointment;
        }

        public void DeleteAppointment(int appID)
        {
            Occurence oc = dbConnection.Occurences.Find(appID);
            oc.isDeleted = true;
            OccurenceHistory och = dbConnection.OccurenceHistories.Where(oh => oh.Oc_ID == oc.ID && oh.isDeleted == false).ToList().OrderByDescending(o => o.Oc_H_ID).FirstOrDefault();
            och.isDeleted = true;
            dbConnection.SaveChanges();

        }
        public List<Room> GetRooms()
        {
            List<Room> rooms = dbConnection.Rooms.ToList();
            return rooms;

        }

        public List<OccurenceHistoryDTO> GetAppointmentHistoryByID(int ID)
        {
            List<OccurenceHistoryDTO> ohList = dbConnection.Occurences.Join(dbConnection.Patients, o => o.External_ID,
                                        p => p.Person_ID, (o, p) => new { occurence = o, patient = p })
                            .Join(dbConnection.Persons, oh_pat => oh_pat.patient.Person_ID,
                            per => per.Person_ID,
                          (oh_pat, per) => new { occurenceHis_pat = oh_pat, persion = per })
                          .Join(dbConnection.Reasons, oh_pat_per => oh_pat_per.occurenceHis_pat.occurence.Reason_Code,
                          r => r.Reason_ID,
                          (oh_pat_per, r) => new { occurenceHis_pat_per = oh_pat_per, reason = r })
                          .Join(dbConnection.Users, oh_pat_per_rsn => oh_pat_per_rsn.occurenceHis_pat_per.occurenceHis_pat.occurence.User_ID,
                          u => u.User_ID,
                          (oh_pat_per_usr, u) => new { occurenceHis_pat_per_usr = oh_pat_per_usr, user = u })
                          .Join(dbConnection.Rooms, oh_pat_per_rsn_usr => oh_pat_per_rsn_usr.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.Location_ID,
                          l => l.ID,
                          (oh_pat_per_usr_loc, l) => new { occurenceHis_pat_per_usr_loc = oh_pat_per_usr_loc, location = l })
                           .Join(dbConnection.Users, oh_pat_per_rsn_usr_loc => oh_pat_per_rsn_usr_loc.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.Last_Change_ID,
                          u1 => u1.User_ID,
                          (oh_pat_per_usr_loc_usr1, u1) => new { occurenceHis_pat_per_usr_loc_usr1 = oh_pat_per_usr_loc_usr1, usr1 = u1 })
                          .Select(s => new OccurenceHistoryDTO
                          {
                              Notes = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.Notes,
                              Start = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.Start,
                              End = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.End,
                              Reason = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.reason.Reason_Text,
                              DoctorName = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.user.Nickname,
                              LocationName = s.occurenceHis_pat_per_usr_loc_usr1.location.name,
                              PatientName = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.patient.Record_Number
                                            + " " + s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.persion.First_Name
                                            + " " + s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.persion.Last_Name,
                              LastChangedBy = s.usr1.Nickname,
                              TimeStamp = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.TimeStamp,
                              OccurenceID = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.ID,
                              PatientID = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.persion.Person_ID,
                              hasAttended = s.occurenceHis_pat_per_usr_loc_usr1.occurenceHis_pat_per_usr_loc.occurenceHis_pat_per_usr.occurenceHis_pat_per.occurenceHis_pat.occurence.isAttended
                          })
                          .Where(x => x.PatientID == ID).Distinct().OrderByDescending(o => o.TimeStamp).ToList();
            return ohList;
        }

        public List<paymentDTO> GetPaymentsByID(int ID)
        {
            //List<paymentDTO> ohList = dbConnection.OccurenceHistories.Join(dbConnection.Patients, oh => oh.External_ID,
            //                            p => p.Person_ID, (oh, p) => new { occurence_history = oh, patient = p })
            //                .Join(dbConnection.Persons, oh_pat => oh_pat.patient.Person_ID,
            //                per => per.Person_ID, (oh_pat, per) => new { occurence_patient = oh_pat, person = per })
            //                .Join(dbConnection.oplatas, ocurence_op => ocurence_op.person.Person_ID,
            //                op => op.patient_ID, (op, o) => new { occurence_person = op, oplatas = o })
            //                .Join(dbConnection.Form_Datas, oh_op => oh_op.oplatas.naznach_ID,
            //                formdata => formdata.form_Data_ID, (fds, fd) => new { occurence_formData = fds, formdata = fd })
            //                .Join(dbConnection.Forms, oh_fds => oh_fds.formdata.form_ID,
            //                form => form.form_ID, (oh_fds, form) => new { oc_formData = oh_fds, frm = form })
            //                .Join(dbConnection.storage_strings, oh_f => oh_f.oc_formData.formdata.form_Data_ID,
            //                ss => ss.foreign_Key, (oh_f, ss) => new { oc_fData = oh_f, storagestring = ss })
            //                .Where(c => c.storagestring.value_Key == "Итог" && c.oc_fData.oc_formData.occurence_formData.occurence_person.occurence_patient.occurence_history.Oc_ID == ID)
            //                //.Join(dbConnectio)
            //                .Select(s => new paymentDTO
            //                {
            //                    lastEdit = s.oc_fData.oc_formData.formdata.last_Edit,
            //                    username = s.oc_fData.oc_formData.formdata.username,
            //                    created = s.oc_fData.oc_formData.occurence_formData.oplatas.created,
            //                    name = s.oc_fData.frm.name,
            //                    value = s.storagestring.value,
            //                    sum_paid = s.oc_fData.oc_formData.occurence_formData.oplatas.sum_payed
            //                    //balance =  Convert.ToDouble(s.storagestring.value) - s.oc_fData.oc_formData.occurence_formData.oplatas.sum_payed
            //                }).ToList();
            List<paymentDTO> ohList = dbConnection.OccurenceHistories.Join(dbConnection.Patients, oh => oh.External_ID,
                            p => p.Person_ID, (oh, p) => new { occurence_history = oh, patient = p })
                .Join(dbConnection.Persons, oh_pat => oh_pat.patient.Person_ID,
                per => per.Person_ID, (oh_pat, per) => new { occurence_patient = oh_pat, person = per })
                .Join(dbConnection.oplatas, ocurence_op => ocurence_op.person.Person_ID,
                op => op.patient_ID, (op, o) => new { occurence_person = op, oplatas = o })
                .Join(dbConnection.Form_Datas, oh_op => oh_op.oplatas.naznach_ID,
                formdata => formdata.form_Data_ID, (fds, fd) => new { occurence_formData = fds, formdata = fd })
                .Join(dbConnection.Forms, oh_fds => oh_fds.formdata.form_ID,
                form => form.form_ID, (oh_fds, form) => new { oc_formData = oh_fds, frm = form })
                .Join(dbConnection.storage_strings, oh_f => oh_f.oc_formData.formdata.form_Data_ID,
                ss => ss.foreign_Key, (oh_f, ss) => new { oc_fData = oh_f, storagestring = ss })
                .Where(c => c.storagestring.value_Key == "Итог" && c.oc_fData.oc_formData.occurence_formData.occurence_person.occurence_patient.occurence_history.Oc_ID == ID)
                //.Join(dbConnectio)
                .Select(s => new paymentDTO
                {
                    lastEdit = s.oc_fData.oc_formData.formdata.last_Edit,
                    username = s.oc_fData.oc_formData.formdata.username,
                    created = s.oc_fData.oc_formData.occurence_formData.oplatas.created,
                    name = s.oc_fData.frm.name,
                    value = s.storagestring.value,
                    sum_paid = s.oc_fData.oc_formData.occurence_formData.oplatas.sum_payed
                                //balance =  Convert.ToDouble(s.storagestring.value) - s.oc_fData.oc_formData.occurence_formData.oplatas.sum_payed
                            }).ToList();

            return ohList;
        }
        public PatientCardDTO GetPatientCardData(int perID)
        {
            var res = dbConnection.Persons
             .Join(dbConnection.Patients,
                        p => p.Person_ID,
                        pat => pat.Person_ID,
                        (p, pat) => new { per = p, patient = pat })
             .Join(dbConnection.PatientStatistics,
                        per_pat => per_pat.per.Person_ID,
                        ps => ps.Person_ID,
                        (per_pat, ps) => new { patient_person = per_pat, patientStatistic = ps })
            .Join(dbConnection.Buildings,
                        per_pat_ps => per_pat_ps.patientStatistic.Registration_Location,
                        b => b.ID,
                        (per_pat_ps, b) => new { per_pat_patientStatistic = per_pat_ps, buildings = b })
             .GroupJoin(dbConnection.Documents,
             per_pat_ps_b => per_pat_ps_b.per_pat_patientStatistic.patientStatistic.Person_ID,
             d => d.Foreign_ID,
             (per_pat_ps_b, d) => new { patient_person_patSta_Build = per_pat_ps_b, doc = d.FirstOrDefault() })
             .Join(dbConnection.CategoryToDocument,
             per_pat_ps_b_d => per_pat_ps_b_d.doc.ID,
             ctd => ctd.Document_ID,
             (per_pat_ps_b_d, ctd) => new { patient_person_patSta_Build_doc = per_pat_ps_b_d, catToDoc = ctd })
             .Join(dbConnection.PatientInsurance,
             per_pat_ps_b_d_ctd => per_pat_ps_b_d_ctd.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patientStatistic.Person_ID,
             pi=>pi.Patient_Id,
             (per_pat_ps_b_d_ctd, pi) => new { patient_person_patSta_Build_doc_catToDoc = per_pat_ps_b_d_ctd, patIns = pi})
              .Where(d => d.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc
              .patient_person_patSta_Build.per_pat_patientStatistic.patientStatistic.Person_ID == perID
              && d.patient_person_patSta_Build_doc_catToDoc.catToDoc.Category_ID == 1065948).FirstOrDefault();

            PatientCardDTO objPC = new PatientCardDTO();
            if (res != null)

            {
                objPC.PatientFName = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patient_person.per.First_Name;
                objPC.PatientLName = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patient_person.per.Last_Name;
                objPC.Photo = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.doc.url;
                objPC.Age = DateTime.Now.Year - res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patient_person.per.Date_Of_Birth.Value.Year;
                objPC.DOB = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patient_person.per.Date_Of_Birth.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                objPC.Sex = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patient_person.per.Gender == 1 ? "Муж" : "Жен";
                objPC.Source = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patientStatistic.Ethnicity;
                objPC.RegistrationLocation = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.buildings.Name;
                objPC.RegistrationLocationID = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.buildings.ID;
                objPC.RegistrationDate = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patientStatistic.sign_in_date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                objPC.PatientCardNumber = res.patient_person_patSta_Build_doc_catToDoc.patient_person_patSta_Build_doc.patient_person_patSta_Build.per_pat_patientStatistic.patient_person.patient.Record_Number;
                objPC.PatientIns = res.patIns;
            }
            return objPC;

            //    select * from occurences oc inner join person p on oc.external_id = p.person_id
            //inner join patient pat on oc.external_id = pat.person_id
            //where oc.id = 1000064749
            //            select * from document 
            //left join category_to_document on document.id=category_to_document.document_id
            //where document.foreign_id=2055192
            //and category_to_document.category_id=1065948
        }

        public List<FormDataDTO> GetFormDataList(int personID)
        {
            var list = dbConnection.Form_Datas.Join(dbConnection.Forms, fd => fd.form_ID, f => f.form_ID,
                (fd, f) => new { form_data = fd, form = f })
                .Where(a => a.form_data.External_ID == personID)
                 .GroupBy(s => new
                 {
                     FormID = s.form.form_ID,
                     FormName = s.form.name,
                     UserName = s.form_data.username
                 })
                    .Select(s => new FormDataDTO
                    {
                        FormID = s.Key.FormID,
                        FormName = s.Key.FormName,
                        FormDataID = s.Max(x => x.form_data.form_Data_ID),
                        LastEdit = s.Max(x => x.form_data.last_Edit),
                        UserName = s.Key.UserName
                    }).OrderByDescending(o => o.LastEdit).ToList();
            return list;
        }

        public List<Number> GetpatientNumberList(int patientID)
        {
            var list = dbConnection.Persons
              .GroupJoin(dbConnection.PersonNumbers,
                         p => p.Person_ID,
                         pn => pn.Person_ID,
                         (p, pn) => new { person = p, personNum = pn })
                         .SelectMany(x => x.personNum.DefaultIfEmpty(),
                         (p, pn) => new { Person = p.person, perNum = pn })
              .GroupJoin(dbConnection.Numbers,
                         p_pn => p_pn.perNum.Number_Id,
                         n => n.Number_Id,
                         (p_pn, n) => new { personNum = p_pn, num = n })
                         .SelectMany(x => x.num.DefaultIfEmpty(),
                         (p_pn, n) => new { per_perNum = p_pn.personNum, num = n })
                        .Where(c => c.per_perNum.Person.Person_ID == patientID).
                        Select(s => s.num).
                        ToList();
            return list;
        }
        public List<Address> GetPatientAddressList(int patientId)
        {
            var list = dbConnection.Persons
              .GroupJoin(dbConnection.PersonAddress,
                         p => p.Person_ID,
                         pa => pa.Person_ID,
                         (p, pa) => new { person = p, personAdd = pa })
                         .SelectMany(x => x.personAdd.DefaultIfEmpty(),
                         (p, pa) => new { Person = p.person, perAdd = pa })
              .GroupJoin(dbConnection.Address,
                         p_pa => p_pa.perAdd.Address_Id,
                         a => a.Address_Id,
                         (p_pa, a) => new { personAdd = p_pa, Add = a })
                         .SelectMany(x => x.Add.DefaultIfEmpty(),
                         (p_pa, a) => new { per_perAdd = p_pa.personAdd, Add = a })
                        .Where(c => c.per_perAdd.Person.Person_ID == patientId).
                        Select(s => s.Add).
                        ToList();
            return list;
        }

        public List<PatientNote> GetPatientNoteListByID(int patID)
        {
            var list = dbConnection.PatientNote.Where(a => a.Patient_ID == patID).ToList();
            return list;
        }
        public List<PatientContract> GetPatientContractListByID(int patID)
        {
            var list = dbConnection.PatientContract.Where(a => a.Patient_ID == patID).ToList();
            return list;
        }
        public List<Buildings> GetAllBuildingsFromDB()
        {
            return dbConnection.Buildings.OrderBy(a=>a.Name).ToList();
        }
        public void SavePatientNote(DiscountNote pn)
        {
            dbConnection.Notes.Add(pn);
            dbConnection.SaveChanges();
        }

        public int SavePatientContactDB(PatientContract pc)
        {
            dbConnection.PatientContract.Add(pc);
            dbConnection.SaveChanges();
            return pc.Contract_ID;
        }
        public int SavePatientInsuranceDB(PatientInsurance pi)
        {
            dbConnection.PatientInsurance.Add(pi);
            dbConnection.SaveChanges();
            return pi.ins_id;
        }
        //******************************Section Forms start*********************************

        public List<MenuFormDTO> GetFormListWithCategory()
        {
            var list = dbConnection.MenuForm.Join(dbConnection.Forms, mf => mf.form_ID, f => f.form_ID,
                (mf, f) => new { menu_form = mf, form = f })
                .Select(s => new MenuFormDTO
                {
                    FormID = s.form.form_ID,
                    FormName = s.form.name,
                    MenuFormID = s.menu_form.menu_Form_ID,
                    MenuID = s.menu_form.Menu_ID,
                }).ToList();
            list.Where(a => a.MenuID == 90).ToList().ForEach(i => i.MenuName = "Медицинские формы");
            list.Where(a => a.MenuID == 116).ToList().ForEach(i => i.MenuName = "Медицинские формы ВОП");
            list.Where(a => a.MenuID == 114).ToList().ForEach(i => i.MenuName = "Назначения ОМС");
            list.Where(a => a.MenuID == 113).ToList().ForEach(i => i.MenuName = "Возвраты");
            list.Where(a => a.MenuID == 112).ToList().ForEach(i => i.MenuName = "Медицинские формы назначений");
            return list.OrderBy(a => a.MenuID).ToList();
        }

        public int UpdatePatientNumberByID(Number objNum, int patID)
        {

            if (objNum.Number_Id == 0)
            {
                Number n = new Number();
                n.Number_Id = dbConnection.Numbers.Max(m => m.Number_Id) + 1;
                n.number = objNum.number;
                n.Active = 1;
                n.Notes = objNum.Notes;
                dbConnection.Numbers.Add(n);

                PersonNumber pn = new PersonNumber();
                pn.Person_ID = patID;
                pn.Number_Id = n.Number_Id;
                dbConnection.PersonNumbers.Add(pn);
            }
            else
            {
                var patnum = dbConnection.Numbers.FirstOrDefault(w => w.Number_Id == objNum.Number_Id);
                patnum.Notes = objNum.Notes;
                patnum.number = objNum.number;
                patnum.Number_Type = objNum.Number_Type;
            }
            dbConnection.SaveChanges();
            return 1;
        }
        public int UpdatePatientAddressByID(Address objAdd, int patID)
        {
            if (objAdd.Address_Id == 0)
            {
                Address add = new Address();
                add.Address_Id = dbConnection.Address.Max(m => m.Address_Id) + 1;
                add.City = objAdd.City;
                add.Line1 = objAdd.Line1;
                add.Line2 = objAdd.Line2;
                add.Notes = objAdd.Notes;
                add.Name = objAdd.Name;
                add.Postal_Code = objAdd.Postal_Code;
                add.Type = objAdd.Type;
                dbConnection.Address.Add(add);

                PersonAddress pa = new PersonAddress();
                pa.Person_ID = patID;
                pa.Address_Id = add.Address_Id;
                dbConnection.PersonAddress.Add(pa);
            }
            else
            {
                var patAdd = dbConnection.Address.FirstOrDefault(a => a.Address_Id == objAdd.Address_Id);
                patAdd.City = objAdd.City;
                patAdd.Type = objAdd.Type;
                patAdd.Line1 = objAdd.Line1;
                patAdd.Line2 = objAdd.Line2;
                patAdd.Notes = objAdd.Notes;
                patAdd.Postal_Code = objAdd.Postal_Code;
                patAdd.Name = objAdd.Name;
            }
            dbConnection.SaveChanges();
            return 1;
        }
        public int DeletePatientNumberByID(int numID)
        {
            var num = dbConnection.Numbers.FirstOrDefault(a => a.Number_Id == numID);
            var pernum = dbConnection.PersonNumbers.FirstOrDefault(a => a.Number_Id == numID);
            dbConnection.Numbers.Remove(num);
            dbConnection.PersonNumbers.Remove(pernum);
            dbConnection.SaveChanges();
            return 1;
        }
        public int DeletePatientAddressByID(int addID)
        {
            var add = dbConnection.Address.FirstOrDefault(a => a.Address_Id == addID);
            var perAdd = dbConnection.PersonAddress.FirstOrDefault(a => a.Address_Id == addID);
            dbConnection.Address.Remove(add);
            dbConnection.PersonAddress.Remove(perAdd);
            dbConnection.SaveChanges();
            return 1;
        }
        public void UpdatePersonByID(PatientDTO p)
        {
            var per = dbConnection.Persons.FirstOrDefault(a => a.Person_ID == p.PersonID);
            per.First_Name = p.FirstName;
            per.Last_Name = p.LastName;
            per.Date_Of_Birth = Convert.ToDateTime(p.DOB);
            per.Gender = p.Gender == "Male" ? 1 : 2;
            dbConnection.SaveChanges();
        }
        public void UpdatePatientStatByID(PatientDTO p)
        {
            var patStat = dbConnection.PatientStatistics.FirstOrDefault(a => a.Person_ID == p.PersonID);
            patStat.Ethnicity = p.Source;
            patStat.sign_in_date = Convert.ToDateTime(DateTime.ParseExact(p.RegistrationDate, "MM'/'dd'/'yyyy", CultureInfo.InvariantCulture).ToString("dd'/'MM'/'yyyy"));
            patStat.Registration_Location = p.BuildingLocation;
            dbConnection.SaveChanges();
        }
        public int SavePatientAttendaceData(int occID, bool val)
        {
            var occ = dbConnection.Occurences.FirstOrDefault(a => a.ID == occID);
            occ.isAttended = val;
            dbConnection.SaveChanges();
            return occ.ID;
        }

        public string SendEmailToDoc(string email, string body)
        {
            MailMessage msg = new MailMessage();

            msg.From = new MailAddress("romirocks0246@gmail.com");
            msg.To.Add(email);
            msg.Subject = "OC Manager " + DateTime.Now.ToString();
            msg.Body = body;
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new NetworkCredential("romirocks0246@gmail.com", "rohit$515");
            client.Timeout = 20000;
            try
            {
                client.Send(msg);
                return "Mail has been successfully sent!";
            }
            catch (Exception ex)
            {
                return "Fail Has error" + ex.Message;
            }
            finally
            {
                msg.Dispose();
            }
        }
    }
}
