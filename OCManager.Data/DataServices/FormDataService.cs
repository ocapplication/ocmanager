﻿using OCManager.Interface.DataInterface;
using System;
using System.Collections.Generic;
using OCManager.Entities.DBEntities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using OCManager.Entities.Anonymous;
using System.Data.Linq;
using System.Net.Mail;
using System.Net;

namespace OCManager.Data.DataServices
{
    public class FormDataService : EntityFrameworkService, IFormDataService
    {
        public void SaveForm1464314(Form1464314 frm, int patID)
        {
            foreach (var propertyInfo in frm.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(frm, null) == null)
                    {
                        propertyInfo.SetValue(frm, string.Empty, null);
                    }
                }
            }
            using (var trans = dbConnection.Database.BeginTransaction())
            {
                Form_Data fd = new Form_Data();
                fd.External_ID = patID;
                fd.form_ID = 1464314;
                fd.last_Edit = DateTime.Now;
                fd.username = frm.Username;
                fd.User_ID = frm.User_Id;
                fd.Omc = 0;
                fd.form_Data_ID = SaveFormData(fd);

                frm.form_data_id = fd.form_Data_ID;
                dbConnection.Form1464314.Add(frm);
                dbConnection.SaveChanges();

                trans.Commit();
            }
        }

        public List<Appointment> ProcListByFrmID(int formid)
        {
            return dbConnection.Appointments.Where(a => a.Form_ID == formid).OrderBy(o => o.Order).ToList();
        }
        public int GetPatientDiscountCalDB(int patID)
        {
            return dbConnection.oplatas.Where(a => a.patient_ID == patID).Sum(s => s.Vozvrat);
        }


        public void SaveOrderToDB(List<string> ids)
        {
            for (int i = 0; i < ids.Count; i++)
            {
                int keyVal = Convert.ToInt32(ids[i]);
                var appointment = dbConnection.Appointments.FirstOrDefault(a => a.ID == keyVal);
                if (appointment != null)
                {
                    appointment.Order = i + 1;
                }
            }
            dbConnection.SaveChanges();
        }
        public Form_2161884DTO GetForm2061884DB()
        {
            Form_2161884DTO obj = new Form_2161884DTO();
            int[] subFormsIds = { 2051655,2051656,2051657,2051658,1951659,1951660,1951661,1951662,1951663,1951664,
                                  1951671,1951672,1951669,1951670,1951665,1951666,2161880,2161881};
            List<Form> frmList = dbConnection.Forms.Where(frm => subFormsIds.Contains(frm.form_ID)).ToList();
            foreach (var item in frmList)
            {
                obj.GetType().GetProperty("Name_" + item.form_ID).SetValue(obj, item.name, null);
                obj.GetType().GetProperty("Cost_" + item.form_ID).SetValue(obj, item.OMC_Price, null);
                obj.GetType().GetProperty("Procedure_" + item.form_ID).SetValue(obj
                    , dbConnection.Appointments.Where(a => a.Form_ID == item.form_ID).ToList(), null);
            }
            return obj;
        }
        public int CheckPatientAttendance(int patID, DateTime selectedDate)
        {
            var res = dbConnection.Occurences.FirstOrDefault(a => a.External_ID == patID
                        && a.Start.Year == selectedDate.Year
                        && a.Start.Month == selectedDate.Month
                        && a.Start.Day == selectedDate.Day
                        );
            if (res != null)
                return 1;
            else
                return 0;
        }

        public int SaveForm1247857DB(List<storage_string> items, string total, int userID, int frmID, int? formDataID, DateTime dateSelected)
        {
            bool isUpdate = false;
            using (var trans = dbConnection.Database.BeginTransaction())
            {
                Form_Data fd = new Form_Data();
                if (formDataID != null && formDataID > 0)
                {
                    //update operation
                    isUpdate = true;
                    Form_Data objfd = dbConnection.Form_Datas.FirstOrDefault(a => a.form_Data_ID == formDataID);
                    objfd.last_Edit = DateTime.Now;
                    objfd.username = HttpContext.Current.User.Identity.Name;
                    objfd.User_ID = userID;
                }
                else
                {
                    fd.External_ID = items[0].foreign_Key;
                    fd.form_ID = frmID;
                    fd.last_Edit = DateTime.Now;
                    fd.username = HttpContext.Current.User.Identity.Name;
                    fd.User_ID = userID;
                    fd.Omc = 0;
                    fd.form_Data_ID = SaveFormData(fd);
                    formDataID = fd.form_Data_ID;
                }

                if (!isUpdate)
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        storage_string ss = new storage_string();
                        ss.Appointment_ID = items[i].Appointment_ID;
                        ss.foreign_Key = formDataID.Value;
                        if (items[i].value_Key.Contains("цена"))
                        {
                            var app = dbConnection.Appointments.FirstOrDefault(a => a.ID == ss.Appointment_ID);
                            ss.procedure_name = app.Name;
                            ss.procedure_price = Convert.ToDecimal(app.Cost);
                            ss.procedure_total = Convert.ToDecimal(app.Cost * Convert.ToDouble(ss.value));
                            ss.discount = "1";
                            ss.order_number = app.Order;
                        }
                        ss.value = items[i].value;
                        ss.value_Key = items[i].value_Key;
                        if(ss.value != null)
                        dbConnection.storage_strings.Add(ss);
                    }
                }
                else
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        string procName = "";
                        bool isProc = false;
                        if (items[i].value_Key.Contains("цена"))
                        {
                            procName = items[i].value_Key.Substring(0, items[i].value_Key.IndexOf("цена"));
                            procName = procName.Substring(procName.IndexOf(' '), procName.Length - procName.IndexOf(' ')).Trim();
                            isProc = true;
                        }
                        else if (items[i].value_Key == "Скидки :" || items[i].value_Key == "Добавления к цене :")
                        {
                            procName = items[i].value_Key;
                        }

                        storage_string ss = dbConnection.storage_strings.FirstOrDefault(s => s.foreign_Key == formDataID.Value
                                            && s.value_Key.Contains(procName));
                        if (ss == null)
                        {
                            ss = new storage_string();
                            ss.Appointment_ID = items[i].Appointment_ID;
                            ss.foreign_Key = formDataID.Value;
                            ss.value = items[i].value;
                            ss.value_Key = items[i].value_Key;
                            if (isProc)
                            {
                                var app = dbConnection.Appointments.FirstOrDefault(a => a.ID == ss.Appointment_ID);
                                ss.procedure_name = app.Name;
                                ss.procedure_price = Convert.ToDecimal(app.Cost);
                                ss.procedure_total = Convert.ToDecimal(app.Cost * Convert.ToDouble(ss.value));
                                ss.discount = "1";
                                ss.order_number = app.Order.Value;
                            }
                            dbConnection.storage_strings.Add(ss);
                            dbConnection.Entry(ss).State = EntityState.Added;
                        }
                        else
                        {
                            if (ss.value != items[i].value)
                            {
                                SaveStorageStringHistory(ss, items[i].value, ss.value_Key);
                                ss.value = items[i].value;
                                if (isProc)
                                {
                                    var app = dbConnection.Appointments.FirstOrDefault(a => a.ID == ss.Appointment_ID);
                                    ss.procedure_name = app.Name;
                                    ss.procedure_price = Convert.ToDecimal(app.Cost);
                                    ss.procedure_total = Convert.ToDecimal(app.Cost * Convert.ToDouble(ss.value));
                                    ss.discount = "1";
                                    ss.order_number = app.Order.Value;
                                }
                                dbConnection.storage_strings.Add(ss);
                                dbConnection.Entry(ss).State = EntityState.Modified;
                            }
                        }
                    }
                }
                if (!isUpdate)
                {
                    storage_string ssDate = new storage_string();
                    ssDate.Appointment_ID = 10;
                    ssDate.foreign_Key = fd.form_Data_ID;
                    ssDate.value = Convert.ToString(dateSelected);
                    ssDate.value_Key = "Дата";
                    dbConnection.storage_strings.Add(ssDate);

                    storage_string ssUser = new storage_string();
                    ssUser.Appointment_ID = 10;
                    ssUser.foreign_Key = fd.form_Data_ID;
                    ssUser.value = HttpContext.Current.User.Identity.Name;
                    ssUser.value_Key = "Заполнил";
                    dbConnection.storage_strings.Add(ssUser);

                    storage_string sstot = new storage_string();
                    sstot.Appointment_ID = 10;
                    sstot.foreign_Key = fd.form_Data_ID;
                    sstot.value = total;
                    sstot.value_Key = "Итог";
                    dbConnection.storage_strings.Add(sstot);
                }
                else
                {
                    storage_string ss = dbConnection.storage_strings.FirstOrDefault(a => a.foreign_Key == formDataID
                                           && a.value_Key == "Итог");
                    if (ss.value != total)

                    {
                        SaveStorageStringHistory(ss, total, ss.value_Key);
                        ss.value = total;
                        dbConnection.storage_strings.Add(ss);
                        dbConnection.Entry(ss).State = EntityState.Modified;
                    }
                }

                dbConnection.SaveChanges();

                trans.Commit();
            }
            return formDataID.Value;
        }
        public int SaveStorageStringHistory(storage_string ss, string newVal, string newName)
        {
            storage_string_history ssh = new storage_string_history();
            ssh.foreign_Key = ss.foreign_Key;
            ssh.AppointmentID = ss.Appointment_ID;
            ssh.NameKeyNew = newName;
            ssh.NameKeyOld = ss.value_Key;
            ssh.ValueKeyNew = newVal;
            ssh.ValueKeyOld = ss.value;
            ssh.DateOfChange = DateTime.Now;
            dbConnection.StorageStringHistory.Add(ssh);
            return 1;
        }
        public int Save2161884DB(Form_2161884DTO obj, int patID)
        {
            Form2161884 frm = new Form2161884();
            using (var trans = dbConnection.Database.BeginTransaction())
            {
                Form_Data fd = new Form_Data();
                fd.External_ID = patID;
                fd.form_ID = 2161884;
                fd.last_Edit = DateTime.Now;
                fd.username = obj.username;
                fd.User_ID = obj.user_id;
                fd.Omc = 0;
                fd.form_Data_ID = SaveFormData(fd);

                //construct the Form2161884 from DTO
                frm.form_data_id = fd.form_Data_ID;
                frm.Id_1951659 = obj.Id_1951659;
                frm.Id_2051655 = obj.Id_2051655;
                frm.Id_2051656 = obj.Id_2051656;
                frm.Id_2051657 = obj.Id_2051657;
                frm.Id_2051658 = obj.Id_2051658;
                frm.Id_1951659 = obj.Id_1951659;
                frm.Id_1951660 = obj.Id_1951660;
                frm.Id_1951661 = obj.Id_1951661;
                frm.Id_1951662 = obj.Id_1951662;
                frm.Id_1951663 = obj.Id_1951663;
                frm.Id_1951664 = obj.Id_1951664;
                frm.Id_1951671 = obj.Id_1951671;
                frm.Id_1951672 = obj.Id_1951672;
                frm.Id_1951669 = obj.Id_1951669;
                frm.Id_1951670 = obj.Id_1951670;
                frm.Id_1951665 = obj.Id_1951665;
                frm.Id_1951666 = obj.Id_1951666;
                frm.Id_2161880 = obj.Id_2161880;
                frm.Id_2161881 = obj.Id_2161881;


                frm.Primechanie_k_1951659 = obj.Primechanie_k_1951659 == null ? "" : obj.Primechanie_k_1951659;
                frm.Primechanie_k_2051655 = obj.Primechanie_k_2051655 == null ? "" : obj.Primechanie_k_2051655;
                frm.Primechanie_k_2051656 = obj.Primechanie_k_2051656 == null ? "" : obj.Primechanie_k_2051656;
                frm.Primechanie_k_2051657 = obj.Primechanie_k_2051657 == null ? "" : obj.Primechanie_k_2051657;
                frm.Primechanie_k_2051658 = obj.Primechanie_k_2051658 == null ? "" : obj.Primechanie_k_2051658;
                frm.Primechanie_k_1951659 = obj.Primechanie_k_1951659 == null ? "" : obj.Primechanie_k_1951659;
                frm.Primechanie_k_1951660 = obj.Primechanie_k_1951660 == null ? "" : obj.Primechanie_k_1951660;
                frm.Primechanie_k_1951661 = obj.Primechanie_k_1951661 == null ? "" : obj.Primechanie_k_1951661;
                frm.Primechanie_k_1951662 = obj.Primechanie_k_1951662 == null ? "" : obj.Primechanie_k_1951662;
                frm.Primechanie_k_1951663 = obj.Primechanie_k_1951663 == null ? "" : obj.Primechanie_k_1951663;
                frm.Primechanie_k_1951664 = obj.Primechanie_k_1951664 == null ? "" : obj.Primechanie_k_1951664;
                frm.Primechanie_k_1951671 = obj.Primechanie_k_1951671 == null ? "" : obj.Primechanie_k_1951671;
                frm.Primechanie_k_1951672 = obj.Primechanie_k_1951672 == null ? "" : obj.Primechanie_k_1951672;
                frm.Primechanie_k_1951669 = obj.Primechanie_k_1951669 == null ? "" : obj.Primechanie_k_1951669;
                frm.Primechanie_k_1951670 = obj.Primechanie_k_1951670 == null ? "" : obj.Primechanie_k_1951670;
                frm.Primechanie_k_1951665 = obj.Primechanie_k_1951665 == null ? "" : obj.Primechanie_k_1951665;
                frm.Primechanie_k_1951666 = obj.Primechanie_k_1951666 == null ? "" : obj.Primechanie_k_1951666;
                frm.Primechanie_k_2161880 = obj.Primechanie_k_2161880 == null ? "" : obj.Primechanie_k_2161880;
                frm.Primechanie_k_2161881 = obj.Primechanie_k_2161881 == null ? "" : obj.Primechanie_k_2161881;

                frm.kommentarii_k_kursu_lecheniya_12 = obj.kommentarii_k_kursu_lecheniya_12 == null? "" : obj.kommentarii_k_kursu_lecheniya_12;
                frm.occ_id = obj.occ_id;
                frm.occ_id_1 = obj.occ_id_1;
                frm.occ_id_2 = obj.occ_id_2;
                frm.occ_id_3 = obj.occ_id_3;
                frm.occ_id_4 = obj.occ_id_4;
                frm.occ_id_5 = obj.occ_id_5;
                frm.occ_id_6 = obj.occ_id_6;
                frm.occ_id_7 = obj.occ_id_7;
                frm.username = obj.username;
                frm.user_id = obj.user_id;
                frm.data = DateTime.Now;
                //end construction of 2161884

                dbConnection.Form2161884.Add(frm);
                dbConnection.SaveChanges();

                trans.Commit();
            }
            return frm.form_data_id.Value;
        }

        public int SavePatHeilingDB(PatientInHealing patData)
        {
            string nickName = dbConnection.Users.FirstOrDefault(a => a.username == patData.Who_Filled_Username).Nickname;
            patData.Who_Filled_Nickname = nickName;
            dbConnection.PatientInHealing.Add(patData);
            dbConnection.SaveChanges();
            return patData.ID_In_Healing;
        }

        public void SaveBaseForm1247857DB(List<Appointment> lstApp)
        {
            lstApp.Where(a => a.Type == "0").ToList().ForEach(i => i.Type = "single");
            lstApp.Where(a => a.Type == "1").ToList().ForEach(i => i.Type = "multi");
            lstApp.Where(a => a.Type == "2").ToList().ForEach(i => i.Type = "title");
            lstApp = lstApp.Where(a => a.Cost > 0 && a.Name != "").ToList();
            var newlyAdded = lstApp.Where(a => a.ID == 0).ToList();
            List<Appointment> filteredLst = lstApp.Where(a => a.ID > 0).OrderBy(o => o.ID).ToList();
            List<Appointment> dbAppList = ProcListByFrmID(1247857).OrderBy(o => o.ID).ToList();

            dbAppList.Where(a => a.Ordinary_Discount == "").ToList().ForEach(i => i.Ordinary_Discount = "0");
            dbAppList.Where(a => a.Second_Treatment_Discount == "").ToList().ForEach(i => i.Second_Treatment_Discount = "0");
            dbAppList.Where(a => a.story_discount == "").ToList().ForEach(i => i.story_discount = "0");
            dbAppList.Where(a => a.First_Consult_Bonus == "").ToList().ForEach(i => i.First_Consult_Bonus = "0");
            dbAppList.Where(a => a.Certificate_Payment == "").ToList().ForEach(i => i.Certificate_Payment = "0");


            //List<Appointment> lstModified = new List<Appointment>();
            List<Appointment> lstModified = filteredLst.Except(dbAppList, new AppointmentEqualityComparer()).ToList();
            using (var context = new OCManagerContext())
            {
                foreach (var row in newlyAdded)
                {
                    row.Form_ID = 1247857;
                    row.TS = DateTime.Now;
                    if (row.Ordinary_Discount == null)
                        row.Ordinary_Discount = "0";
                    if (row.story_discount == null)
                        row.story_discount = "0";
                    if (row.First_Consult_Bonus == null)
                        row.First_Consult_Bonus = "0";
                    if (row.Certificate_Payment == null)
                        row.Certificate_Payment = "0";
                    if (row.Second_Treatment_Discount == null)
                        row.Second_Treatment_Discount = "0";
                    context.Entry(row).State = EntityState.Added;
                }
                foreach (var row1 in lstModified)
                {
                    row1.TS = DateTime.Now;
                    row1.Form_ID = 1247857;
                    //var item = context.Appointments.Find(row1.ID);
                    //item.Name = row1.Name;
                    //item.Cost = row1.Cost;
                    //item.Ordinary_Discount = row1.Ordinary_Discount;
                    //item.First_Consult_Bonus = row1.First_Consult_Bonus;
                    //item.Type = row1.Type;
                    //item.story_discount = row1.story_discount;
                    //item.Second_Treatment_Discount = row1.Second_Treatment_Discount;
                    //item.Certificate_Payment = row1.Certificate_Payment;
                    //item.Order = row1.Order;
                    context.Entry(row1).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }
        public int SaveFormData(Form_Data frm)
        {
            dbConnection.Form_Datas.Add(frm);
            dbConnection.SaveChanges();
            return frm.form_Data_ID;
        }
        public List<storage_string> GetSavedFormData(int fdID)
        {
            return dbConnection.storage_strings.Where(a => a.foreign_Key == fdID).ToList();
        }

        public List<Discount> GetDiscountFromDB()
        {
            return dbConnection.Discount.Where(a => a.Value != 0).OrderBy(o => o.Value).ToList();
        }
        public List<FormDataDTO> GetAllPatientFormDataByID(int patID)
        {
            var frmList = dbConnection.Form_Datas.Join(dbConnection.Forms, fd => fd.form_ID, f => f.form_ID,
                (fd, f) => new { form_data = fd, form = f })
                .Where(a => a.form_data.External_ID == patID)
                 .GroupBy(s => new
                 {
                     FormID = s.form.form_ID,
                     FormName = s.form.name,
                 }).Select(s => new FormDataDTO
                 {
                     FormID = s.Key.FormID,
                     FormName = s.Key.FormName,
                     FormDataID = s.Max(x => x.form_data.form_Data_ID),
                     LastEdit = s.Max(x => x.form_data.last_Edit),
                 }).OrderByDescending(o => o.LastEdit).ToList();


            return frmList;
        }

        public List<FormDataDTO> GetPatientFormDataByID(int patID, string from, string to)
        {
            DateTime fromDate = Convert.ToDateTime(from);
            DateTime toDate = Convert.ToDateTime(to);
            var frmList = dbConnection.Form_Datas.Join(dbConnection.Forms, fd => fd.form_ID, f => f.form_ID,
                (fd, f) => new { form_data = fd, form = f })
                .Where(a => a.form_data.External_ID == patID)
                 .GroupBy(s => new
                 {
                     FormID = s.form.form_ID,
                     FormName = s.form.name,
                 }).Select(s => new FormDataDTO
                 {
                     FormID = s.Key.FormID,
                     FormName = s.Key.FormName,
                     FormDataID = s.Max(x => x.form_data.form_Data_ID),
                     LastEdit = s.Max(x => x.form_data.last_Edit),
                 }).Where(a=>a.LastEdit > fromDate && a.LastEdit < toDate).OrderByDescending(o => o.LastEdit).ToList();


            return frmList;
        }

        public Form1242785 GetAssignedPersonFromDB(int patID, int userID)
        {
            int perId = dbConnection.Users.FirstOrDefault(u => u.User_ID == userID).Person_Id;
            string pID = Convert.ToString(perId);
            var res = dbConnection.Form1242785.Where(a => a.Customer == patID).OrderByDescending(o => o.create_date).FirstOrDefault();
            if (res != null && res.NextAssignee != "Completed")
            {
                int nextAssigneeID = Convert.ToInt32(res.NextAssignee);
                if (nextAssigneeID == 0)
                {
                    nextAssigneeID = res.Attending_Doctor;
                }
                Person Objdoc = dbConnection.Persons.FirstOrDefault(p => p.Person_ID == nextAssigneeID);
                res.Username = res.Username + "_" + Objdoc.First_Name + " " + Objdoc.Last_Name;
            }
            return res;
        }
        public Form1242785 GetReleaseFormAssigneeDB(int patID)
        {
            Form1242785 objReleasefrm = dbConnection.Form1242785.Where(a => a.Customer == patID).OrderByDescending(o => o.create_date).First();
            return objReleasefrm;
        }

        public List<Doctors> GetDoctorListDB()
        {
            List<Doctors> docs = new List<Doctors>();
            Doctors obj = new Doctors();
            obj.ID = 0;
            obj.Name = "--Select--";
            docs.Add(obj);
            try
            {
                var list = dbConnection.Persons.Join(dbConnection.Users, p => p.Person_ID, u => u.Person_Id,
                                                (per, u) => new Doctors
                                                {
                                                    ID = per.Person_ID
                                                  ,
                                                    Email = per.Email + "_" + per.Person_ID
                                                  ,
                                                    Name = per.Last_Name + " " + per.First_Name
                                                  ,
                                                    isDisabled = u.Disabled
                                                  ,
                                                    Color = u.Color
                                                }).Where(x => x.isDisabled == "no" && x.Color != "")
                                                .GroupBy(a => a.ID).Select(a => a.FirstOrDefault()).OrderBy(a => a.Name).ToList();
                docs.AddRange(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return docs;
        }
        public string SendEmailToAssgnee(string email, string body)
        {
            MailMessage msg = new MailMessage();

            msg.From = new MailAddress("romirocks0246@gmail.com");
            msg.To.Add(email);
            msg.Subject = "OC Manager " + DateTime.Now.ToString();
            msg.Body = body;
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new NetworkCredential("romirocks0246@gmail.com", "rohit$515");
            client.Timeout = 20000;
            try
            {
                client.Send(msg);
                return "Mail has been successfully sent!";
            }
            catch (Exception ex)
            {
                return "Fail Has error" + ex.Message;
            }
            finally
            {
                msg.Dispose();
            }
        }
        public string Save1242785DB(Form1242785 frm, int locID)
        {
            Form1242785 objReleasefrm = dbConnection.Form1242785.Where(a => a.Customer == frm.Customer)
                    .OrderByDescending(o => o.create_date).FirstOrDefault();

            
            //if (objReleasefrm.Signature_Status == null)
            //{
            //    return "0";
            //}
            
            if (frm.Form_Data_ID != null)
            {
                //var frmObj = dbConnection.Form1242785.FirstOrDefault(a => a.Form_Data_ID == frm.Form_Data_ID);

                //frm.Signature_Status = 0;
                if (frm.Signature_Status > 0)
                {
                    frm.Signature_Status = objReleasefrm.Signature_Status + 1;
                    int pID = 0;
                    if (objReleasefrm.Signature_Status == 0)
                    {
                        if (frm.Head1 != "0" && frm.Head1 != null)
                        {
                            frm.NextAssignee = frm.Head1;
                        }
                        else if (frm.Head2 != "0" && objReleasefrm.Head2 != null)
                        {
                            frm.NextAssignee = frm.Head2;
                        }
                        else if (frm.Signature_603615 != null)
                        {
                            frm.NextAssignee = objReleasefrm.Signature_603615;
                        }
                        else if (objReleasefrm.Signature_613915 != null)
                        {
                            frm.NextAssignee = frm.Signature_613915;
                        }
                        else
                        {
                            frm.NextAssignee = "Completed";
                        }
                    }
                    else
                    {
                        int personID = dbConnection.Users.FirstOrDefault(a => a.User_ID == frm.User_ID).Person_Id;
                        if (objReleasefrm.Head1 != null && objReleasefrm.Head1 == Convert.ToString(personID))
                        {
                            if(objReleasefrm.Head2 != null)
                            {
                                frm.NextAssignee = objReleasefrm.Head2;
                            }
                            else if(objReleasefrm.Signature_603615 != null && objReleasefrm.Signature_603615 != "0")
                            {
                                frm.NextAssignee = objReleasefrm.Signature_603615;
                            }
                            else if(objReleasefrm.Signature_613915 != null && objReleasefrm.Signature_613915 != "0")
                            {
                                frm.NextAssignee = objReleasefrm.Signature_613915;
                            }
                            else
                            {
                                frm.NextAssignee = "Completed";
                            }
                        }
                        else if(objReleasefrm.Head2 != null && objReleasefrm.Head2 == Convert.ToString(personID))
                        {
                            if (objReleasefrm.Signature_603615 != null && objReleasefrm.Signature_603615 != "0")
                            {
                                frm.NextAssignee = objReleasefrm.Signature_603615;
                            }
                            else if (objReleasefrm.Signature_613915 != null && objReleasefrm.Signature_613915 != "0")
                            {
                                frm.NextAssignee = objReleasefrm.Signature_613915;
                            }
                            else
                            {
                                frm.NextAssignee = "Completed";
                            }
                        }
                        else if(objReleasefrm.Signature_603615 != null && objReleasefrm.Signature_603615 == Convert.ToString(personID))
                        {
                            if (objReleasefrm.Signature_613915 != null && objReleasefrm.Signature_613915 != "0")
                            {
                                frm.NextAssignee = objReleasefrm.Signature_613915;
                            }
                            else
                            {
                                frm.NextAssignee = "Completed";
                            }
                        }
                        else
                        {
                            frm.NextAssignee = "Completed";
                        }
                    }
                    string mailText = "Release Document has been assigned to you. Please review and sign.";
                    if (frm.NextAssignee == "Completed")
                    {
                        // when completed operator shud be notified via email.
                        int uID = dbConnection.Form1242785.FirstOrDefault(a => a.Attending_Doctor == frm.Attending_Doctor
                              && a.Form_Data_ID == frm.Form_Data_ID && a.Tip_Vipiski != null && a.Tekst_Vipiski_0 == null).User_ID;
                        pID = dbConnection.Users.FirstOrDefault(a => a.User_ID == uID).Person_Id;
                        mailText = "Release document has beem complted by all";
                    }
                    else
                    {
                        pID = Convert.ToInt32(frm.NextAssignee);
                    }
                    string assigneeEmail = dbConnection.Persons.FirstOrDefault(a => a.Person_ID == pID).Email;
                    SendEmailToAssgnee(assigneeEmail, mailText);
                }
                if(frm.Signature_Status == null)
                {
                    // Next assignee ID will be same if signature status is null i.e. logged in member has not signed.
                    int nextAssignee = dbConnection.Users.FirstOrDefault(a => a.User_ID == frm.User_ID).Person_Id;
                    frm.Signature_Status = objReleasefrm.Signature_Status;
                    frm.NextAssignee = Convert.ToString(nextAssignee);
                }
                //frm.Head1 = objReleasefrm.Head1;
                //frm.Head2 = objReleasefrm.Head2;
                frm.create_date = DateTime.Now;
                //frm.Signature_603615 = objReleasefrm.Signature_603615;
                //frm.Signature_613915 = objReleasefrm.Signature_613915;

                dbConnection.Form1242785.Add(frm);
                dbConnection.SaveChanges();
                return "1";
            }
            else
            {
                if (objReleasefrm == null || objReleasefrm.NextAssignee == "Completed")
                {
                    using (var trans = dbConnection.Database.BeginTransaction())
                    {
                        Form_Data fd = new Form_Data();
                        fd.External_ID = Convert.ToInt32(frm.Customer);
                        fd.form_ID = 1242785;
                        fd.last_Edit = DateTime.Now;
                        fd.username = frm.Username;
                        fd.User_ID = frm.User_ID;
                        fd.Omc = 0;
                        fd.form_Data_ID = SaveFormData(fd);

                        frm.Form_Data_ID = fd.form_Data_ID;
                        string assigneeEmail = dbConnection.Persons.FirstOrDefault(a => a.Person_ID == frm.Attending_Doctor).Email;
                        SendEmailToAssgnee(assigneeEmail, "Release Document has been assigned to you. Please review and sign.");
                        int res = SaveOccurenceToDB(frm.User_ID, locID, frm.Customer, frm.Attending_Doctor);
                        frm.NextAssignee = Convert.ToString(frm.Attending_Doctor);
                        frm.Signature_Status = 0;
                        dbConnection.Form1242785.Add(frm);
                        dbConnection.SaveChanges();

                        trans.Commit();
                    }
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }

        public int RejectReleaseFormDB(int patID, int uID, string textArea)
        {
            int pID = dbConnection.Users.FirstOrDefault(a => a.User_ID == uID).Person_Id;
            string personID = Convert.ToString(pID);
            var res = dbConnection.Form1242785.FirstOrDefault(a => a.Customer == patID && a.NextAssignee == personID);
            res.NextAssignee = Convert.ToString(res.Attending_Doctor);
            res.Signature_Status = 0;
            res.create_date = DateTime.Now;
            res.Tekst_Vipiski_0 = textArea;
            dbConnection.Form1242785.Add(res);
            dbConnection.SaveChanges();
            return res.Form_1242785_ID;
        }
        public Person GetPeronFromDB(int perID)
        {
            return dbConnection.Persons.FirstOrDefault(a => a.Person_ID == perID);
        }
        public int SaveOccurenceToDB(int uID, int locID, int assID, int patID)
        {
            Occurence oc = new Occurence();
            Occurence oc1 = new Occurence();

            oc.Start = DateTime.Today.AddHours(9);
            oc.End = DateTime.Today.AddHours(9).AddMinutes(15);
            oc.Notes = "Ordering";
            oc.Location_ID = locID;
            oc.Event_ID = 12345;
            oc.User_ID = 1046495;
            oc.Last_Change_ID = assID;
            oc.External_ID = patID;
            oc.Reason_Code = 7;
            oc.TimeStamp = DateTime.Now;
            oc.WalkIn = 0;
            oc.Group_Appointment = 0;
            oc.isDeleted = false;

            oc1.Start = DateTime.Today.AddDays(3).AddHours(9);
            oc1.End = DateTime.Today.AddDays(3).AddHours(9).AddMinutes(15);
            oc1.Notes = "Ordering";
            oc1.Location_ID = locID;
            oc1.Event_ID = 12345;
            oc1.User_ID = 1046495;
            oc1.Last_Change_ID = assID;
            oc1.External_ID = patID;
            oc1.Reason_Code = 7;
            oc1.TimeStamp = DateTime.Now;
            oc1.WalkIn = 0;
            oc1.Group_Appointment = 0;
            oc1.isDeleted = false;

            dbConnection.Occurences.Add(oc);
            dbConnection.Occurences.Add(oc1);

            OccurenceHistory och = new OccurenceHistory();
            OccurenceHistory och1 = new OccurenceHistory();

            och.Oc_ID = oc.ID;
            och.Start = oc.Start;
            och.End = oc.End;
            och.Notes = oc.Notes;
            och.Location_ID = oc.Location_ID;
            och.User_ID = oc.User_ID;
            och.Last_Change_ID = oc.Last_Change_ID;
            och.External_ID = oc.External_ID;
            och.Reason_Code = oc.Reason_Code;
            och.TimeStamp = oc.TimeStamp;
            och.WalkIn = 0;
            och.isDeleted = false;

            och1.Oc_ID = oc1.ID;
            och1.Start = oc1.Start;
            och1.End = oc1.End;
            och1.Notes = oc1.Notes;
            och1.Location_ID = oc1.Location_ID;
            och1.User_ID = oc1.User_ID;
            och1.Last_Change_ID = oc1.Last_Change_ID;
            och1.External_ID = oc1.External_ID;
            och1.Reason_Code = oc1.Reason_Code;
            och1.TimeStamp = oc1.TimeStamp;
            och1.WalkIn = 0;
            och1.isDeleted = false;

            dbConnection.OccurenceHistories.Add(och);
            dbConnection.OccurenceHistories.Add(och1);
            return 1;
        }

        public string GetPatientInfoDB(int perID)
        {
            var res = dbConnection.Persons
                .Join(dbConnection.Patients,
                p => p.Person_ID,
                pat => pat.Person_ID,
                (p, pat) => new { person = p, patient = pat }).FirstOrDefault(a => a.patient.Person_ID == perID);
            DateTime fdate = dbConnection.Form_Datas.FirstOrDefault(a => a.External_ID == perID && a.User_ID != 0).last_Edit;
            DateTime tdate = dbConnection.Form_Datas.OrderByDescending(o=>o.form_Data_ID).FirstOrDefault(a => a.External_ID == perID && a.User_ID != 0).last_Edit;
            string patData = Convert.ToString(res.patient.Record_Number) + "_" + res.person.Last_Name + "_" 
                + res.person.First_Name + "_" + res.person.Date_Of_Birth + "_" + Convert.ToString(fdate) +  "_" + Convert.ToString(tdate);
            return patData;
        }
    }
    public class AppointmentEqualityComparer : IEqualityComparer<Appointment>
    {
        public bool Equals(Appointment x, Appointment y)
        {
            if (Object.ReferenceEquals(x, y)) return true;

            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            return x.ID == y.ID && x.Name == y.Name && x.Type == y.Type
                && x.Ordinary_Discount == y.Ordinary_Discount
                && x.Second_Treatment_Discount == y.Second_Treatment_Discount
                && x.story_discount == y.story_discount
                && x.First_Consult_Bonus == y.First_Consult_Bonus
                && x.Certificate_Payment == y.Certificate_Payment
                && x.Order == y.Order;
        }


        public int GetHashCode(Appointment app)
        {
            if (Object.ReferenceEquals(app, null)) return 0;

            return app.ID.GetHashCode()
                ^ app.Name.GetHashCode()
                ^ app.Type.GetHashCode()
                ^ app.Ordinary_Discount.GetHashCode()
                ^ app.Second_Treatment_Discount.GetHashCode()
                ^ app.story_discount.GetHashCode()
                ^ app.First_Consult_Bonus.GetHashCode()
                ^ app.Certificate_Payment.GetHashCode()
                ^ app.Order.GetHashCode();

        }
    }
}