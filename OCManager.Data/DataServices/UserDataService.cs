﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Interface.DataInterface;
namespace OCManager.Data.DataServices
{
    public class UserDataService : EntityFrameworkService, IUserDataService
    {
        public Entities.DBEntities.User GetUser(string username, string password)
        {
           var user = dbConnection.Users.Where(u=>u.username == username && u.Password == password).First();
           return user;
        }

    }
}
