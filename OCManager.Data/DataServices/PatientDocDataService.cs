﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Interface.DataInterface;
using OCManager.Entities.DBEntities;
using OCManager.Entities.Anonymous;
using System.Web;
using System.Data.Entity;

namespace OCManager.Data.DataServices
{
    public class PatientDocDataService : EntityFrameworkService, IPatientDocDataService
    {
        OCManagerContext dbContext = null;
        public List<PatientDocDTO> GetPatientDocList(int patID)
        {

            var list = dbConnection.Documents.Join(dbConnection.CategoryToDocument, d => d.ID,
                                        ctd => ctd.Document_ID, (d, ctd) => new { doc = d, CatToDoc = ctd })
                                        .Join(dbConnection.Category, d_ctd => d_ctd.CatToDoc.Category_ID,
                                        c => c.ID, (d_ctd, c) => new { doc_CatToDoc = d_ctd, cat = c })
                                      .Where(a => a.doc_CatToDoc.doc.Foreign_ID == patID).ToList();

            List<PatientDocDTO> listPatientDoc = null;
            if (list.Count > 0)
            {
                listPatientDoc = new List<PatientDocDTO>();
                foreach (var s in list)
                {
                    PatientDocDTO obj = new PatientDocDTO();

                    obj.CategoryID = s.cat.ID;
                    obj.CategoryName = s.cat.Name;
                    obj.ID = s.doc_CatToDoc.doc.ID;
                    obj.Date = s.doc_CatToDoc.doc.Date;
                    obj.MimeType = s.doc_CatToDoc.doc.MimeType;
                    obj.Owner = s.doc_CatToDoc.doc.Owner;
                    obj.Pages = s.doc_CatToDoc.doc.Pages;
                    obj.Revision = s.doc_CatToDoc.doc.Revision;
                    obj.Size = s.doc_CatToDoc.doc.Size;
                    obj.Type = s.doc_CatToDoc.doc.Type;
                    obj.DocName = s.doc_CatToDoc.doc.Name;
                    obj.PatientID = s.doc_CatToDoc.doc.Foreign_ID;
                    obj.url = s.doc_CatToDoc.doc.url;
                    listPatientDoc.Add(obj);
                }
            }
            return listPatientDoc != null ? listPatientDoc.OrderBy(a => a.CategoryID).ToList() : null;
        }
        public List<Category> GetCatListDB()
        {
            return dbConnection.Category.ToList();
        }

        public void SaveDocumentCategoryDB(HttpPostedFileBase file, int catID, string path, int patientID, int userID)
        {
            using (var trans = dbConnection.Database.BeginTransaction())
            {
                Documents doc = new Documents();
                doc.Name = file.FileName;
                doc.Size = file.ContentLength;
                doc.Date = DateTime.Now;
                doc.url = path;
                doc.MimeType = file.ContentType;
                doc.Pages = null;
                doc.Owner = userID;
                doc.Revision = DateTime.Now;
                doc.Type = "file_url";
                doc.Foreign_ID = patientID;
                doc.Group_ID = 1;
                doc = dbConnection.Documents.Add(doc);

                dbConnection.SaveChanges();

                CategoryToDocument CatToDoc = new CategoryToDocument();
                CatToDoc.Category_ID = catID;
                CatToDoc.Document_ID = doc.ID;

                dbConnection.CategoryToDocument.Add(CatToDoc);
                dbConnection.SaveChanges();

                trans.Commit();
            }
        }

        public int updateDocumentToDB(int docID, int catID, string docName)
        {
            Documents doc = new Documents();
            CategoryToDocument ctd = new CategoryToDocument();

            using (var trans = dbConnection.Database.BeginTransaction())
            {
                doc = dbConnection.Documents.Where(d => d.ID == docID).FirstOrDefault();
                ctd = dbConnection.CategoryToDocument.Where(c => c.Document_ID == docID).FirstOrDefault();
                doc.Name = docName;
                dbConnection.CategoryToDocument.Remove(ctd);

                CategoryToDocument CatToDoc = new CategoryToDocument();
                CatToDoc.Category_ID = catID;
                CatToDoc.Document_ID = doc.ID;

                dbConnection.CategoryToDocument.Add(CatToDoc);
                dbConnection.SaveChanges();
                trans.Commit();
            }
            return doc.ID;
        }
    }
}
