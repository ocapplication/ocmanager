﻿using System;
using System.Collections.Generic;
using OCManager.Entities.DBEntities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Entities.Anonymous;

namespace OCManager.Interface.DataInterface
{
    public interface IFormDataService : IDataRepository
    {
        void SaveForm1464314(Form1464314 frm, int patID);
        List<Appointment> ProcListByFrmID(int formid);
        void SaveOrderToDB(List<string> ids);
        List<Discount> GetDiscountFromDB();
        int GetPatientDiscountCalDB(int patID);
        int SaveForm1247857DB(List<storage_string> items, string total, int userID, int frmID, int? formDataID, DateTime dateSelected);
        void SaveBaseForm1247857DB(List<Appointment> appList);
        Form_2161884DTO GetForm2061884DB();
        int Save2161884DB(Form_2161884DTO obj, int patID);
        List<storage_string> GetSavedFormData(int fdID);
        List<FormDataDTO> GetAllPatientFormDataByID(int patID);
        List<FormDataDTO> GetPatientFormDataByID(int patID, string from, string to);
        string Save1242785DB(Form1242785 obj, int locID);
        List<Doctors> GetDoctorListDB();
        Form1242785 GetAssignedPersonFromDB(int patID, int userID);
        Form1242785 GetReleaseFormAssigneeDB(int patID);
        Person GetPeronFromDB(int perID);
        int CheckPatientAttendance(int patID, DateTime date);
        int RejectReleaseFormDB(int pID, int uID, string textArea);
        string GetPatientInfoDB(int perID);
        int SavePatHeilingDB(PatientInHealing patData);
    }
}

