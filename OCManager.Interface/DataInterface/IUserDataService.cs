﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Entities.DBEntities;

namespace OCManager.Interface.DataInterface
{
    public interface IUserDataService : IDataRepository
    {
        User GetUser(string username, string password);
    }
}
