﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Entities.Anonymous;
using OCManager.Entities.DBEntities;
using System.Web;

namespace OCManager.Interface.DataInterface
{
    public interface IPatientDocDataService : IDataRepository
    {
        List<PatientDocDTO> GetPatientDocList(int patientID);
        List<Category> GetCatListDB();
        void SaveDocumentCategoryDB(HttpPostedFileBase file, int catID, string path, int patientID, int userID);
        int updateDocumentToDB(int docID, int catID, string docName);
    }
}
