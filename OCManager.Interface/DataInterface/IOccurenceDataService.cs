﻿using OCManager.Entities.Anonymous;
using OCManager.Entities.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Interface.DataInterface
{
    public interface IOccurenceDataService:IDataRepository
    {
        List<OccurenceDTO> GetOccurencesList(int locationID, string selectedDate);
        List<Room> GetRooms();
        List<Reason> GetReasonFromDB();
        List<Doctors> GetDoctorList();
        List<PatientSearch> GetPatientList();
        void SaveAppointment(Occurence oc);
        int SavePatientDB(PatientDTO obj);
        OccurenceHistory GetAppointmentById(int appointmentID);
        void DeleteAppointment(int appID);
        List<OccurenceHistoryDTO> GetAppointmentHistoryByID(int ID);
        List<paymentDTO> GetPaymentsByID(int ID);
        PatientCardDTO GetPatientCardData(int perID);

        List<FormDataDTO> GetFormDataList(int personID);
        void SavePatientNote(DiscountNote pn);
        int SavePatientContactDB(PatientContract pi);
        int SavePatientInsuranceDB(PatientInsurance pi);
        List<Number> GetpatientNumberList(int patientID);
        List<Address> GetPatientAddressList(int patientID);
        List<PatientNote> GetPatientNoteListByID(int patID);
        List<PatientContract> GetPatientContractListByID(int patID);
        List<MenuFormDTO> GetFormListWithCategory();
        int UpdatePatientNumberByID(Number objNum, int patID);
        int UpdatePatientAddressByID(Address objAdd, int patID);
        int DeletePatientNumberByID(int numID);
        int DeletePatientAddressByID(int addID);
        void UpdatePersonByID(PatientDTO p);
        List<Buildings> GetAllBuildingsFromDB();
        void UpdatePatientStatByID(PatientDTO p);
        List<PatientDTO> SearchPatientFromDB(string sc1, string sc2, string sc3);
        int SavePatientNoteToDB(PatientNote pn);
        void DeletePatientNoteByID(int pnID);
        int SendEmailToDoc_SavePatProviderHistory(int patID, int concernedPerID, string username);
        List<PatientProviderHistory> GetSupervisorDocListFromDB(int patID);
        int SavePatientAttendaceData(int id, bool val);
    }
}
