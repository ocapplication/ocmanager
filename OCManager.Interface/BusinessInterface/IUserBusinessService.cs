﻿using OCManager.Entities.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCManager.Interface.BusinessInterface
{
    public interface IUserBusinessService
    {
        User GetUser(string username, string password);
    }
}
