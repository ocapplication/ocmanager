﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OCManager.Entities.DBEntities;
using System.Threading.Tasks;
using OCManager.Entities.Anonymous;

namespace OCManager.Interface.BusinessInterface
{
    public interface IFormBusinessService
    {
        void SaveForm1464314(Form1464314 frm, int patID);
        int FetchDiscountCalculation(int patID);
        List<Appointment> GetProcedureListByFormID(int frmID);
        void SaveOrderIndex(List<string> Ids);
        List<Discount> GetDiscountList();
        List<storage_string> GetStorageStrinfFormData(int fdID);
        int SaveForm1247857(List<storage_string> items, string total, int userID, int frmID, int? formDataID, DateTime dateSelected);
        void SaveBaseForm1247857(List<Appointment> lstApp);
        Form_2161884DTO GetForm2061884Data();
        int Save2161884(Form_2161884DTO obj, int patID);
        List<FormDataDTO> GetAllPatientFormData(int patID);
        List<FormDataDTO> GetPatientFormData(int patID, string from, string to);
        string SaveForm1242785(Form1242785 obj, int LocID);
        List<Doctors> GetDoctorList();
        Form1242785 GetAssignedDoctor(int patID, int userID);
        Form1242785 GetReleaseFormData(int patID, int userID);
        Form1242785 CheckReleaseFormAssignee(int patID);
        Person GetPerson(int perID);
        int CheckAttendaceDate(int patID, DateTime date);
        int RejectReleaseForm(int patID, int uID, string textArea);
        string GetPatientInfo(int personID);
        int SavePatHeiling(PatientInHealing patData);
    }
}
