﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Entities.DBEntities;
using OCManager.Entities.Anonymous;

namespace OCManager.Interface.BusinessInterface
{
    public interface IOccurenceBusinessService
    {
        List<OccurenceDTO> GetOccurencesList(int locationID, string selectedDate);
        List<Reason> GetReasons();
        List<Doctors> GetDoctors();

        List<PatientSearch> GetPatients();
        void SaveAppointment(Occurence oc);
        OccurenceHistory GetAppointmentById(int appointmentID);
        void DeleteAppointment(int appID);

        int SavePatientData(PatientDTO obj);

        List<OccurenceHistoryDTO> GetAppointmentHistory(int ID);

        List<Room> GetRooms();
        List<paymentDTO> GetPaymentList(int ID);
        PatientCardDTO GetPatientCard(int perID);
        List<FormDataDTO> GetFormData(int perID);
        void SavePatientDiscount(DiscountNote oc);
        List<Number> GetpatientNumbers(int patientID);
        List<Address> GetpatientAddress(int patientID);
        List<MenuFormDTO> GetFormList();
        int UpdatePatientNumber(Number objNum, int patID);
        int UpdatePatientAddress(Address objAdd, int patID);
        int DeletePatientNumber(int numID);
        int DeletePatientAddress(int addID);
        void UpdatePerson(PatientDTO p);
        List<Buildings> GetAllBuildings();
        void UpdatePateintStat(PatientDTO p);
        List<PatientNote> GetPatientNoteList(int patID);
        List<PatientContract> GetPatientContractList(int patID);
        List<PatientDTO> SearchPatientList(string sc1, string sc2, string sc3);
        int SavePatientNote(PatientNote pn);
        void DeletePatientNote(int pnID);
        int SendEmailInviteToDoc(int patID, int concernedPerID, string username);
        List<PatientProviderHistory> GetSupervisorDoctorList(int patID);
        int SavePatientContract(PatientContract pc);
        int SavePatientInsurance(PatientInsurance pc);
        int SavePatientAttendace(int ocID, bool val);
    }
}
