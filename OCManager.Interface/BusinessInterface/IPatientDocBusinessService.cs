﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Entities.Anonymous;
using OCManager.Entities.DBEntities;
using System.Web;

namespace OCManager.Interface.BusinessInterface
{
    public interface IPatientDocBusinessService
    {
        List<PatientDocDTO> GetDocumentsByPatientID(int patientID);
        List<Category> GetCategoryList();
        void SaveDocumentUnderCategory(HttpPostedFileBase doc, int CatID, string path, int patientID, int userID);
        int UpdateDocumentData(int docID, int catID, string docName);
    }
}
