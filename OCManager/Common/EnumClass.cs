﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace OCManager.Common
{
    public class EnumClass
    {
        public enum PatientDiscountPriority
        {
            Заметки = 6,
            VIP = 3,
            [Description("Младше 2-х = 4")]
            Младше = 4,
            [Description("Внимание!")]
            Внимание = 5,
            Акция = 6
        }
    }   
}