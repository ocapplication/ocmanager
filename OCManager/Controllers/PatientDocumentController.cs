﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using OCManager.Interface.BusinessInterface;
using Ninject;
using System.IO;

namespace OCManager.Controllers
{
    [Authorize]
    public class PatientDocumentController : Controller
    {
        [Inject]
        public IPatientDocBusinessService _patientDocumentBusinessService { get; set; }
        // GET: PatientDocument
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetPatientDocuments(int id)
        {
            List<OCManager.Entities.Anonymous.PatientDocDTO> patDoc = _patientDocumentBusinessService.GetDocumentsByPatientID(id);
            List<OCManager.Entities.DBEntities.Category> catList = _patientDocumentBusinessService.GetCategoryList();
            List<SelectListItem> catValues = new List<SelectListItem>();
            foreach (var item in catList)
            {
                SelectListItem sl = new SelectListItem();
                sl.Text = item.Name;
                sl.Value = Convert.ToString(item.ID);
                catValues.Add(sl);
            }
            StringBuilder html = new StringBuilder();
            if (patDoc != null)
            {
                for (int i = 0; i < patDoc.Count; i++)
                {
                    string fileProperties = Convert.ToString(patDoc[i].Size) + '$' + patDoc[i].MimeType +
                       '$' + Convert.ToString(patDoc[i].Date) + '$' + patDoc[i].url + '$' + patDoc[i].ID;
                    if (i == 0)
                    {
                        html.Append("<li><label id=" + patDoc[i].CategoryID + " onclick='categoryClick(this);' " +
                            "class='tree-toggler nav-header'>" + patDoc[i].CategoryName + "</label> " +
                              "<ul class='nav nav-list tree' style='display:none;'>  " +
                              "<li><a id=" + patDoc[i].ID + " href ='#' name ='" + fileProperties +
                              "' onclick='imageClick(this);'>" + patDoc[i].DocName + "</a></li> ");
                    }
                    else if (i > 0 && patDoc[i].CategoryID == patDoc[i - 1].CategoryID)
                    {
                        html.Append("<li><a id=" + patDoc[i].ID + " href= '#' name ='" + fileProperties +
                            "' onclick ='imageClick(this);'>" + patDoc[i].DocName + "</a></li>");
                    }
                    else
                    {
                        html.Append("</ul></li><li class='divider'></li> " +
                            "<li><label id=" + patDoc[i].CategoryID + " onclick='categoryClick(this);' " +
                            "class='tree-toggler nav-header'>" + patDoc[i].CategoryName + "</label>" +
                            "<ul class='nav nav-list tree' style='display:none;'> <li><a id=" + patDoc[i].ID +
                            " href='#' name ='" + fileProperties + "' onclick ='imageClick(this);' > "
                             + patDoc[i].DocName + "</a></li>");
                    }
                }
            }
            ViewBag.treeData = html;
            ViewBag.CategoryList = catValues;
            ViewBag.PatientID = id;
            return View();
        }
        [HttpPost]
        public ActionResult UploadFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;

                    //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                    //string filename = Path.GetFileName(Request.Files[i].FileName);
                    string[] catIds = null;
                    if (Request.Form["selectedCat"] != null)
                    {
                         catIds = Request.Form["selectedCat"].Split(',');
                    }
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        //check if path doesn't exists and create
                        if (!Directory.Exists(Server.MapPath("~/PatientDocUploads/")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/PatientDocUploads/"));
                        }
                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/PatientDocUploads/"), fname);
                       
                        // save to database
                        int categoryID = 0;
                        if (Convert.ToInt32(Request.Form["CategoryID"]) > 0)
                        {
                            categoryID = Convert.ToInt32(Request.Form["CategoryID"]);
                        }
                        else
                        {
                            categoryID = Convert.ToInt32(catIds[i]);
                        }
                        _patientDocumentBusinessService.SaveDocumentUnderCategory(file, categoryID, fname, Convert.ToInt32(Request.Form["PatientID"]), Convert.ToInt32(Session["userID"]));
                       
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("Done");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("File Not Found");
            }
        }

        public ActionResult UpdateDocument(int dID, int fID, string dname)
        {
            int res = _patientDocumentBusinessService.UpdateDocumentData(dID, fID, dname);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UploadMultipleDoc()
        {
            return PartialView("_UploadMultiple");
        }
       
    }
}