﻿using Ninject;
using OCManager.Entities.Anonymous;
using OCManager.Entities.DBEntities;
using OCManager.Interface.BusinessInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using PagedList;

namespace OCManager.Controllers
{
    [Authorize]
    //[SessionAuthenticate]
    public class OccurenceManagerController : Controller
    {
        [Inject]
        public IOccurenceBusinessService _occurenceBusinessService { get; set; }
        //
        // GET: /OccurenceManager/
        public ActionResult Index()
        {
            var roomList = _occurenceBusinessService.GetRooms();
            Session["rooms"] = roomList;
            ViewBag.LocationID = Convert.ToInt32(Session["Location"]);
            var reasons = _occurenceBusinessService.GetReasons();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "--Select--", Value = "0", Selected = true });
            foreach (var i in reasons)
            {
                items.Add(new SelectListItem { Text = i.Reason_Text, Value = Convert.ToString(i.Reason_ID) });
            }
            Session["Reasons"] = items;
            Session["docList"] = _occurenceBusinessService.GetDoctors();
            // ViewBag.Patients = _occurenceBusinessService.GetPatients();
            return View();
        }
        [HttpPost]
        public ActionResult Index(string doc, string location, string date, string notes, string reason, string personID, string startTime, string endTime, string appID, string locID)
        {
            Occurence oc = new Occurence();
            if (!string.IsNullOrEmpty(appID))
            {
                oc.ID = Convert.ToInt32(appID);
            }

            oc.Event_ID = 12345;
            oc.Start = Convert.ToDateTime(date + " " + startTime);
            oc.End = Convert.ToDateTime(date + " " + endTime);
            oc.Notes = notes;
            oc.Location_ID = Convert.ToInt32(locID); //Convert.ToInt32(Session["Location"]);
            oc.User_ID = Convert.ToInt32(doc);
            oc.Last_Change_ID = Convert.ToInt32(Session["userID"]);
            oc.External_ID = Convert.ToInt32(personID);
            oc.Reason_Code = Convert.ToInt32(reason);
            oc.TimeStamp = DateTime.Now;
            oc.WalkIn = 0;
            oc.Group_Appointment = 0;
            oc.isDeleted = false;

            _occurenceBusinessService.SaveAppointment(oc);

            ViewBag.LocationID = Convert.ToInt32(Session["Location"]);
            var reasons = _occurenceBusinessService.GetReasons();
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var i in reasons)
            {
                items.Add(new SelectListItem { Text = i.Reason_Text, Value = Convert.ToString(i.Reason_ID) });
            }
            Session["Reasons"] = items;
            Session["docList"] = _occurenceBusinessService.GetDoctors();

            var roomList = _occurenceBusinessService.GetRooms();
            Session["rooms"] = roomList;
            //GetData(Convert.ToString(oc.Location_ID));

            return View();
        }
        [HttpPost]
        public ActionResult SavePatient(PatientDTO obj)
        {
            int patID = _occurenceBusinessService.SavePatientData(obj);
            return Json(patID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PatientLookup(string query)
        {
            var patientList = _occurenceBusinessService.GetPatients();
            var result = Json(patientList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        public JsonResult GetData(string locationID, string selectedDate)
        {

            //int locationID = Convert.ToInt32(Session["Location"]);
            var occurence = _occurenceBusinessService.GetOccurencesList(Convert.ToInt32(locationID), selectedDate);
            return Json(occurence, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAppointment()
        {
            //ViewBag.Doctors = _occurenceBusinessService.GetDoctors();
            return PartialView("_AddAppointment");
        }
        public ActionResult AddPatient()
        {
            Person pd = new Person();
            return PartialView("_AddPatient", pd);
        }
        public JsonResult EditAppontment(string appID)
        {
            var appointment = _occurenceBusinessService.GetAppointmentById(Convert.ToInt32(appID));
            var patientList = _occurenceBusinessService.GetPatients();
            appointment.Notes = appointment.Notes + "_" + patientList.FirstOrDefault(a => a.ID == appointment.External_ID).Name;
            return Json(appointment, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAppointment(string appID)
        {
            _occurenceBusinessService.DeleteAppointment(Convert.ToInt32(appID));
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAppointmentHistoryList(int ID, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            IPagedList<OccurenceHistoryDTO> oh = _occurenceBusinessService.GetAppointmentHistory(ID).ToPagedList(pageNumber, pageSize);
            return PartialView("_GetAppointmentHistoryList", oh);
        }
        public ActionResult ShowPayment(string appID)
        {
            List<paymentDTO> obj = _occurenceBusinessService.GetPaymentList(Convert.ToInt32(appID));
            return View(obj);
        }
        [HttpGet]
        public ActionResult PatientCard(int ID, int? page)
        {
            PatientCardDTO patientData = new PatientCardDTO();
            var data = _occurenceBusinessService.GetPatientCard(ID);
            patientData.PatientID = ID;
            patientData.Age = data.Age;
            patientData.RegistrationDate = data.RegistrationDate;
            patientData.DOB = data.DOB;
            patientData.PatientCardNumber = data.PatientCardNumber;
            patientData.PatientFName = data.PatientFName;
            patientData.PatientLName = data.PatientLName;
            patientData.Photo = data.Photo;
            patientData.RegistrationLocation = data.RegistrationLocation;
            patientData.Sex = data.Sex;
            
            int pageSize = 5000;
            int pageNumber = (page ?? 1);
            patientData.patienthistory = _occurenceBusinessService.GetAppointmentHistory(ID).ToPagedList(pageNumber, pageSize); //occurenceID

            patientData.FormDataList = _occurenceBusinessService.GetFormData(ID); //personID
            patientData.PatientNumbers = _occurenceBusinessService.GetpatientNumbers(ID);// patientaID or personID
            patientData.PatientAddress = _occurenceBusinessService.GetpatientAddress(ID);// patientaID or personID
            patientData.MenuForms = _occurenceBusinessService.GetFormList();
            patientData.PatientNoteList = _occurenceBusinessService.GetPatientNoteList(ID);
            patientData.PatientContractList = _occurenceBusinessService.GetPatientContractList(ID);
            //patientData.patienthistory = patientAppointmentHistory.ToPagedList(pageNumber, pageSize).ToList();
            ViewBag.PatInsurance = data.PatientIns.ins_id > 0 ? "Add Insurance" : "Edit Insurance";
            return View(patientData);
        }

        [HttpGet]
        public ActionResult AddPatientDiscount(int ID)
        {
            DiscountNote note = new DiscountNote();
            note.Foreign_ID = ID;
            note.Owner = Convert.ToInt32(Session["userID"]);
            return View(note);
        }
        [HttpPost]
        public ActionResult AddPatientDiscount()
        {
            DiscountNote note = new DiscountNote();
            note.Owner = Convert.ToInt32(Session["userID"]);
            note.Discount = Request.Form["Discount"] == "" ? 0 : Convert.ToInt32(Request.Form["Discount"]);
            note.Note = Request.Form["Note"];
            note.Revision = DateTime.Now;
            if (note.ID == 0)
            {
                note.Date = DateTime.Now;
            }
            note.Foreign_ID = Convert.ToInt32(Request.Form["Foreign_ID"]);
            note.Note_Type = 1;
            _occurenceBusinessService.SavePatientDiscount(note);
            return View();
        }
        [HttpGet]
        public ActionResult AddPatientInsurance(int patID)
        {
            PatientInsurance pi = new PatientInsurance();
            ViewBag.PatID = patID;
            return View("_PatientInsurance", pi);
        }
        [HttpPost]
        public ActionResult SavePatientInsurance(PatientInsurance pi)
        {
            pi.User_Id = Convert.ToInt32(Session["userID"]);
            pi.Created = DateTime.Now;

            int piID = _occurenceBusinessService.SavePatientInsurance(pi);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddPatientContract(int patID)
        {
            PatientContract pc = new PatientContract();
            ViewBag.PatID = patID;
            return View("_AddPatientContract", pc);
        }
       
        public ActionResult PatientContarctList(int patID)
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPatientContract(PatientContract pc)

        {
            pc.User_ID = Convert.ToInt32(Session["userID"]);
            pc.Created = DateTime.Now;

            int pcID = _occurenceBusinessService.SavePatientContract(pc);
            return Json(pcID, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditPatientInformation(int ID)
        {
            PatientCardDTO patientData = new PatientCardDTO();
            var data = _occurenceBusinessService.GetPatientCard(ID);
            patientData.PatientID = ID;
            patientData.Age = data.Age;
            patientData.RegistrationDate = data.RegistrationDate;
            patientData.DOB = data.DOB;
            patientData.PatientCardNumber = data.PatientCardNumber;
            patientData.PatientFName = data.PatientFName;
            patientData.PatientLName = data.PatientLName;
            patientData.Photo = data.Photo;
            patientData.RegistrationLocation = data.RegistrationLocation;
            patientData.RegistrationLocationID = data.RegistrationLocationID;
            patientData.Sex = data.Sex;
            patientData.Source = data.Source;

            patientData.PatientNumbers = _occurenceBusinessService.GetpatientNumbers(ID);// patientaID or personID
            patientData.PatientAddress = _occurenceBusinessService.GetpatientAddress(ID);// patientaID or personID
            //ViewBag.BuildingList = _occurenceBusinessService.GetAllBuildings();
            var blist = _occurenceBusinessService.GetAllBuildings().Select(a => 
            new SelectListItem { Text = a.Name, Value = Convert.ToString(a.ID) });
            ViewBag.BuildingList = blist;
            
            return View(patientData);
        }
        [HttpPost]
        public ActionResult UpdatePatientNumber(int perID, Number objNum)
        {
            int ID = _occurenceBusinessService.UpdatePatientNumber(objNum, perID);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdatePatientAddress(Address objAdd, int perID)
        {
            int ID = _occurenceBusinessService.UpdatePatientAddress(objAdd, perID);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletePatientNumber(int numID)
        {
            int ID = _occurenceBusinessService.DeletePatientNumber(numID);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletePatientAddress(int addID)
        {
            int ID = _occurenceBusinessService.DeletePatientAddress(addID);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdatePatient(PatientDTO p)
        {
            _occurenceBusinessService.UpdatePerson(p);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdatePatientStat(PatientDTO p)
        {
            _occurenceBusinessService.UpdatePateintStat(p);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAttendance(int ocID, bool value)
        {
            int ID = _occurenceBusinessService.SavePatientAttendace(ocID, value);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPatientList(string sc1, string sc2, string sc3)
        {
            List<PatientDTO> patientList = _occurenceBusinessService.SearchPatientList(sc1, sc2, sc3);
            return View("PatientList", patientList);
        }
        public ActionResult SavePatientNote(string note, int priority, int patID)
        {
            PatientNote pn = new PatientNote();
            pn.Note = note;
            pn.Note_Date = DateTime.Now;
            pn.User_ID = Convert.ToInt32( Session["UserID"]);
            pn.Priority = priority;
            pn.Deprecated = 0;
            pn.Patient_ID = patID;

            int ID = _occurenceBusinessService.SavePatientNote(pn);
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletePatientNote(int pnID)
        {
            _occurenceBusinessService.DeletePatientNote(pnID);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SendRequestToDoctor(int patID, int concernedPerID)
        {
            int ID = _occurenceBusinessService.SendEmailInviteToDoc(patID, concernedPerID, User.Identity.GetUserName());
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSupervisorDocList(int patID)
        {
            List<PatientProviderHistory> pph = _occurenceBusinessService.GetSupervisorDoctorList(patID);
            return View();

        }
    }
}