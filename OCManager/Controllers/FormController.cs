﻿using OCManager.Entities.DBEntities;
using System;
using System.Collections.Generic;
using OCManager.Interface.BusinessInterface;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OCManager.Entities.Anonymous;
using System.Net;
using System.Text;
using System.IO;
using SelectPdf;
using System.Globalization;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace OCManager.Controllers
{
    [Authorize]
    public class FormController : Controller
    {
        [Inject]
        public IFormBusinessService _FormBusinessService { get; set; }

        [Inject]
        public IOccurenceBusinessService _occurenceBusinessService { get; set; }
        // GET: Form
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Form1464314(int patID)
        {
            Form1464314 frm = new Form1464314();
            ViewBag.PatID = patID;
            return View(frm);
        }
        [HttpPost]
        public ActionResult Form1464314(Form1464314 frm)
        {
            frm.Username = User.Identity.GetUserName();
            frm.User_Id = Convert.ToInt32(Session["userID"]);
            frm.Data = DateTime.Now;
            int patID = Convert.ToInt32(Request.Form["patID"]);
            _FormBusinessService.SaveForm1464314(frm, patID);
            return View();
        }

        // [AllowAnonymous]
        [HttpGet]      
        public ActionResult Form1247857(int patID, int? formDataId, int? isPdf)
        {
            var procedureList = _FormBusinessService.GetProcedureListByFormID(1247857);
            if (formDataId > 0)
            {
                List<storage_string> lstFrmData = _FormBusinessService.GetStorageStrinfFormData(formDataId.Value);
                ViewBag.SS = lstFrmData;
                if (isPdf != null)
                {
                    ViewBag.isPDF = 1;
                    foreach (var item in lstFrmData)
                    {
                        if (!String.IsNullOrEmpty(item.value_Key) && Char.IsNumber(item.value_Key[0]))
                        {
                            string name = item.value_Key.Substring(0, item.value_Key.IndexOf("цена"));
                            name = name.Substring(name.IndexOf(' '), name.Length - 2).Trim();
                            var obj = procedureList.FirstOrDefault(p => p.Name.Contains(name));
                            obj.Name = Convert.ToString(obj.Name + "_" + item.value);
                        }
                        else if (item.value_Key == "Итог")
                        {
                            ViewBag.tot = item.value;
                        }
                        else if (item.value_Key == "Скидки :")
                        {
                            ViewBag.DiscountBonus = double.Parse(item.value.Replace(',', '.'), CultureInfo.InvariantCulture);
                        }
                        else if (item.value_Key == "Добавления к цене :")
                        {
                            ViewBag.DiscountIncrease = double.Parse(item.value.Replace(',', '.'), CultureInfo.InvariantCulture);
                        }
                        else if(item.value_Key == "Дата")
                        {
                            ViewBag.data = item.value.Split(' ')[0];
                        }
                    }
                }
            }
            ViewBag.Discount = _FormBusinessService.GetDiscountList();

            int DiscountCalc = _FormBusinessService.FetchDiscountCalculation(patID);
            if (DiscountCalc < 40000)
            {
                ViewBag.GetDiscountBySpend = 0;
            }
            else if (DiscountCalc > 40000 && DiscountCalc < 70000)
            {
                ViewBag.GetDiscountBySpend = 1;
            }
            else
            {
                ViewBag.GetDiscountBySpend = 2;
            }
            ViewBag.patID = patID;
            return View(procedureList);
        }

        //This function saves both forms 1247857 and 2161884
        [HttpPost]
        public ActionResult saveFormValues(List<storage_string> items, string total, int frmID, int? formDataID, int patID, DateTime dateSelected)
        {
            int userID = Convert.ToInt32(Session["userID"]);
            int res = 1;
            if (frmID == 1247857)
            {
                res = _FormBusinessService.CheckAttendaceDate(patID, dateSelected);
            }
            int frmDataID = 0;
            if (res == 1)
                frmDataID = _FormBusinessService.SaveForm1247857(items, total, userID, frmID, formDataID, dateSelected);
            else
                formDataID = res;
            return Json(frmDataID, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult saveIndex(List<string> vals)
        {
            _FormBusinessService.SaveOrderIndex(vals);
            //return View();
            //return Json("output", JsonRequestBehavior.AllowGet);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult ConfigureForm1247857(int ID)
        {
            var procedureList = _FormBusinessService.GetProcedureListByFormID(1247857);
            return View(procedureList);
        }
        [HttpPost]
        public ActionResult ConfigureForm1247857(List<Appointment> lstApp, List<string> vals)
        {
            _FormBusinessService.SaveBaseForm1247857(lstApp);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public ActionResult Form2161884(int patID, bool main)
        //{
        //    Form_2161884DTO obj = _FormBusinessService.GetForm2061884Data();
        //    ViewBag.Discount = _FormBusinessService.GetDiscountList();
        //    ViewBag.PatID = patID;
        //    return View(obj);
        //}

        [HttpGet]
        public ActionResult Form2161884(int patID)
        {
            Form_2161884DTO obj = _FormBusinessService.GetForm2061884Data();
            ViewBag.Discount = _FormBusinessService.GetDiscountList();
            var blist = _occurenceBusinessService.GetAllBuildings().Select(a =>
            new SelectListItem { Text = a.Name, Value = Convert.ToString(a.ID) });
            ViewBag.BuildingList = blist;
            ViewBag.PatID = patID;
            return View(obj);
        }

        [HttpPost]
        public ActionResult SavePatientInHealing(PatientInHealing objData)
        {
            objData.Form_Filling_Date = DateTime.Now;
            objData.Who_Filled_Username = User.Identity.Name;
            objData.Who_Filled_Nickname = "";
            _FormBusinessService.SavePatHeiling(objData);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Form2161884(Form_2161884DTO obj, int patID)
        {
            obj.data = DateTime.Now;
            obj.user_id = Convert.ToInt32(Session["userID"]);
            obj.username = User.Identity.Name;
            int frmDataID = _FormBusinessService.Save2161884(obj, patID);
            return Json(frmDataID, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Form1242785(int patID, string from, string to, string isAll) //, string formDataID)
        {
            //Form1242785 relForm = null;
            if (Session["IsOperator"] != null)
            {
                ViewBag.docList = _FormBusinessService.GetDoctorList();
            }
            else
            {
                //relForm = _FormBusinessService.CheckReleaseFormAssignee(patID);
                int perID = Convert.ToInt32(Session["perID"]);
                
                Form1242785 releaseFormData = _FormBusinessService.GetReleaseFormData(patID, Convert.ToInt32(Session["userID"]));
                if (releaseFormData.NextAssignee == "Completed")
                {
                    ViewBag.Assignee = "completed";
                    return View(releaseFormData);
                }
                string GeneralPatientGeneralInfo = _FormBusinessService.GetPatientInfo(patID);
                string generalInfo = "\n <center><h3>Выписка из амбулаторной карты № " + GeneralPatientGeneralInfo.Split('_')[0] + System.Environment.NewLine +
                 GeneralPatientGeneralInfo.Split('_')[1] + " " + GeneralPatientGeneralInfo.Split('_')[2] + "</h3> </center> <hr> ";
                generalInfo = generalInfo + "Пациент: " + GeneralPatientGeneralInfo.Split('_')[1] + " " + GeneralPatientGeneralInfo.Split('_')[2] + System.Environment.NewLine;
                generalInfo = generalInfo + " Дата рождения: " + GeneralPatientGeneralInfo.Split('_')[3] + System.Environment.NewLine;
                generalInfo = generalInfo + " Находится на амбулаторном обследовании и лечении в КО ЦОЗДиП «Ясный Взор» с ";
                if(isAll == "1")
                {
                    generalInfo = generalInfo + GeneralPatientGeneralInfo.Split('_')[4] ;
                }
                else
                {
                    generalInfo = generalInfo + from ;
                }
                
                Console.WriteLine(generalInfo);
                if (perID == Convert.ToInt32(releaseFormData.NextAssignee) && releaseFormData.Tekst_Vipiski_0 != null)
                {
                    string RegexPattern = "navbar-fixed-top";
                    releaseFormData.Tekst_Vipiski_0 = Regex.Replace(releaseFormData.Tekst_Vipiski_0, "</head>", "<style>.hideHeader{display: none;}</style></head>");
                    releaseFormData.Tekst_Vipiski_0 = Regex.Replace(releaseFormData.Tekst_Vipiski_0, RegexPattern, "hideHeader");
                    ViewBag.stringData = generalInfo + releaseFormData.Tekst_Vipiski_0;
                    ViewBag.PatID = patID;
                    return View(releaseFormData);
                }
                if (releaseFormData.Signature_Status == 0)
                {
                    //if (from != null && to != null)
                    //{
                    if (releaseFormData.Tekst_Vipiski_0 != null)
                    {
                        string RegexPattern = "navbar-fixed-top";
                        releaseFormData.Tekst_Vipiski_0 = Regex.Replace(releaseFormData.Tekst_Vipiski_0, "</head>", "<style>.hideHeader{display: none;}</style></head>");
                        releaseFormData.Tekst_Vipiski_0 = Regex.Replace(releaseFormData.Tekst_Vipiski_0, RegexPattern, "hideHeader");
                        ViewBag.stringData = generalInfo + releaseFormData.Tekst_Vipiski_0;
                    }
                    else
                    {
                        if (from != null && to != null)
                        {
                            var res = _FormBusinessService.GetPatientFormData(patID, from, to);
                            ViewBag.stringData = generalInfo +  GetPatientForms(patID, res).ToString();
                        }
                        else
                        {
                            if (isAll == "1")
                            {
                                var res = _FormBusinessService.GetAllPatientFormData(patID);
                                ViewBag.stringData = generalInfo + GetPatientForms(patID, res).ToString();
                            }
                        }
                    }
                    ViewBag.fdate = GeneralPatientGeneralInfo.Split('_')[4].Split(' ')[0].Replace('.', '/');
                    ViewBag.tdate = GeneralPatientGeneralInfo.Split('_')[5].Split(' ')[0].Replace('.', '/');
                    ViewBag.PatID = patID;
                    return View(releaseFormData);
                    //}
                    //else
                    //{

                    // }
                }
                else
                {
                    string assName = GetAssignee(perID, releaseFormData);
                    ViewBag.Assignee = assName;
                }
            }
            ViewBag.PatID = patID;
            return View();
        }
        [HttpPost]
        public ActionResult RejectReleaseForm(int patID, string textArea)
        {
            _FormBusinessService.RejectReleaseForm(patID, Convert.ToInt32(Session["userID"]), textArea);
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public string GetAssignee(int personID, Form1242785 objReleasefrm)
        {

            int nextAssignee = Convert.ToInt32(objReleasefrm.NextAssignee);

            if (objReleasefrm.NextAssignee == null && objReleasefrm.Signature_Status == null)
            {
                if (Convert.ToInt32(objReleasefrm.Attending_Doctor) == personID)
                {
                    return "1";
                }
                else
                {

                    int perID = objReleasefrm.Attending_Doctor;
                    var per = _FormBusinessService.GetPerson(perID);
                    return per.First_Name + " " + per.Last_Name;
                }
            }
            else
            {
                if (objReleasefrm.NextAssignee == "Completed")
                {
                    //getting the last person signed
                    int perID = 0;
                    if (objReleasefrm.Head1 != null)
                        perID = Convert.ToInt32(objReleasefrm.Head1);
                    if (objReleasefrm.Head2 != null)
                        perID = Convert.ToInt32(objReleasefrm.Head2);
                    if (objReleasefrm.Signature_603615 != null)
                        perID = Convert.ToInt32(objReleasefrm.Signature_603615);
                    if (objReleasefrm.Signature_613915 != null)
                        perID = Convert.ToInt32(objReleasefrm.Signature_613915);


                    var per = _FormBusinessService.GetPerson(perID);
                    return per.First_Name + " " + per.Last_Name + "_completed";
                }
                else
                {
                    if (personID == Convert.ToInt32(objReleasefrm.NextAssignee))
                    {
                        return "1";
                    }
                    else
                    {
                        var per = _FormBusinessService.GetPerson(Convert.ToInt32(objReleasefrm.NextAssignee));
                        return per.First_Name + " " + per.Last_Name;
                    }
                }
            }
        }
        [HttpPost]
        public ActionResult Form1242785(Form1242785 obj1242785)
        {
            obj1242785.Username = User.Identity.GetUserName();
            obj1242785.User_ID = Convert.ToInt32(Session["userID"]);
            int locID = Convert.ToInt32(Session["Location"]);
            obj1242785.Tip_Vipiski = obj1242785.Tip_Vipiski == 0 ? null : obj1242785.Tip_Vipiski;

            obj1242785.Data = DateTime.Now;
            if (obj1242785.Head1 == "0")
                obj1242785.Head1 = null;
            if (obj1242785.Head2 == "0")
                obj1242785.Head2 = null;
            obj1242785.create_date = DateTime.Now;
            string res = _FormBusinessService.SaveForm1242785(obj1242785, locID);
            ViewBag.docList = _occurenceBusinessService.GetDoctors();
            ViewBag.PatID = obj1242785.Customer;
            return Content(res);
        }
        public string GetPatientForms(int patID, List<FormDataDTO> res)
        {
            StringBuilder sb = new StringBuilder();
            var  client = new WebClient { Encoding = System.Text.Encoding.UTF8 };
            foreach (var item in res)
            {

                if (item.FormID == 1247857 || item.FormID == 2161884)
                {
                    //commenting the below code since we need only simple forms in release doc
                    //bool contains = Directory.EnumerateFiles(Server.MapPath("~/Views/Form/")).Any(f => f.Contains(item.FormID.ToString()));
                    //string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(User.Identity.GetUserName() + ":" + "gegcbr170390"));
                    //client.Headers.Add("Authorization", "Basic " + credentials);
                    //if (contains)
                    //    sb = sb.Append(client.DownloadString("http://" + Request.Url.Authority + "/Form/Form" + item.FormID
                    //        + "?patID=" + patID + "&formDataId=" + item.FormDataID + "&isPdf=1"));
                }
                else
                {
                    bool contains = Directory.EnumerateFiles(Server.MapPath("~/ReleaseForms/")).Any(f => f.Contains(item.FormID.ToString()));
                    if (item.FormID == 1464314 || item.FormID == 1150712)
                    {
                        sb = sb.Append(client.DownloadString("http://" + Request.Url.Authority + "/ReleaseForms/" + item.FormID
                        + ".php?patID=" + patID + "&formDataId=" + item.FormDataID + ""));
                    }
                }
            }
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            string val = sb.ToString();
            doc.LoadHtml(val);
            //var nodes = doc.DocumentNode.SelectNodes("//div[@class='navbar']");
            //foreach (HtmlNode node in nodes)
            //{
            //    node.Remove();
            //}
            string result = doc.DocumentNode.InnerHtml;

            return result;
        }
        [HttpPost]
        public ActionResult GeneratePdf(string stringData, int patID)
        {
            if (stringData == null)
            {
                var res = _FormBusinessService.GetAllPatientFormData(patID);

                stringData = GetPatientForms(patID, res);
                stringData = @"<img src='~/ Content/ReleaseDoc/wapka-kaliningrad.jpg' alt='' />" + stringData;
            }
            stringData = Regex.Replace(stringData, "<body>", "<body><img src='~/ Content/ReleaseDoc/wapka-kaliningrad.jpg' alt='' />");
            string fname = DateTime.Now.ToString();
            // read parameters from the webpage
            string url = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(
                typeof(PdfPageOrientation), pdf_orientation, true);

            int webPageWidth = 1024;
            try
            {
                webPageWidth = 1024;
            }
            catch { }

            int webPageHeight = 0;
            try
            {
                webPageHeight = 0;
            }
            catch { }

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;


            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(stringData);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();
            if (!Directory.Exists(Server.MapPath("~/PatientReleaseDocs/")))
            {
                Directory.CreateDirectory(Server.MapPath("~/PatientReleaseDocs/"));
            }
            // return resulted pdf document
            //FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            string pdfpath = Server.MapPath("~/PatientReleaseDocs/ReleaseDoc.pdf");
            System.IO.File.WriteAllBytes(pdfpath, pdf);
            //fileResult.FileDownloadName = "Document.pdf";

            return Content("/PatientReleaseDocs/ReleaseDoc.pdf");
            //PdfConvert.ConvertHtmlToPdf(new PdfDocument
            //{
            //    Html = html,
            //    HeaderLeft = "[title]",
            //    HeaderRight = "[date] [time]",
            //    FooterCenter = "Page [page] of [topage]"

            //}, new PdfOutput
            //{
            //    OutputFilePath = Path.Combine(Server.MapPath("~/PatientDocUploads/"), "wkhtmltopdf-page.pdf")
            //});

        }
    }
}