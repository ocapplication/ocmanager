﻿$(document).ready(function () {



    $('#addAppointment').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget) // Button that triggered the modal

        if (window.location.href.indexOf("PatientCard")> 0)
        {
            $("#selectedValue").val($("#patID").val());
            $(".typeahead").typeahead('val', $("#patFullName").text().trim());
        }
        if ($('#Rooms').length > 0) {
            $("#lbllocValue").text($("#Rooms option:selected")[0].text);
            localStorage['loc'] =  $("#Rooms option:selected")[0].text;
        }
        else {
            $("#lbllocValue").text(localStorage['loc']);
        }
        //var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);

        //modal.find('.modal-title').text('New message to ' + recipient)
        //modal.find('.modal-body input').val(recipient)
    });
    $('#addAppointment').on('hidden.bs.modal', function (e) {
        // do something...
        $('#Doctors').selectpicker('val', [0]);
        $("#txtPatientSearch").val('');
        $('#txtCalendar').datepicker("setDate", new Date());
        $("#patNotes").val('');
        //$("#Reasons option")[0].value;
        $("#Reasons").selectpicker('val', [0]);
        $('#selectedValue').val('');
        //$('#timepicker1').timepicker({ 'setTime': '' });
        $('.timepicker').timepicker({ showMeridian: false, defaultTime: 'current' });
        $("#hdnAppID").val('');
    })

});

$('#addPatient').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    //var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    //modal.find('.modal-title').text('New message to ' + recipient)
    //modal.find('.modal-body input').val(recipient)
});


function savePatient() {

    var validateResult = ValidatePatient();
    if (validateResult == true) {
        var requestData = {
            FirstName: $("#recipient-firstname").val(),
            LastName: $("#recipient-lastname").val(),
            MiddleName: $("#recipient-middlename").val(),
            DOB: $("#recipient-dob").val(),
            BuildingLocation: $("#registration-location option:selected")[0].value,
            PhoneNumber: $('#phone-number').val(),
            PersonID: $('#Person_ID').val()
        };
        var pathUrl = window.location.origin;
        $.ajax({
            type: "POST",
            url: pathUrl +"/OccurenceManager/SavePatient",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(requestData),
            success: function (data) {
                $('#addPatient').modal('hide');
                alert("Patient Saved Successfully");
                location.href = pathUrl + "/OccurenceManager/PatientCard/" + data + "?page=1";
            },
            error: function () { }
        });
    }
}

function SaveAppointment() {

    var validateResult = ValidateAppointment();
    if (validateResult == true) {
        var requestData = {
            doc: $("#Doctors option:selected")[0].value,
            location: $("#Rooms").val(),
            date: $("#txtCalendar").val(),
            notes: $("#patNotes").val(),
            reason: $("#Reasons option:selected")[0].value,
            personID: $('#selectedValue').val(),
            startTime: $('#timepicker1').val(),
            endTime: $('#timepicker2').val(),
            appID: $("#hdnAppID").val(),
            locID: $("#location").val()
        };
        var path = "../OccurenceManager/Index";
        if (window.location.href.indexOf("PatientCard") > 0) {
            path = "../Index";
        }
        $.ajax({
            type: "POST",
            url: path,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(requestData),
            success: function (data) {
                alert("Appointment Saved");
                $('#addAppointment').modal('hide');
            },
            error: function () { }
        });
    }
}

function EditAppointemt(obj) {
    var path = "../OccurenceManager/EditAppontment?appID=";
    if (window.location.href.indexOf("PatientCard") > 0)
    {
        path = "../EditAppontment?appID=";
    }
    $.ajax({
        type: "GET",
        url: path + obj.id,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //debugger;
            //$("#txtCalendar").val(moment(data.Start).format('DD/MM/YYYY'));
            // alert(moment(data.Start).format('DD/MM/YYYY'));

            $("#txtCalendar").datepicker("setDate", new Date(moment(data.Start).format("MM/DD/YYYY'")));
            $("#patNotes").val(data.Notes.split('_')[0]);
            $("#timepicker1").timepicker("setTime", moment(data.Start).format("H:mm"));
            $("#timepicker2").timepicker("setTime", moment(data.End).format("H:mm"));
            //$("#timepicker2").val(moment(data.End).format("h:mm"));
            $("#selectedValue").val(data.External_ID);
            $(".typeahead").typeahead('val', data.Notes.split('_')[1]);
            // $("#txtCalendar").val(moment(data.Start).format('YYYY/MM/DD'));
            var array = JSON.parse("[" + data.User_ID + "]");
            $("#Doctors").selectpicker("val", array);
            var array1 = JSON.parse("[" + data.Reason_Code + "]");
            $("#Reasons").selectpicker("val", array1);
            var locArr = JSON.parse("[" + data.Location_ID + "]");
            $("#location").selectpicker("val", locArr);
            $("#hdnAppID").val(obj.id);
        },
        error: function () { }
    });
}

function DeleteAppointment(obj) {
    $.ajax({
        type: "GET",
        url: "../OccurenceManager/DeleteAppointment?appID=" + obj.id,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            alert("Deleted");
        },
        error: function () { alert("Error Occured"); }
    });
}
function GetAppointmentHistory(obj) {
    $.ajax({
        type: "GET",
        url: "../OccurenceManager/GetAppointmentHistoryList?ID=" + obj,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#listContainer").html(data);
            $("#patientName").text("История изменений записи календаря для - " + $("#hdnPatientName").val());

        },
        error: function () { alert("Error Occured"); }
    });
}

function ShowPayment(obj) {
    //$('#myPleaseWait').modal('show');
    $.ajax({
        type: "GET",
        url: "../ShowPayment",
        data: { appID: obj.id },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //alert(data);
            $("#paymentContainer").html(data);
            // $('#myPleaseWait').modal('hide');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}

// **********************Patient Controller Start ******************************
function PatientDocs(obj) {
    $.ajax({
        type: "GET",
        url: "PatientDocument/GetPatientDocuments",
        data: { appID: obj.id },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //alert(data);
            $("#paymentContainer").html(data);
            // $('#myPleaseWait').modal('hide');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}
function categoryClick(obj) {
    $("#hdnCatID").val(obj.id);
    $("#dvImage").hide();
    $("#dvUpload").show();
}
function imageClick(obj) {
    $("#dvUpload").hide();
    $("#dvImage").show();
    $("#fileName").text($(obj).text());
    $("#fileSize").text(($(obj).attr("name").split('$')[0] / 1024).toFixed(2));
    $("#lblDate").text($(obj).attr("name").split('$')[2]);
    $("#lblType").text($(obj).attr("name").split('$')[1]);
    $("#imgDownload").attr("name", $(obj).attr("name").split('$')[4]);
    var ext = $('#fileName').text().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) > 0) {
        $("#selectedImage").show();
        $("#fileView").hide();
        var url = $(obj).attr("name").split('$')[3];
        url = url.substr(url.lastIndexOf("\\"), url.length);
        $("#selectedImage").attr("src", '/PatientDocUploads/' + url);
    }
    if ($.inArray(ext, ['pdf']) > -1) {
        $("#fileView").show();
        $("#selectedImage").hide();
        $("#fileView").attr("src", $(obj).attr("name").split('$')[3] + "&as_file=false");
    }
}
function EditFile(obj) {
    $(obj).hide();
    $("#fileName").hide();
    $("#txtFile").val($("#fileName").text());
    $("#txtFile").show();
    $('.selectpicker').removeAttr('disabled');
    $('.selectpicker').selectpicker('refresh');
    $("#btnUpdate").show();
    $("#btnCancel").show();
}

function CancelUpdate(obj) {
    $(obj).hide();
    $("#btnUpdate").hide();
    $("#txtFile").val('');
    $("#fileName").show();
    $("#txtFile").hide();
    $('.selectpicker').attr('disabled', true);
    $('.selectpicker').selectpicker('refresh');
    $("#btnEdit").show();
}
function UpdateFile(obj) {
    var folder = $("#ddlCategories").val();
    var docID = $("#imgDownload").attr("name");
    var name = $("#txtFile").val();
    if (name == "") {
        alert("Name can't be left blank");
        return;
    }
    else {
        $.ajax({
            type: "GET",
            url: "../UpdateDocument",
            data: { dID: docID, fID: folder, dname: name },

            success: function (data) {
                if (data > 0) {
                    location.reload();
                }
            },
            error: function () {
                alert("Error Occured");

            }
        });
    }
}
function UploadFile() {
    // Checking whether FormData is available in browser  
    if (window.FormData !== undefined) {

        var fileUpload = $("#file").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();

        fileData.append(files[0].name, files[0]);

        // Adding one more key to FormData object  
        fileData.append('PatientID', $("#hdnPatID").val());
        fileData.append('CategoryID', $("#hdnCatID").val());

        $.ajax({
            url: '../UploadFiles',
            type: "POST",
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,
            success: function (result) {
                if (result == 'Done') {
                    location.reload();
                }
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
    } else {
        alert("Could not upload the file. Please try again");
    }
};

function uploadMultiFile() {
    var res = validatePair();
    if (res == true) {
        if (window.FormData !== undefined) {

            var fileData = new FormData();
            $('input[type="file"]').each(function () {
                var $this = $(this);
                if ($this.val() != '') {
                    var fileUpload = $($this).get(0);
                    var files = fileUpload.files;
                    fileData.append(files[0].name, files[0]);
                }
            });
            var categories = "";
            $('select[id=CategoryList] > option:selected').each(function () {
                categories = categories + $(this).val() + ",";
            });
            categories = categories.substring(0, categories.length - 1);
            // Adding one more key to FormData object  
            fileData.append('PatientID', $("#hdnPatID").val());
            fileData.append('selectedCat', categories);
            $.ajax({
                url: '../UploadFiles',
                type: "POST",
                contentType: false, // Not to set any content header  
                processData: false, // Not to process data  
                data: fileData,
                success: function (result) {
                    if (result == 'Done') {
                        location.reload();
                    }
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        } else {
            alert("Could not upload the file. Please try again");
        }
    }
}
function validatePair() {
    var res = true;
    var rows = $('div.row').length;
    var iteration = 1;
    $('select[id=CategoryList] > option:selected').each(function () {
        if ($(this).val() == '') {
            alert("Please select the Category for the document in row :-" + iteration);
            res = false;
        }
        iteration = iteration + 1;
    });
    var iteration1 = 1;
    $('input[id="mfile"]').each(function () {
        var $this = $(this);
        if ($this.val() == '') {
            alert("Please select the document to upload in row :-" + iteration1);
            res = false;
        }
        iteration1 = iteration1 + 1;
    });
    return res;
}
function addMore(obj) {
    var row = $(obj).prev()[0].outerHTML;
    $(obj).prev().parent().prepend(row);
}
function closeReset() {
    $(".row").not('div:first').remove();
}
function SaveAttendance(obj, ocid)
{
    var isattended = $(obj).is(':checked');
    $.ajax({
        type: "POST",
        url: "../SaveAttendance",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ ocID: ocid, value: isattended }),
        success: function (data) {
            alert('Patient Data Saved');
        },
        error: function () {
            alert("Error Occured");
        }
    });
}
// **********************Patient Controller End ******************************
// **********************Form Controller Start ******************************

function openForm(obj) {
    //location.href = "../../Form/Form" + $(obj).val() + "?patID=" + obj.id;
    //alert($(obj).val());
    if ($(obj).val() == 1247857 || $(obj).val() == 2161884 || $(obj).val() == 1242785) {
        location.href = "../../Form/Form" + $(obj).val() + "?patID=" + obj.id;
    }
    else {
        location.href = "../../PhpForms/" + $(obj).val() + ".php?patID=" + obj.id;
    }
    //PhpForms/1464314/1464314.php
    //var actionUrl = "../../Form/Form" + $(obj).val();

    //$.ajax({
    //    type: "GET",
    //    url: actionUrl,
    //    data: {patID : obj.id},
    //    contentType: "application/json; charset=utf-8",
    //    success: function (data) {
    //        //alert(data);
    //        $("#formContainerData").html(data);
    //    },
    //    error: function () {
    //        alert("Error Occured");
    //        return false;
    //    }
    //});
}
function fillValues() {
    var valuecount = 1;
    $("#formContainerData :text").each(function () {
        $(this).val(valuecount);
        valuecount++;
    });
}
function SaveForm1464314(obj) {
    var form = $(obj).parents('form');
    var isvalid = form.valid();
    if (isvalid) {
        $.ajax({
            type: "POST",
            url: "../../Form/Form1464314",
            data: form.serialize(),//JSON.stringify(myData),
            success: function (data) {
                alert('Form Saved');
                $("#FormContainer").modal('hide');
                $('#myform')[0].reset();
                $(':input').val('');
            },
            error: function () {
                alert("Error Occured");
            }
        });
    }
    else {
        alert('Please enter the data');
    }
    return false;
}

function savePatHeiling()
{
    if ($("#hdnFrmDataID2161884").val() == "")
    {
        alert("Please save the Patient Form First");
        return false;
        
    }
    else if ($("#buildings").val() == "" || $("#txtStartDate").val() == "")
    {
        alert("Building location and Start date is mandatory");
        return false;
    }
    else
    {
        var obj = {
            Patient_Id: window.location.href.split('=')[1],
            Form_Data_Id: $("#hdnFrmDataID2161884").val(),
            Building: $("#buildings").val(),
            Treatment_Date_Started: $("#txtStartDate").val(),
            Dates_Of_Passed_Days: $("#txtMissedDate").val(),
            Date_Of_Termination_Of_Treatment: $("#txtPlannedEndDate").val(),
            Date_Of_End_Of_Treatment: $("#txtActualEndDate").val(),
            In_Healing_Note: $("#txtNote").val(),
        }
        $.ajax({
            type: "POST",
            url: "../../Form/SavePatientInHealing",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ objData: obj }),
            success: function (data) {
                alert('Data Saved');
                window.location.reload();
            },
            error: function () {
                alert("Error Occured");
            }
        });
    }
}

//********************1247857 Start*****************
function Save1247857(obj) {
    var arr = [];
    var pattern = /^([0-9]{2})\-([0-9]{2})\-([0-9]{4})$/;
    if ($("#txtDate").val() == "" || pattern.test($("#txtDate").val()) == false)
    {
        alert("Please select a valid date");
        return false;
    }
    $(obj).parents('div .container').find('select').each(function () {
        if ($(this).val() != "0") {
            if ($(this).attr('id') == 'DiscountIncrease' || $(this).attr('id') == 'DiscountBonus') {
                var storage_string = {
                    foreign_Key: window.location.href.split('=')[1],
                    value_Key: $(this).parents('div .row').find('label').text(),
                    value: $(this).val(),
                    Appointment_ID: $(this).parents('tr').find('td:eq(0) input').val()
                };
            }
            else {
                var storage_string = {
                    foreign_Key: window.location.href.split('=')[1],
                    value_Key: $(this).parents('tr').find('td:eq(8) input').val()
                         + " " + $(this).parents('tr').find('td:eq(1)').text().trim()
                         + " цена - " + $(this).parents('tr').find('td:eq(3) input').val()
                         + ", кол. -",
                    value: $(this).val(),
                    Appointment_ID: $(this).parents('tr').find('td:eq(0) input').val()
                };
            }
            arr.push(storage_string);
        }
    });
    $.ajax({
        url: '../../Form/saveFormValues',
        type: "post",
        dataType: 'json',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ items: arr, total: $("#tot").val()
                             , frmID: 1247857, formDataID: $("#hdnFdID").val()
                             , patID: $("#hdnPatID").val()
                             , dateSelected: $("#txtDate").val() }),
        success: function (data) {
            if (data == 1 || data > 1) {
                alert('Form Saved');
                window.location.reload();
            }
            if(data == 0)
            {
                alert("Дата формы и календара не совпадают");
            }
        },
        error: function () {
            alert("Ошибка");
        }
    });
}
$("#btnSave1247857").click(function () {

});
function calculatePrice() {
    var tot = 0;
    $('.selectlist').each(function () {
        if ($(this).val() > 0) {
            tot = tot + parseInt($(this).val()) * parseInt($(this).parent().next().find("input[type='hidden']").val());
        }
    });
    $("#tot").val(tot);
    var objInc = $("#DiscountIncrease");
    var objDisB = $("#DiscountBonus");
    calcInc(objInc);
    calcDisBonus(objDisB);
}
function calcInc(obj) {
    var totVal = parseInt($("#tot").val());
    if (totVal > 0 && $(obj).val() != "") {
        var selectedVal = parseFloat($(obj).val().replace(',', '.'));
        var incVal = totVal + (selectedVal * totVal);
        $("#tot").val(incVal);
    }
}
function calcDisBonus(obj) {
    var totVal = parseInt($("#tot").val());
    var selectedVal = 0; var incVal = 0;
    if (totVal > 0 && $(obj).val() != "") {
        if ($(obj).val() == "1") {
            $("#tot").val(0);
        }
        else if ($(obj).val().indexOf(',') > 0) {
            selectedVal = parseFloat($(obj).val().replace(',', '.'));
            incVal = totVal - (selectedVal * totVal);
            $("#tot").val(incVal);
        }
        else {
            selectedVal = parseFloat($(obj).val());
            incVal = totVal - selectedVal;
            $("#tot").val(incVal);
        }
    }
}
//********************1247857 End*****************

//********************2161884 Start*****************
function saveNested(obj) {
    var arr = [];
    $(obj).parents('div .container').find('select').each(function () {
        if ($(this).val() != "") {
            if ($(this).attr('id') == 'DiscountIncrease' || $(this).attr('id') == 'DiscountBonus') {
                var storage_string = {
                    foreign_Key: window.location.href.split('=')[1],
                    value_Key: $(this).parents('div .row').find('label').text(),
                    value: $(this).val(),
                    Appointment_ID: $(this).parents('tr').find('td:eq(0) input').val()
                };
            }
            else {
                var storage_string = {
                    foreign_Key: window.location.href.split('=')[1],
                    value_Key: $(this).parents('tr').find('td:eq(8) input').val()
                         + " " + $(this).parents('tr').find('td:eq(1)').text().trim()
                         + " цена - " + $(this).parents('tr').find('td:eq(3) input').val()
                         + ", кол. -",
                    value: $(this).val(),
                    Appointment_ID: $(this).parents('tr').find('td:eq(0) input').val()
                };
            }
            arr.push(storage_string);
        }
    });
    $.ajax({
        url: '../../Form/saveFormValues',
        type: "post",
        dataType: 'json',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            items: arr, total: $(obj).parents('div .container').find('label[id="tot"]').text(),
            frmID: $(obj).parents('div .container').find('tr')[1].id, formDataID: $("#hdnFrmDataID").val(),
            patID: window.location.href.split('=')[1], dateSelected: new Date()
        }),
        success: function (data) {
            $(obj).parents('div .container').find('input:hidden[id="hdnFrmDataID"]').val(data);
            $(obj).parents('div .container').find('input:hidden[id="id_' + $(obj).parents('div .container').find('tr')[1].id +'"]').val(data);
            if (data != "") {
                alert('Saved Successfully');
            }
        },
        error: function () {
            alert("Error Occured");
        }
    });
}

function Save2161884(obj) {

    var PrimechanieVals = "";
    var frmDataIDs = "";
    var form = $(obj).parents('form');
    $('div #accordion').each(function () {
        //alert($(this).find('textarea').attr('id'));
        //alert($(this).find('textarea').val());
        //Primechanie_k_2051655
        //PrimechanieVals = PrimechanieVals +
        //alert($(this).find('input[type="hidden"][id="hdnFrmDataID"]').val());
    });
    //$(obj).parents('form').find('textarea').each(function () {
    //    PrimechanieVals = PrimechanieVals + $(this).attr('id') + "_" + $(this).val() + "^";
    //    frmDataIDs = frmDataIDs + $(this).find('input[type="hidden"][id="hdnFrmDataID"]').val() + "^";
    //});
    PrimechanieVals = PrimechanieVals.substr(PrimechanieVals.length - 1, PrimechanieVals.length);
    frmDataIDs = frmDataIDs.substr(frmDataIDs.length - 1, frmDataIDs.length);
    //alert(PrimechanieVals);
    
    var form2161884 = {
          Id_1951659 : $("#id_1951659").val(),
          Id_2051655 : $("#id_2051655").val(),
          Id_2051656 : $("#id_2051656").val(),
          Id_2051657 : $("#id_2051657").val(),
          Id_2051658 : $("#id_2051658").val(),
          Id_1951659 : $("#id_1951659").val(),
          Id_1951660 : $("#id_1951660").val(),
          Id_1951661 : $("#id_1951661").val(),
          Id_1951662 : $("#id_1951662").val(),
          Id_1951663 : $("#id_1951663").val(),
          Id_1951664 : $("#id_1951664").val(),
          Id_1951671 : $("#id_1951671").val(),
          Id_1951672 : $("#id_1951672").val(),
          Id_1951669 : $("#id_1951669").val(),
          Id_1951670 : $("#id_1951670").val(),
          Id_1951665 : $("#id_1951665").val(),
          Id_1951666 : $("#id_1951666").val(),
          Id_2161880 : $("#id_2161880").val(),
          Id_2161881 : $("#id_2161881").val(),
          
          
          Primechanie_k_1951659 : $("#Primechanie_k_1951659").val(),
          Primechanie_k_2051655 : $("#Primechanie_k_2051655").val(),
          Primechanie_k_2051656 : $("#Primechanie_k_2051656").val(),
          Primechanie_k_2051657 : $("#Primechanie_k_2051657").val(),
          Primechanie_k_2051658 : $("#Primechanie_k_2051658").val(),
          Primechanie_k_1951659 : $("#Primechanie_k_1951659").val(),
          Primechanie_k_1951660 : $("#Primechanie_k_1951660").val(),
          Primechanie_k_1951661 : $("#Primechanie_k_1951661").val(),
          Primechanie_k_1951662 : $("#Primechanie_k_1951662").val(),
          Primechanie_k_1951663 : $("#Primechanie_k_1951663").val(),
          Primechanie_k_1951664 : $("#Primechanie_k_1951664").val(),
          Primechanie_k_1951671 : $("#Primechanie_k_1951671").val(),
          Primechanie_k_1951672 : $("#Primechanie_k_1951672").val(),
          Primechanie_k_1951669 : $("#Primechanie_k_1951669").val(),
          Primechanie_k_1951670 : $("#Primechanie_k_1951670").val(),
          Primechanie_k_1951665 : $("#Primechanie_k_1951665").val(),
          Primechanie_k_1951666 : $("#Primechanie_k_1951666").val(),
          Primechanie_k_2161880 : $("#Primechanie_k_2161880").val(),
          Primechanie_k_2161881 : $("#Primechanie_k_2161881").val(),
          
          kommentarii_k_kursu_lecheniya_12: $("#kommentarii_k_kursu_lecheniya_12").val(),
          occ_id   : 0,
          occ_id_1 : 0,
          occ_id_2 : 0,
          occ_id_3 : 0,
          occ_id_4 : 0,
          occ_id_5 : 0,
          occ_id_6 : 0,
          occ_id_7 : 0
    };
    $.ajax({
        url: '../../Form/Form2161884',
        type: "post",
        dataType: 'json',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            //textAreas: PrimechanieVals, formdataIds:frmDataIDs, patID: window.location.href.split('=')[1]
            obj: form2161884, patID: window.location.href.split('=')[1]
        }),
        success: function (data) {
            if (data != "") {
                alert('Saved Successfully');
                $("#hdnFrmDataID2161884").val(data);
            }
        },
        error: function () {
            alert("Error Occured");
        }
    });

}
//********************2161884 End*****************

// **********************Form Controller End ******************************

function deleteNumber(numberID)
{
    $.ajax({
        type: "GET",
        url: "../DeletePatientNumber",
        contentType: "application/json; charset=utf-8",
        data: { numID: numberID },
        success: function (data) {
            if (data == 1)
                alert("Deleted Successfully");
        },
        error: function () {
            alert("Error Occured");
        }
    });
}
function deleteAddress(addressID) {
    $.ajax({
        type: "GET",
        url: "../DeletePatientAddress",
        contentType: "application/json; charset=utf-8",
        data: { addID: addressID },
        success: function (data) {
            if (data == 1)
                alert("Deleted Successfully");
        },
        error: function () {
            alert("Error Occured");
        }
    });
}

function EditPatientInformation(obj) {
    var urlPath = window.location.origin;
    $.ajax({
        type: "GET",
        url: "../EditPatientInformation",
        contentType: "application/json; charset=utf-8",
        data: { ID: obj.id },
        success: function (data) {
            $("#PatientInformationContainer").html(data);
        },
        error: function () {
            alert("Error Occured");
        }
    });
}
function getNumber(obj) {
    alert($(obj).text().trim());
    $("#txtNumber").val($(obj).text().trim());
    $("#txtAreaNote").val($(obj).parent().next('td').text().trim());
    $("#ddltype").val($(obj).parent().next().next('td').text().trim());
    $("#numID").val($(obj).parent().prev('td').text().trim());
}
function UpdateNumber(obj) {
    var num = {
        Notes: $("#txtAreaNote").val(),
        Number_Id: $("#numID").val(),
        Number_Type: $("#ddltype").val(),
        number : $("#txtNumber").val()
    }
    $.ajax({
        type: "POST",
        url: "../UpdatePatientNumber",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ objNum: num, perID: $("#hdnPatID").val() }),
        success: function (data) {
            alert('Patient Number Updated');
        },
        error: function () {
            alert("Error Occured");
        }
    });
}
function getAddress(obj) {
    $("#txtName").val($(obj).parent().prev('td').text().trim());
    //$("#txtAreaNoteAdd").val($(obj).parent().next('td').text().trim());
    alert($(obj).text());
    $("#txtAddressLine1").val($(obj).text().trim());
    $("#txtAddressLine2").val($(obj).parent().next('td').text().trim());
    $("#txtAddressLine1").val($("#txtAddressLine1").val().replace($("#txtAddressLine2").val(), ""));
    $("#ddltypeAdd").val($(obj).parent().next().next('td').text().trim());
    $("#txtCity").val($(obj).parent().next().next().next('td').text().trim());
    $("#txtPostCode").val($(obj).parent().next().next().next().next('td').text().trim());
    $("#addID").val($(obj).parent().prev('td').prev('td').text().trim());
}
function UpdateAddress(obj) {
    var add = {
        Notes: $("#txtAreaNoteAdd").val(),
        Address_Id: $("#addID").val(),
        Name: $("#txtName").val(),
        Line1: $("#txtAddressLine1").val(),
        Line2: $("#txtAddressLine2").val(),
        City :  $("#txtCity").val(),
        Postal_Code : $("#txtPostCode").val(),
        Type: $("#ddltypeAdd").val()
    }
    $.ajax({
        type: "POST",
        url: "../UpdatePatientAddress",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ objAdd: add, perID: $("#hdnPatID").val() }),
        success: function (data) {
            alert('Patient Address Updated');
        },
        error: function () {
            alert("Error Occured");
        }
    });
}
function UpdatePatient()
{
    var person = {
        FirstName: $("#PatientFName").val(),
        LastName: $("#PatientLName").val(),
        Gender: $("#Sex").val(),
        DOB: $("#DOB").val(),
        PersonID: $("#hdnPatID").val(),
    }
    $.ajax({
        type: "POST",
        url: "../UpdatePatient",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ p: person }),
        success: function (data) {
            alert('Patient Data Updated');
        },
        error: function () {
            alert("Error Occured");
        }
    });
}

function UpdatePatientStat() {
    var patStat = {
        BuildingLocation: $("#RegistrationLocationID").val(),
        RegistrationDate: $("#RegistrationDate").val(),
        Source: $("#Source").val(),
        PersonID: $("#hdnPatID").val(),
    }
    $.ajax({
        type: "POST",
        url: "../UpdatePatientStat",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ p: patStat }),
        success: function (data) {
            alert('Patient Stat Updated');
        },
        error: function () {
            alert("Error Occured");
        }
    });
}


function PatInsurancePopup(obj) {
    $.ajax({
        type: "GET",
        url: "../AddPatientInsurance",
        contentType: "application/json; charset=utf-8",
        data: { patID: obj.id },
        success: function (data) {
            $("#patientInsuranceContainer").html(data);
        },
        error: function () {
            alert("Error Occured");

        }
    });
}
function SavePatientInsurance(obj) 
{
    var res = validatePatientInsurance();
    if (res) {
        var insNomer = $("#nomer1").val() + $("#nomer2").val() + $("#nomer3").val() + $("#nomer4").val() + $("#nomer5").val() +
        $("#nomer6").val() + $("#nomer7").val() + $("#nomer8").val() + $("#nomer9").val() + $("#nomer10").val() + $("#nomer11").val() +
        $("#nomer12").val() + $("#nomer13").val() + $("#nomer14").val() + $("#nomer15").val() + $("#nomer16").val();
        $("#Ins_Nomer").val(insNomer);
        alert(insNomer);
        var patInsurance = {
            Ins_Tip: $("#Ins_Tip").val(),
            Birth_Cert: $("#Birth_Cert").val(),
            Ins_Nomer: $("#Ins_Nomer").val(),
            Ins_Name: $("#Ins_Name").val(),
            Napravlenie: $("#Napravlenie").val(),
            Polik_Name: $("#Polik_Name").val(),
            Polik_City: $("#Polik_City").val(),
            Naprav_Date: $("#Naprav_Date").val(),
            Agent_Name: $("#Agent_Name").val(),
            Agent_Date: $("#Agent_Date").val(),
            Kurator: $("#Kurator").val(),
            Naprav_Kuda: $("#Naprav_Kuda").val(),
            Region: $("#Region").val(),
            Patient_Id: $("#hdnPatID").val()
        }
        var form = $(obj).parents('form');
        $.ajax({
            type: "POST",
            url: "../SavePatientInsurance",
            data: JSON.stringify({ pi: patInsurance }),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                alert('Patient Insurance Saved');
            },
            error: function () {
                // $('#myPleaseWait').modal('hide');
                alert("Error Occured");

            }
        });
    }
}

function validatePatientInsurance()
{
    if($("#nomer1").val().trim() == "" || $("#nomer2").val().trim() == "" ||$("#nomer3").val().trim() == "" 
        ||$("#nomer4").val().trim() == "" ||$("#nomer5").val().trim() == "" ||$("#nomer6").val().trim() == "" ||
        $("#nomer7").val().trim() == "" ||$("#nomer8").val().trim() == "" ||$("#nomer9").val().trim() == "" ||
        $("#nomer10").val().trim() == "" ||$("#nomer11").val().trim() == "" ||$("#nomer12").val().trim() == "" ||
        $("#nomer13").val().trim() == "" ||$("#nomer14").val().trim() == "" ||$("#nomer15").val().trim() == "" ||
        $("#nomer16").val().trim() == "" )
    {
        alert("Please fill the Nomer values");
        return false;
    }
    if($("#Birth_Cert").val().trim()=="")
    {
        alert("Please fill the Birth Certificate");
        return false;
    }
    return true;
}
function AddPatientContract(obj) {
    $.ajax({
        type: "GET",
        url: "../AddPatientContract",
        contentType: "application/json; charset=utf-8",
        data: { patID: obj.id },
        success: function (data) {
            $("#PatientContractContainer").html(data);
        },
        error: function () {
            alert("Error Occured");

        }
    });

}
function SavePatientContract()
{
    var patContract = {
        Contract_Nomer: $("#Contract_Nomer").val(),
        Contract_Tip: $("#Contract_Tip").val(),
        Contract_Amount: $("#Contract_Amount").val(),
        Contract_Note: $("#Contract_Note").val(),
        Patient_ID: $("#hdnPatID").val()
    }
    $.ajax({
        type: "POST",
        url: "../AddPatientContract",
        data: JSON.stringify({ pc: patContract }),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            alert('Patient Contract Saved');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}

function AddPatientDiscount(obj) {
    $.ajax({
        type: "GET",
        url: "../AddPatientDiscount",
        contentType: "application/json; charset=utf-8",
        data: { ID: obj.id },
        success: function (data) {
            //alert(data);
            $("#PatientDiscountContainer").html(data);
            // $('#myPleaseWait').modal('hide');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}
function SavePatientNote(obj)
{
    $.ajax({
        type: "GET",
        url: "../SavePatientNote",
        data: { note: $("#txtNote").val(), priority: $("#Priority").val(), patID: $(obj).attr("id") },
        success: function (data) {
            alert('Patient note Saved');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}
function deletePatientNote(obj)
{
    $.ajax({
        type: "GET",
        url: "../DeletePatientNote",
        data: { pnID: obj },
        success: function (data) {
            alert('Patient note deleted');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");
        }
    });
}
function SendEmailToDoc(obj)
{
    $.ajax({
        type: "GET",
        url: "../SendRequestToDoctor",
        data: { patID: obj, concernedPerID: $("#SupervisingDoc").val() },
        success: function (data) {
            alert('Email to supervising Doctor sent successfully');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");
        }
    });
}
function SavePatientDiscount(obj) {
    var form = $(obj).parents('form');
    var isvalid = form.valid();
    if (isvalid) {
        $.ajax({
            type: "POST",
            url: "../AddPatientDiscount",
            data: form.serialize(),//JSON.stringify(myData),
            success: function (data) {
                alert('Discount Saved');
                $("#addPatientDiscount").modal('hide');
            },
            error: function () {
                // $('#myPleaseWait').modal('hide');
                alert("Error Occured");

            }
        });
    }
    else {
        alert('Please enter the data');
    }

    return false;
}

function GetPatientList(searchCreteria1, searchCreteria2, searchCreteria3)
{
    if (searchCreteria1 == "" && searchCreteria2 == "" && searchCreteria3 == "")
    {
        alert("Please enter filter creteria");
        return false;
    }
   // alert(1);
    $.ajax({
        type: "GET",
        url: "../OccurenceManager/GetPatientList",
        contentType: "application/json; charset=utf-8",
        data: { sc1: searchCreteria1, sc2: searchCreteria2, sc3: searchCreteria3 },
        success: function (data) {
            //alert(data);
            $("#PatientSearchList").html(data);
            // $('#myPleaseWait').modal('hide');
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}

function getFormDataRange() {
    var pathUrl = window.location.origin;
    location.href = pathUrl + "/Form/Form1242785?patID=" + $("#hdnPatID").val() + "&from=" + $("#fromDate").val() + "&to=" + $("#toDate").val(); //+ "&formDataID=" + $("#hdnFormDataID").val();
}
function getAllFormData()
{
    var pathUrl = window.location.origin;
    location.href = pathUrl + "/Form/Form1242785?patID=" + $("#hdnPatID").val() + "&isAll=1";
}


function SaveReleaseDoc()
{    
    if ($('input[name="deputy"]:checked').length == 0 && $('input[name="mainHead"]:checked').length ==0
        && $("#head1").val() == 0 && $("#head2").val() == 0 && $('input[name="signed"]:checked').length ==1)
    {
        alert("Please select some next level approvers");
        return;
    }
    if ($("#head1").val() == 0 && $("#head2").val() != 0)
    {
        alert("Please select from Head1 first");
        return;
    }
    if (($('input[name="deputy"]:checked').length != 0 || $('input[name="mainHead"]:checked').length != 0
        || $("#head1").val() != 0 || $("#head2").val() != 0) && $('input[name="signed"]:checked').length == 0
        && $("#hdnSigStatus").val() == "0")
    {
        alert("Please sign the document before assigning");
        return;
    }
    

    var objRel1242785 = {
        Form_Data_ID: $("#hdnFormDataID").val(),
        Tekst_Vipiski_0: ($("#richText").val() == undefined) ? null : window.parent.tinymce.get('richText').getContent({format: 'html'}),
        Username: "",
        User_ID: 0,
        Data: "",
        Customer: $("#hdnPatID").val(),
        Tip_Vipiski: ($("input[name='tip_vipiski']:checked").val() == undefined) ? 0 : $("input[name='tip_vipiski']:checked").val(),
        create_date: "",
        Comment: ($("#txtareaComment").val() == undefined) ? "" : $("#txtareaComment").val(),
        Attending_Doctor: ($("#docDDL").val() == undefined) ? $("#hdnDocID").val() : $("#docDDL").val().split('_')[1],
        Head1: $("#head1").val() != undefined ? $("#head1").val() : $("#lblhead1").text(),
        Head2: $("#head2").val() != undefined ? $("#head2").val() : $("#lblhead2").text(),
        Signature_603615: $('input[name="deputy"]:checked').length > 0 ? $("#deputy").val() : ($('input[name="deputy"]:checked').val() == undefined &&   $("#lblsig1").val() != undefined) ? $("#lblsig1").text()  : null,
        Signature_613915: $('input[name="mainHead"]:checked').length > 0 ? $("#mainHead").val() : ($('input[name="mainHead"]:checked').val() == undefined && $("#lblsig2").val() != undefined) ? $("#lblsig2").text() : null,
        Signature_Status :$('input[name="signed"]:checked').length > 0 ? $('input[name="signed"]:checked').length : null
    }
    $.ajax({
        type: "POST",
        url: "../Form/Form1242785",
        data: JSON.stringify({ obj1242785: objRel1242785}),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if ($('input[name="signed"]:checked').length > 0) {
                alert('Form Signed and assigned to next level hierarchy');
                window.location.reload();
            }
            else {
                window.location.reload();
                alert('Doctor Assigned');
            }
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}
function RejectReleaseDoc()
{
    $.ajax({
        type: "POST",
        url: "../Form/RejectReleaseForm",
        data: JSON.stringify({ patID: $("#hdnPatID").val(), textArea: window.parent.tinymce.get('richText').getContent({format: 'html'}) }),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
                alert('The release document has been assigned back to First Doctor');
                window.location.reload();
        },
        error: function () {
            alert("Error Occured");
        }
    });
}
function AssignReleaseForm()
{    
    var objRel1242785 = {
        Tip_Vipiski: ($("input[name='tip_vipiski']:checked").val() == undefined) ? 0 : $("input[name='tip_vipiski']:checked").val(),
        Customer: $("#hdnPatID").val(),
        Comment: ($("#txtareaComment").val() == undefined) ? "" : $("#txtareaComment").val(),
        Attending_Doctor: ($("#docDDL").val() == undefined) ? $("#hdnDocID").val() : $("#docDDL").val().split('_')[1],
        Signature_Status: null
    }
    $.ajax({
        type: "POST",
        url: "../Form/Form1242785",
        data: JSON.stringify({ obj1242785: objRel1242785}),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if ($('input[name="#signed"]:checked').length > 0) {
                alert('Form Signed and assigned to next level hierarchy');
            }
            else if (data == "0") {
                alert('Doctor already assigned');
            }
            else
            {
                alert('Doctor Assigned');
                window.location.reload();
            }
        },
        error: function () {
            // $('#myPleaseWait').modal('hide');
            alert("Error Occured");

        }
    });
}
function printPDF()
{
    $.ajax({
        type: "POST",
        url: "../Form/GeneratePdf",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({   stringData: $("#richText").text(), patID: $("#hdnPatID").val() }),
        success: function (data) {
            
            window.open(window.location.origin + data);
        },
        error: function () {
            alert("Error Occured");
        }
    });
}


function ValidateAppointment() {
    var returnVal = true;
    var errors = "";
    if (document.getElementById("Doctors").value == 0) {
        errors += "* Please select a Doctor!\n";
    }
    if (document.getElementById("txtCalendar").value == "") {
        errors += "* Please Enter a date!\n";
    }
    if (document.getElementById("patNotes").value == "") {
        errors += "* Please Enter Notes!\n";
    }
    if (document.getElementById("Reasons").value == 0) {
        errors += "* Please Select Reason!\n";
    }
    if (document.getElementById("selectedValue").value == "") {
        errors += "* Please Select Patient!\n";
    }
    if (errors != "") {
        alert("Following errors were found on your form:\n\n" + errors);
        returnVal = false;
    }
    return returnVal;
}

function ValidatePatient() {
    var returnVal = true;
    var errors = "";
    if (document.getElementById("recipient-lastname").value == "") {
        errors += "* Please enter patient's last name!\n";
    }
    if (document.getElementById("recipient-firstname").value == "") {
        errors += "* Please Enter patient's First Name!\n";
    }
    if (document.getElementById("recipient-dob").value == "") {
        errors += "* Please Enter DOB!\n";
    }
    if (document.getElementById("phone-number").value == "") {
        errors += "* Please enter patient's Phone !\n";
    }
    if (document.getElementById("registration-location").value == "") {
        errors += "* Please Select Registration Location!\n";
    }
    if (errors != '') {
        alert("Following errors were found on your form:\n\n" + errors);
        returnVal = false;
    }
    return returnVal;
}
