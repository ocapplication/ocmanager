<!DOCTYPE html>
<html>
<head>
<meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>   	
<link rel='stylesheet' href='../../../../modern-style.css' type='text/css'>
<link rel='stylesheet' href='../../../css/style.css' type='text/css'>
<link rel='stylesheet' href='../../../css/often.css' type='text/css'>
<link rel='stylesheet' href='../../../css/ui.datepicker.css' type='text/css'>


<!--<script src='../../../js/jquery-1.3.2.js'/>/
<script src='jquery-<?=$form_id?>.js'/>
<script src='../../../js/ui.datepicker.js'/>
<script src='../../../js/ui.datepicker-ru.js'/>-->

		<script type='text/javascript'>
		    $(document).ready(function () {
		        $.datepicker.setDefaults($.extend($.datepicker.regional['ru']));
		        $(".kalendar").datepicker();
		    });
		</script>
<link href="/Content/bootstrap.min.css" rel="stylesheet"/>
<link href="/Content/fullcalendar.min.css" rel="stylesheet"/>
<link href="/Content/site.css" rel="stylesheet"/>
<link href="/Content/datepicker.css" rel="stylesheet"/>
<link href="/Content/select2.css" rel="stylesheet"/>
<link href="/Content/typeahead.css" rel="stylesheet" />
<script src="/Scripts/modernizr-2.6.2.js"></script>
<script src="/Scripts/jquery-3.1.1.js"></script>
<script src="/Scripts/moment.min.js"></script>
<script src="/Scripts/fullcalendar.js"></script>
<script src="/Scripts/bootstrap-timepicker.min.js"></script>
<script src="/Scripts/bootstrap-select.js"></script>
<script src="/Scripts/bootstrap.js"></script>
<script src="/Scripts/respond.js"></script>
<script src="/Scripts/bootstrap-datepicker.js"></script>
<script src="/Scripts/typeahead.bundle.js"></script>
<script src="/Scripts/siteLayout.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#timepicker1').timepicker({
                showMeridian: false,
                minuteStep: 5
            });
            $('#timepicker2').timepicker({
                showMeridian: false,
                minuteStep: 5
            });
            $('#txtCalendar').datepicker("setDate", new Date());

            $('.Calendar').datepicker();
        });
    </script>

    <style type="text/css">
        .bootstrap-timepicker-widget {
            z-index: 1048 !important;
        }

        .datepicker-dropdown {
            z-index: 1048 !important;
        }
    </style>

</head>

  <body bgcolor="#eeeeee">



   <!-- <input type="hidden" id="patient_id" value=""<?=$_GET['id']?>">
    <input type="hidden" id="username" value=""<?=$_GET['username']?>">
    <input type="hidden" id="user_id" value=""<?=$_GET['user_id']?>">
    <input type="hidden" id="form_data_id" value=""<?=$_GET['form_data_id']?>">-->
    
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Календарь</a>
            </div>

            <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">

                        <!--<li><a href="/">Домой</a></li>-->
                        <li class="" data-toggle="modal" data-target="#addPatient"><a href="/OccurenceManager/AddPatient">Добавить пациента</a></li>

                        <li data-toggle="modal" data-target="#addAppointment"><a href="/OccurenceManager/AddAppointment">Добавить запись в календарь</a></li>
                        <li data-toggle="modal" data-target="#searchPatient"><a href="/OccurenceManager/SearchPatient">Поиск</a></li>
                    </ul>
                <form action="/Account/LogOff" class="navbar-right" id="logoutForm" method="post"><input name="__RequestVerificationToken" type="hidden" value="y3B0U1iSV1UlaYF8d9GWvyq9pweFqK27-8esWfjSAFQRYaSeyvGPtk9CrohP4SKUOCCu32n0rOwmeXHdPMOshCVAWrvm-9yfzEO4mZJP3SE1" />    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="/Manage" title="Manage">Добрый день</a>
        </li>
        <li><a href="javascript:document.getElementById('logoutForm').submit()">Log off</a></li>
    </ul>
</form>
            </div>
        </div>
    </div>
    <div class="container body-content">
    
        

