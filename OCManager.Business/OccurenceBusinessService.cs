﻿using OCManager.Interface.BusinessInterface;
using OCManager.Interface.DataInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Entities.DBEntities;
using OCManager.Entities.Anonymous;

namespace OCManager.Business
{
    public class OccurenceBusinessService : IOccurenceBusinessService
    {
        IOccurenceDataService _occurenceDataService;
        public OccurenceBusinessService(IOccurenceDataService occurenceDataService)
        {
            _occurenceDataService = occurenceDataService;
        }
        public List<OCManager.Entities.Anonymous.OccurenceDTO> GetOccurencesList(int locationID, string selectedDate)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetOccurencesList(locationID, selectedDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OCManager.Entities.DBEntities.Room> GetRooms()
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetRooms();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Reason> GetReasons()
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetReasonFromDB();
                //return new List<OCManager.Entities.DBEntities.Reason>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SavePatientData( PatientDTO obj)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.SavePatientDB(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public List<OCManager.Entities.Anonymous.Doctors> GetDoctors()
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetDoctorList();
                //return new List<OCManager.Entities.DBEntities.Reason>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<OCManager.Entities.Anonymous.PatientSearch> GetPatients()
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetPatientList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveAppointment(Occurence occurence)
        {
            try
            {
                _occurenceDataService.CreateSession();
                _occurenceDataService.SaveAppointment(occurence);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OccurenceHistory GetAppointmentById(int appointmentID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetAppointmentById(appointmentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAppointment(int appID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                _occurenceDataService.DeleteAppointment(appID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<OccurenceHistoryDTO> GetAppointmentHistory(int ID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetAppointmentHistoryByID(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<paymentDTO> GetPaymentList(int ID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetPaymentsByID(ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public PatientCardDTO GetPatientCard(int perID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetPatientCardData(perID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdatePatientNumber(Number objNum, int patID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.UpdatePatientNumberByID(objNum, patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdatePatientAddress(Address objAdd, int patID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.UpdatePatientAddressByID(objAdd, patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeletePatientNumber(int numID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.DeletePatientNumberByID(numID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int DeletePatientAddress(int addID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.DeletePatientAddressByID(addID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdatePerson(PatientDTO p)
        {
            try
            {
                _occurenceDataService.CreateSession();
                _occurenceDataService.UpdatePersonByID(p);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdatePateintStat(PatientDTO p)
        {
            try
            {
                _occurenceDataService.CreateSession();
                _occurenceDataService.UpdatePatientStatByID(p);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SavePatientAttendace(int ID, bool val)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.SavePatientAttendaceData(ID,val);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FormDataDTO> GetFormData(int perID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetFormDataList(perID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SavePatientDiscount(DiscountNote pn)
        {
            try
            {
                _occurenceDataService.CreateSession();
                _occurenceDataService.SavePatientNote(pn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SavePatientContract(PatientContract pc)
        {
            try
            {
                _occurenceDataService.CreateSession();
               return _occurenceDataService.SavePatientContactDB(pc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SavePatientInsurance(PatientInsurance pi)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.SavePatientInsuranceDB(pi);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public List<Number> GetpatientNumbers(int patientID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetpatientNumberList(patientID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Address> GetpatientAddress(int patientID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetPatientAddressList(patientID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PatientNote> GetPatientNoteList(int patID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetPatientNoteListByID(patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PatientContract> GetPatientContractList(int patID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetPatientContractListByID(patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Buildings> GetAllBuildings()
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetAllBuildingsFromDB();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<MenuFormDTO> GetFormList()
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetFormListWithCategory();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PatientDTO> SearchPatientList(string sc1, string sc2, string sc3)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.SearchPatientFromDB(sc1, sc2, sc3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SavePatientNote(PatientNote pn)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.SavePatientNoteToDB(pn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeletePatientNote(int pnID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                _occurenceDataService.DeletePatientNoteByID(pnID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SendEmailInviteToDoc(int patID, int concernedPerID, string username)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.SendEmailToDoc_SavePatProviderHistory(patID, concernedPerID, username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PatientProviderHistory> GetSupervisorDoctorList(int patID)
        {
            try
            {
                _occurenceDataService.CreateSession();
                return _occurenceDataService.GetSupervisorDocListFromDB(patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}
