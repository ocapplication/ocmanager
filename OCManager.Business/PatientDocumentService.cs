﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Interface.BusinessInterface;
using OCManager.Interface.DataInterface;
using System.Web;

namespace OCManager.Business
{
    public class PatientDocumentService : IPatientDocBusinessService
    {
        IPatientDocDataService _patientDataService;
        public PatientDocumentService(IPatientDocDataService PatientDocDataService)
        {
            _patientDataService = PatientDocDataService;
        }
        public List<OCManager.Entities.Anonymous.PatientDocDTO> GetDocumentsByPatientID(int patID)
        {
            try
            {
                _patientDataService.CreateSession();
                return _patientDataService.GetPatientDocList(patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OCManager.Entities.DBEntities.Category> GetCategoryList()
        {
            try
            {
                _patientDataService.CreateSession();
                return _patientDataService.GetCatListDB();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveDocumentUnderCategory(HttpPostedFileBase file, int CatID, string path, int patientID, int userID)
        {
            try
            {
                _patientDataService.CreateSession();
                _patientDataService.SaveDocumentCategoryDB(file, CatID, path, patientID, userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateDocumentData(int docID, int catID, string docName)
        {
            try
            {
                _patientDataService.CreateSession();
                return _patientDataService.updateDocumentToDB(docID, catID, docName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
