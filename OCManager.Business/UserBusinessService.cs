﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Interface.BusinessInterface;
using OCManager.Interface.DataInterface;
namespace OCManager.Business
{
    public class UserBusinessService : IUserBusinessService
    {
        IUserDataService _userDataService;
        public UserBusinessService(IUserDataService userDataService)
        {
            _userDataService = userDataService;
        }
        public Entities.DBEntities.User GetUser(string username, string password)
        {
            try
            {
                _userDataService.CreateSession();
                return _userDataService.GetUser(username,password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
