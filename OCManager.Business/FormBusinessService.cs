﻿using OCManager.Entities.Anonymous;
using OCManager.Entities.DBEntities;
using OCManager.Interface.BusinessInterface;
using OCManager.Interface.DataInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCManager.Data.DataServices;

namespace OCManager.Business
{
    public class FormBusinessService : IFormBusinessService
    {
        IFormDataService _formDataService;
        public FormBusinessService(IFormDataService formDataService)
        {
            _formDataService = formDataService;
        }
        
        public void SaveForm1464314(Form1464314 frm, int patID)
        {
            try
            {
                _formDataService.CreateSession();
                _formDataService.SaveForm1464314(frm, patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Appointment> GetProcedureListByFormID(int frmID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.ProcListByFrmID(frmID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int FetchDiscountCalculation(int patId)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetPatientDiscountCalDB(patId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<Discount> GetDiscountList()
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetDiscountFromDB();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveOrderIndex(List<string> Ids)
        {
            try
            {
                _formDataService.CreateSession();
                _formDataService.SaveOrderToDB(Ids);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int CheckAttendaceDate(int patID, DateTime date)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.CheckPatientAttendance(patID, date);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveForm1247857(List<storage_string> items, string total, int userID, int frmID, int? formDataID, DateTime dateSelected)
        {
            try
            {
                _formDataService.CreateSession();
                 return _formDataService.SaveForm1247857DB(items, total, userID, frmID, formDataID, dateSelected);               

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveBaseForm1247857(List<Appointment> lstApp)
        {
            try
            {
                _formDataService.CreateSession();
                _formDataService.SaveBaseForm1247857DB(lstApp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<storage_string> GetStorageStrinfFormData(int fdID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetSavedFormData(fdID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public OCManager.Entities.Anonymous.Form_2161884DTO GetForm2061884Data()
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetForm2061884DB();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Save2161884(Form_2161884DTO obj, int patID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.Save2161884DB(obj, patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SavePatHeiling(PatientInHealing patData)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.SavePatHeilingDB(patData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int RejectReleaseForm(int patID, int uID, string textArea)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.RejectReleaseFormDB(patID, uID, textArea);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FormDataDTO> GetAllPatientFormData(int patID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetAllPatientFormDataByID(patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FormDataDTO> GetPatientFormData(int patID, string from, string to)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetPatientFormDataByID(patID, from, to);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Form1242785 GetReleaseFormData(int patID, int userID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetAssignedPersonFromDB(patID, userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Form1242785 CheckReleaseFormAssignee( int patID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetReleaseFormAssigneeDB( patID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Person GetPerson(int perID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetPeronFromDB(perID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public Form1242785 GetAssignedDoctor(int patID, int userID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetAssignedPersonFromDB(patID, userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Doctors> GetDoctorList()
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetDoctorListDB();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SaveForm1242785(Form1242785 obj, int locID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.Save1242785DB(obj, locID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetPatientInfo(int personID)
        {
            try
            {
                _formDataService.CreateSession();
                return _formDataService.GetPatientInfoDB(personID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
